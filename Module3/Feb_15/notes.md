# CS2610 - Fri Feb 15 - Module 3

# Announcements


## No class on Monday 2/18 for President's Day holiday



## FSLC - Wireshark Tutorial

Wednesday 2/20
7pm - ESLC room 053

Wireshark® is a network protocol analyzer. It lets you capture and
interactively browse the traffic running on a computer network. It has a rich
and powerful feature set and is world's most popular tool of its kind. It runs
on most computing platforms including Windows, macOS, Linux, and UNIX. Network
professionals, security experts, developers, and educators around the world use
it regularly. It is freely available as open source, and is released under the
GNU General Public License version 2. 



# Topics

* What is an HTML Form?
* Form input elements




--------------------------------------------------------------------------------
# What is an HTML Form?

Think about what "form" means

![Choose the form...](form.jpg)

No, not that kind of form.  Something more like this:

![A standard form](mcform.png)

The webpages we've used so far represent one-way communication from server to
client.  HTML Forms enable us to talk back to webservers.  Information is
collected by the browser much as when filling in blanks on a paper form.  The
filled-in form is then sent by the browser to the server in an HTTP request.

* A form is an element on a webpage.
* A form contains <input> child elements to collect user input
* The form describes where to send a user's input and which type of HTTP
  request to use.

https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Your_first_HTML_form


## HTML Form syntax

To create a form section, we provide the form with a name, id, action, and
method. An example with blank attributes looks like this:

    <form action="https://duckduckgo.com/" method="GET">

        <!-- labels, buttons, sliders, text boxes, etc. go in here :) -->

    </form>


## HTML Form element Attributes

At minimum a `form` needs to know *where* to go and *how* to get there.  This
information is encoded into attributes of the `form`.

* `action` - The URL your browser sends the user upon form submission.  This
  may be an absolute or a relative URL.  If you leave this attribute off, the
  browser defaults to the URL of the page you're on.
* `method` - Which HTTP request to send the user's carefully entered data to
  the server.  Any HTTP request type is acceptable here, for example this
  attribute's value may be `GET`, `POST`, `PUT`, or `DELETE`.  If you leave
  this off your browser defaults to `GET`.


Right now, the only HTTP method we've used is `GET`, which causes the browser
to encode the data of your form inputs as into the URL itself as plain text.

The browser adds a ? after the path in your URL, and appends the form's inputs
as `name=value` pairs.  Consecutive `name=value` pairs are separated with
ampersand characters (&).

The `name` comes from the name="" attribute of the input widget.  The browser
replaces spaces ( ) with plus-signs (+) where they occur in the text.  The
browser encodes other non-alphanumeric characters in a scheme known as "URL
Encoding". You've likely seen these before as %0D characters in a URL.




## Do I need a server to use an HTML Form?

Yes and no.

Yes, a server needs to be involved.

No, it doesn't need to be *your* server.

[A search form](search.html)

Observe the URL formed by using this HTML search form.




----------------------------------------------------------------------------
# Form input elements

* https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Your_first_HTML_form
* https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/The_native_form_widgets



## The label element

Input elements can have a label which is associated with them. If you click on
the label, it's corresponding input element is given focus.

    <label for="ddg-search">Search Term</label>

`for=""` selects an input element by it's `id=""` attribute.  In this context
we don't use the '#' mark as with a CSS selector.



## The button element

Generally used to send or "submit" a form to the server.  May contain text or
an image.

    <button id="submission" type="submit">Search on DuckDuckGo</button>


The valid button types:

* submit: submit form data to the server; this is the default button type.
* reset: Reset all inputs on the form to their default values.
* button: The button has no default behavior.  Used to connect a button with a
  JavaScript function.


We can use the `submit` button to easily see what happens to our form data when
sent to the server with a `GET` request by reading the new URL our browser
visits.




## The input element

This versatile element can take many appearances and provide many types of
functionality.  This is all controlled thorugh the `type=""` attribute.  Each
input on a form is named, and this name is sent to the server along with the
data as key/value pairs.

    <input id="ddg-search" name="q" type="password"/>

`id=""` gives an input an ID (this is the same ID concept that we've seen
before with CSS).  The ID of an input is *not* sent to the server; the ID is
used for styling purposes and to associate the input with a label.

`name=""` gives this piece of data a name which is used by the server.  

of the value in the URL after the ?

    `type=""`
    	What sort of input widget - text entry, slider, checkbox, etc.





## Simple input fields

| Input type        | Description
|-------------------|------------------------------
| `type="text"`     | The boring default
| `type="email"`    | Validates input is in the form of an email address
| `type="password"` | Represents entered text with asteriks 
| `type="search"`   | In some browsers adds an "x" to clear out text
| `type="number"`   | Validates input is numeric; adds increment/decrement buttons
| `type="url"`      | Validates input is of the form `protocol://domain.tld`
| `type="color"`    | Provides a color-chooser widget


The `placeholder` attribute can be used to provide text to display when the
input is empty.

You can change the `type` of any input live in the browser with the developer
tools.  I can turn a `type="password"` into a `type="text"` and reveal the
password that is there.  This is why you should never allow a browser running
on a computer which you do not control to remember your login credentials.  For
that matter, you should really think twice about using this feature at all.

**Yandex Mail Demo**



## Multi line text input

Use the `textarea` element to provide a text editor box for the user to enter
longer text.

    <textarea cols="30" rows="10">Prepared text goes here</textarea>




## [Drop-Down content](drop-down.html)

The `select` element is used to provide a drop-down menu of options.  It contains one or more `option` elements, each of which represents an allowed choice.

Options may be grouped into `optgroup` elements.

You can enable the user to make multiple selections (a multiple choice box) by adding the `multiple` attribute to a `select` element

`option` elements may be placed within a `datalist` parent element instead of a
`select` element.  When the `datalist` is given an id attribute, these options
are available to a text input as auto-complete choices.
