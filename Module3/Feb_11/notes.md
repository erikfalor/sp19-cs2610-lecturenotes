# CS2610 - Mon Feb 11 - Module 3

# Announcements



## AIS Security SIG meeting

Military Intelligence personnel will present on "Social Engineering" and
Interrogation.

6pm Tuesday Feb 12 
Huntsman Hall 326




# Topics:
* Finish the highFive demo
* Explore the Django database API in the REPL
* A deeper dive into database organization
* What is the Admin app, and how do I use it?



--------------------------------------------------------------------------------
# Finish the highFive demo

Get the code on bitbucket now


--------------------------------------------------------------------------------
# Explore the Django database API in the REPL

At this point you are able to go through the tutorial, in order, from start to
finish (skipping over those sections listed as unnecessary in the R&R).  Don't
neglect to do this as it is the best way to come up to speed on Django!


Today we will explore the Django database API in the REPL by

* Creating database row objects
* Saving them to the DB
* Retrieving them from the DB again
* Overriding Python's ToString() equivalent (the __str__() method) to give each
  row in our Database a pleasant string representation.

https://docs.djangoproject.com/en/2.1/intro/tutorial02/#playing-with-the-api


Key observations:
-----------------

* You must run ```./manage.py makemigrations APP_NAME``` and then
  ```./manage.py migrate``` before you can begin to use the database.

* The models we're using in our Blog app (Blog and Comment) have a lot of
  functionality given to us for free by virtue of the fact that they inherit
  from django's models.Model class.

* New rows in the database are created by saving the result of a call to the
  Model constructor. In the case of our blog app, the call

    Blog(title="What's new?", pub_date=timezone.now())

  creates a row object. We should save that object into a variable so we can
  later call the save() method on it to write it down into the DB:
      
    b = Blog(title="What's new?", pub_date=timezone.now())

* The database isn't actually changed until we call the `save()` method on an object:

    b.save()

* The validity of the data isn't tested/enforced until we attempt to save it.
  Become familiar with what the error trace looks like when this happens, as it
  may happen often during development ;)

* The primary key field called "id" isn't assigned until we call the `save()`
  method.

* If we go out of our way to manually set the `id` field, calling save() will
  overwrite the existing row. Otherwise, `save()` adds a new row to the DB.


### Django provides a rich database lookup API that's entirely driven by keyword arguments

[How do I do database queries in Django without SQL?](../Readings-and-resources.md#markdown-header-how-do-i-do-database-queries-in-django-without-sql)



----------------------------------------------------------------------------
# A deeper dive into database organization

* What is significant about primary keys?
* Why is the type and size of fields significant to the database system?
* What happens to a child record when its parent is deleted?
* Are primary keys reused after deletion?


polls_app_schema.ods




----------------------------------------------------------------------------
# What is the Admin app, and how do I use it?

https://docs.djangoproject.com/en/2.1/intro/tutorial02/#explore-the-free-admin-functionality

Yay! More free stuff from the Django framework! An entire web application
devoted to making managing your databas models easy.

Let's face it, you're not going to maintain your database by running the Django
REPL directly.

You can reach the admin app by visiting the /admin path of your app:
    http://127.0.0.1:8000/admin

You may have noticed that in your project's urls.py file

It's installed by default in your project's settings.py file, in the INSTALLED_APPS list.


### How do I use the admin app?

Let's visit your admin app by running Django and visiting http://localhost/admin

Oh, what's the password?

Wait, what's my username?



#### Create an administrator account

    $ python manage.py createsuperuser



#### Make your models editable in the admin

Register your models with the admin app by 

1. Editing blog/admin.py 
2. Importing your models
3. Registering each model with `admin.site.register(Blog)`
 
Did you notice how the admin site uses your `__str__()` methods from your models?
