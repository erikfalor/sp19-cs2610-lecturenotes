# CS2610 - Wed Feb 20 - Module 3

# Announcements



## FSLC - Wireshark Tutorial

Tonight 2/20
7pm - ESLC room 053

Wireshark® is a network protocol analyzer. It lets you capture and
interactively browse the traffic running on a computer network. It has a rich
and powerful feature set and is world's most popular tool of its kind. It runs
on most computing platforms including Windows, macOS, Linux, and UNIX. Network
professionals, security experts, developers, and educators around the world use
it regularly. It is freely available as open source, and is released under the
GNU General Public License version 2.





## ACM-W Homework Nights

ACM-W is hosting homework nights every third Thursday of each month
from 6:00pm - 9:00pm in ENGR 202. 

The first one is tomorrow night, Thursday, Feb 21  

ACM-W is especially for women majoring or minoring in Computer Science, but all
are still welcome.

Snacks will be provided!

*(ACM-W is the USU women’s chapter of the Association of Computing Machinery)*

acm-wusu on Facebook



# Topics
* How are HTML Forms an Application Programming Interface?
* What is CRUD?


--------------------------------------------------------------------------------
#  How are HTML Forms an Application Programming Interface?


## What is an API?

The traditional definition of API refered to libraries of code used within a
programming environment. An API describes the packages you can import, objects
you can use, functions you can call and what types of data those functions
return. An API may also describe protocols used to facilitate communication
between different systems.

More recently this acronym has become closely associated with web-based
technologies. The API in use by a web service may describe the URLs from which
certain data may be retrieved, what format that data will be sent (i.e. XML,
JSON, etc.) and what data may be uploaded from the client.


## Using HTML Forms in Django

[Write a simple form](https://docs.djangoproject.com/en/2.1/intro/tutorial04/#write-a-simple-form)


### How does our Blog assignment use HTML Forms?

*   Sending nickname, email address, and comment data to the server
*   The data is sent to a view function, which stores it into the database
*   


## HTTP Redirects - why not stay on the POST view?

What happens if I don't use this complicated HttpResponseRedirect thing?

Demo: Stuffing the ballot box with the 'Refresh' button

* redir:



### What is the {% csrf_token %} tag for?

What does Django's template engine place at that position in the output HTML document?

And what happens to my app if I remove it?

Demo: Stuffing the ballot box with cURL


## Tutorial #4 Generic views

You may skip over this section of the tutorial. You're all done with the polls
app, and you know everything you need to know to implement your Blog app!




--------------------------------------------------------------------------------
# What is CRUD?

      ,--,  ,---.  .-. .-. ,'|"\
    .' .')  | .-.\ | | | | | |\ \
    |  |(_) | `-'/ | | | | | | \ \
    \  \    |   (  | | | | | |  \ \
     \  `-. | |\ \ | `-')| /(|`-' /
      \____\|_| \)\`---(_)(__)`--'
                (__)

Do you know what they put in that square lake 3 1/2 miles west of here?


## There is now another meaning of CRUD
https://en.wikipedia.org/wiki/Create,_read,_update_and_delete

Let's map these words to concepts from HTTP and Django
------------------------------------------------------

                    Database     Database         HTTP        Safety/Idempotence
      ___           (SQL)        (Django)
     / __|reate                                                !idempotent   
    | (__           INSERT       .save()          POST         !safe         
     \___|


     ___
    | _ \ead                     .filter()                      idempotent  
    |   /           SELECT       .get()           GET           safe        
    |_|_\                        .all()


     _   _
    | | | |pdate                                                idempotent   
    | |_| |         UPDATE       .save()          PUT           !safe                
     \___/


     ___
    |   \elete                                                  idempotent   
    | |) |          DELETE       .delete()        DELETE        !safe          
    |___/



## Jot this table down on your mudcard



### Difference between PUT and POST

In this chart I have associated HTTP's POST request with the **Create** verb,
and PUT with **Update**, but the distinction isn't always clear.  Plenty of
APIs in the real world mix the two concepts because HTTP's notions don't
exactly corespond to those in CRUD.

One rule-of-thumb given in the [REST Cookbook](http://restcookbook.com/HTTP%20Methods/put-vs-post/)
suggests that one use **POST** when creating a new resource when the client
doesn't know the URL that new resource will be placed, and using **PUT** when the URL of the resource is known:


> Use PUT when you can update a resource completely through a specific
> resource. For instance, if you know that an article resides at
> http://example.org/article/1234, you can PUT a new resource representation of
> this article directly through a PUT on this URL.
>
> If you do not know the actual resource location, for instance, when you add a
> new article, but do not have any idea where to store it, you can POST it to
> an URL, and let the server decide the actual URL.

An example in our Blog project is the creation of a new comment.  That comment
will get an ID number upon being saved in the Database, but until that happens
the client has no way of predicting that number and cannot use it in any
request.  Therefore, the client should use **POST** to ask the server to create
the comment and assign it an ID.

It would be appropriate to use a **PUT** request to edit an already-existing
comment, since the comment would by that time have an ID number which could be
provided to the client.



### Safety

What does it mean for an operation to be "safe"?

#### Safe: An operation which does not modify resources on the server

Which of these operations are safe?
1. Read



###  Idempotence

How are you supposed to say this silly word?

https://www.youtube.com/watch?v=RE-X9Uqpz8w

What does it mean for an operation to be "idempotent"?


#### Idempotent: It doesn't change things to do an operation more than one time

    # Python example:
    a = 1;
    for i in range(1000):
        a = 1

Which of the above operations are idempotent?
1. Read
2. Update
3. Delete


