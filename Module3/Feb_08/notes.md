# CS2610 - Fri Feb 08 - Module 3

# Announcements

## Assignment 2 Effort Survey

This optional survey will help me develop better homework assignments.
Your participation is not required and all responses are anonymous.

https://usu.instructure.com/courses/529849/quizzes/714282



## AIS Security SIG meeting

Military Intelligence personnel will present on "Social Engineering" and
Interrogation.

6pm Tuesday Feb 12 
Huntsman Hall 326




# Topics:
* Exam 0 Recap
* What's the difference between an *app* and a *project*?
* Django Migrations

----------------------------------------------------------------------------
# Exam 0 Recap


__Question 22__ (66% answered correctly)
* Q:    What will the selector body, div, p select?
* A:    All body, div, and p elements in the document


__Question 23__ (82% answered correctly)
* Q:    What will the selector div.alert select
* A:    All div elements which contain alert in their class list


__Question 27__ (40% answered correctly)

* Q:    In Django, the controller is
* A:    A Python list named urlpatterns which maps URLs to views


__Question 28__ (77% answered correctly)

* Q:    In Django, a view is
* A:    A Python function which takes an HTTP Request as input and returns an HTTP Response


----------------------------------------------------------------------------
# What's the difference between an *app* and a *project*?

A project consists of apps, and an app consists of views.

* Project
    + The code which ```django-admin``` creates for you in your project
      directory
    + *Infrastructure*: Python code which makes Django act as an HTTP server
      which can process HTTP inputs and outputs; routes HTTP requests to your
      view functions based upon the URL given, etc.
    + A container for apps
    + *Configuration*: helpful Python code provided by the Django folks which
      enables your app to run on different web servers with different database
      engines.  This lets you focus on my own project, freeing you from
      "re-inventing wheels".
    + The project stays "behind the scenes"; users are unaware of its presence


* Application
    + The code you actually want to write, the fun stuff that users will see,
      interact with, and enjoy
    + Made of views (a.k.a. pages) which are unified in purpose


As an example, consider two Google products you're likely familiar with: Google
Docs and Gmail.  Each of these on their own would be an example of an *app*.
One is an application providing word processing and spreadsheet functionality
to users.  The other is an application used for communicating via the Internet
Electronic Mail protcol.

While a Google document can be shared via Gmail, and the content of emails
could be stored as documents, these applications really serve two different
purposes.  It makes sense from a software engineering point of view to have
different teams working on differet applications which serve different
purposes.

To give you a sense of how Django is used in the real world, you can find a list of sites using Django at the bottom of the Django project's [Overview page](https://www.djangoproject.com/start/overview/)

This page purports to be a list of [14 Popular Sites Powered by Django Web Framework](https://codecondo.com/popular-websites-django/)



----------------------------------------------------------------------------
# Django Migrations

As your app evolves your model will change.  This means that the objects in
your code will change over time, as well as the data on disk in the database.
Django's "migrations" allow existing data to "follow along" with your code
changes.  We won't delve too deeply into what migrations are or how they work;
it suffices to understand that Django can tell when we change the schema of our
database and we'll have to manually run a couple of commands to get Django to
quiet down about it.


Today we'll finally get rid of Django's red warning about "You have 15
unapplied migration(s)." whenever you run the server.




#### $ python manage.py migrate

Run the migration code in any apps which have unapplied changes.  This will
create a database file if there is none, and update any existing data according
to the instructions in the migration.

How do you create the "instructions in the migration"? 
You don't.  Django writes them for you ;)


#### $ python manage.py makemigrations

Django will read through the models.py files of your INSTALLED_APPS[], detect
any changes, and write the migration instructions for you.  Then, you may run
the previous command to make the changes stick.

Any time we make changes to our models (classes inside our app's `models.py`
file), we'll need to re-run this command in order for the changes to take
effect.

Your workflow will thus be:

1.  Add the name of our app's configuration object to the project's
    `settings.py` file (do this only once)
2.  Edit `models.py`
3.  Run `python manage.py makemigrations`
4.  Run `python manage.py migrate`



