# CS2610 - Wed Feb 13 - Module 3

# Announcements



## FSLC 90's Hacker Movie Night

It is roughly that time for mid-terms and we thought since many of you have
either taken your tests or studying your hearts out we should have a chill
night tonight. We are looking at watching a cheesy 90s movie called *Johnny
Mnemonic*: a Keanu Reeves action movie that has VR, Brain implants, and more!
Here is the trailer: https://www.youtube.com/watch?v=U_8BVWHSU_o

Tonight at 7pm in room 053 in the ESLC.


## Lucid Code Kerfuffle | Coding Competition for Prizes Totaling $50k

Free to enter
Saturday, March 9th 10am - 1pm
Register and fight online: https://codekerfuffle.com/
Requires a HackerRank account

Top 32 performers win $500 and an invitation to the on-site championship


# Topics:
* Clean up the testing database
* Initialize a testing database
* Template Filters



----------------------------------------------------------------------------
# Clean up the testing database

The next assignment requires that you create a couple of views which help the
grader manipulate your database; one view clears out the database, and the
other populates the database.

Now that we've created some junk data in the Django shell and Admin app, let's
clear it out so we can start afresh.  We'll create a new Django view to do this
so as to provide a more convenient user interface than the Django shell.


### Mudcard questions

On your mudcards, take a moment to answer the following questions:

* How might we find out how to delete a single record from the database?

* How might we go about deleting everything in the database?

* What's up with that `on_delete=models.CASCADE` thing in the model?

* After we've deleted all records and are left with an empty database, what's
  the value of the ID field of the next record we create?  Does it start over
  again at 1?


When you have designed the code to do this, you may put it into a view that is
activated when the `/nuke` address is visited.  This will free the user (i.e.
the graders) from needing to fire up an extra Django console to reset your
database while grading your assignments.


This is actually quite simple to achieve; let's write this view together, but
in the 'polls' app.




----------------------------------------------------------------------------
# Initialize a testing database

Now that we've swept the database clean, we must re-populate it so that the
full functionality may be graded.  More to the point, because you should not
include the file `db.sqlite3` in your repository, the graders must populate
your database so they can verify that your assignment meets the requirements.

By reading the assignment description you'll find that you are to write a view
to do this for them:

Description
-----------

Provide a view function accessible at the /init path of your webapp (e.g.
http://localhost:8000/blog/init) which will create a database of dummy blogs
and comments from scratch.  After this view is visited, the user is redirected
to the blog homepage where the three most recent blogs may be seen.


Requirements
------------

1. Update the blog app's urls.py to link the init/ path to the init() view

2. Remove all previous Blog entries and Comments from the database so we can
   start fresh

3. Create more than 3 blogs so that we can verify that the Blog index page and
   Blog archive page operate correctly

    + Create in each blog an arbitrary title, author and posted date
    + Fill each blog in with arbitrary text. Lorem Ipsum is the classic choice
      https://www.lipsum.com/

4. Generate some non-zero number of comments on each post so that we can verify
   that the count of comments is accurate

   + Create an arbitrary email address, commenter name and posted date
   + Fill each comment with arbitrary text. Again, Lorem Ipsum is your friend.



### Mudcard questions

On your mudcards, take a moment to answer the following questions:

* How can this view take advantage of the code already written for `nuke()`?

* Right now, this view displays a message telling the user that the database
  has been initialized.  How can this UI be improved upon?


### HttpResponseRedirect

We answered the last question by having our `nuke/` and `init/` views return an
`HttpResponseRedirect` object instead of an `HttpResponse`.  The difference
between the two is that the `HttpResponseRedirect` gives the browser an HTTP
response status code in the 300 class which instructs it to immediately make a
new request, and includes an HTTP header informing the browser where to go.

It all happens in a blink, but you can actually see this happen by watching
Django's output in the console.

Here you can see that when I visit the URL `/polls/nuke`, Django gives a 302 response code to my browser which results in the browser immediately (within the same second) making a new GET request to my index view:

    [13/Feb/2019 19:15:44] "GET /polls/nuke HTTP/1.1" 302 0
    [13/Feb/2019 19:15:44] "GET /polls/ HTTP/1.1" 200 111

Here is the same thing, but in response to the `/polls/init` view:

    [13/Feb/2019 19:17:02] "GET /polls/init HTTP/1.1" 302 0
    [13/Feb/2019 19:17:02] "GET /polls/ HTTP/1.1" 200 304



## Today's code

Here are the views I wrote today in class.  Adapt it to fit your app, and by
all means, please get your hands dirty with the Django ORM and really get
familiar with how the database works.

[nuke and init views](nuke_init_views.py)



----------------------------------------------------------------------------
# Django Template Filters

[Django Template Filters in Module 3 Readings and Resources](../Readings-and-resources.md)

