from django.http import HttpResponseRedirect
from django.urls import reverse

from django.utils import timezone
from .models import Question, Choice

def nuke(request):
    for q in Question.objects.all():
        q.delete()
    return HttpResponseRedirect(reverse('polls:index'))

def init(request):
    nuke(request)

    for i in range(7):
        q = Question(question_text="This is quiz question #" + str(i), pub_date=timezone.now())
        q.save()

        for j in range(17):
            c = Choice(question=q, choice_text="this is choice #" + str(j), votes=j)
            c.save()
    return HttpResponseRedirect(reverse('polls:all_of_them'))


