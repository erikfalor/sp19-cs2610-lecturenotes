# 3: Readings and Resources

## Table of Contents

* The Official Django Tutorial (continued)
* How do I do database queries in Django without SQL?
* Django Template Filters
* Sending data to the server with HTML Forms



----------------------------------------------------------------------------
## The Official Django Tutorial (continued)

I will expect you to be familiar with this tutorial as it will answer many
common questions that you will encounter as you work. If you ask me or the TA a
question, don't be surprised when the answer is "check the tutorial".

In this module we'll continue with our winding tour through the official Django
tutorial.  For this assignment, you may go through the entire tutorial in the
following sequence.  *Note the sections of the tutorial which you may skip*:

-   [Part 1](https://docs.djangoproject.com/en/2.1/intro/tutorial01/) - Creating a project and an app; writing a simple view
-   [Part 2](https://docs.djangoproject.com/en/2.1/intro/tutorial02/) - Models: Setting up the database, creating models, playing with the Django shell, using the Django Admin Site
-   [Part 3](https://docs.djangoproject.com/en/2.1/intro/tutorial03/) - Dynamic views, rendering templates, avoiding hardcoded URLs
-   [Part 4](https://docs.djangoproject.com/en/2.1/intro/tutorial04/) - Interactive views implementing HTML forms, *skip over Generic views*
-   Part 5 *Skip this section completely*
-   [Part 6](https://docs.djangoproject.com/en/2.1/intro/tutorial06/) - Static files: applying CSS and images to your webpage
-   Part 7 *Skip this section completely*



----------------------------------------------------------------------------
## How do I do database queries in Django without SQL?

The Python programming object `Blog.objects` provides an interface in the
Python programming language to rows in our Database table 'Blog'. Being an
Object-Oriented programming entity it contains many useful methods.  In case
you know SQL, I've translated them into their SQL equivalent:

| Method    | Keyword arguments? | # of results returned         | Equivalent SQL Query                   |
|-----------|--------------------|-------------------------------|----------------------------------------|
|`.all()`   | No                 | 0 or more results             | `SELECT * FROM table`                  |
|`.get()`   | Yes                | exactly 1 result, or it fails | `SELECT * FROM table WHERE ... LIMIT=1`|
|`.filter()`| Yes                | 0 or more results             |  `SELECT * FROM table WHERE ...`       |


*Note* `.get()` returns exactly 1 result or it fails.  If we ask it to 'get'
something ambiguous (meaning there are more than one possible result), it
throws an error instead of returning multiple results.  Use `.filter()` when
you expect multiple matches.


Once we have obtained a Blog object from the database, we can find all related
Comment objects (and vice-versa) through *relations*:

[Relations](https://docs.djangoproject.com/en/2.1/ref/models/relations/)


The keyword argument given to get() and filter() perform an SQL Database query
for us. What keywords are available for use is explained in this document:
[https://docs.djangoproject.com/en/2.1/topics/db/queries/#field-lookups-intro](https://docs.djangoproject.com/en/2.1/topics/db/queries/#field-lookups-intro)

This document describes the entire Database API. Keep it handy:
[https://docs.djangoproject.com/en/2.1/topics/db/queries/](https://docs.djangoproject.com/en/2.1/topics/db/queries/)

I strongly encourage you to spend a good portion of your time understanding
part 2 of the Django tutorial. The interaction with the database is the most
crucial part of your webapp. After this part of the tutorial, the going becomes
much easier!

### Django's Database API (a.k.a. the ORM) gives us protection against SQL-Injection attacks

[https://docs.djangoproject.com/en/2.1/topics/security/#sql-injection-protection](https://docs.djangoproject.com/en/2.1/topics/security/#sql-injection-protection)

----------------------------------------------------------------------------
## Django Template Filters

Filters transform the values of variables and tag arguments.

`{{ textVariable | Filters }}`

Django provides a large selection of filters out-of-the-box:

[Builtin filter reference](https://docs.djangoproject.com/en/2.1/ref/templates/builtins/#ref-templates-builtins-filters)


## truncatewords
You may find the `truncatewords` filter to be particularly useful in Assignment #3:

https://docs.djangoproject.com/en/2.1/ref/templates/builtins/#truncatewords


## first

Extracts the first item from a list

    {{ highFives | first }}


## length

Converts a list into its length

    {{ value | length }}



## last

Extracts the last item from a list

    {{ highFives | last }}


## pluralize

Returns a plural suffix if the value is not 1. By default, this suffix is 's'.

    {{highFives | length }} high five{{highFives | pluralize}} given to date - <a href="highFive">Do you want a high five?</a>


You can supply a different set of strings for the filter to choose from by
giving the filter a comma-separated list of possibilities as a parameter.  Here
I use the filter to choose between the verbs "is" and "are" depending on
whether there is only one highFive or another number:

    There {{highFives | pluralize:"is,are"}} {{highFives | length}} high five{{highFives | pluralize}} on this site so far - <a href="highFive">Do you want a high five?</a>



----------------------------------------------------------------------------
## Sending data to the server with HTML Forms

* [HTML Forms Overview](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms)
* [The Native Form Widgets](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/The_native_form_widgets)
* [Sending form data](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Sending_and_retrieving_form_data)



