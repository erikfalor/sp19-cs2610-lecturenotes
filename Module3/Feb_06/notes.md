# CS2610 - Wed Feb 06 - Module 3

# Announcements


## FSLC Meeting

The Vim vs. Emacs Showdown!!!
Wednesday Feb 6
7pm - ESLC 053



## DC435 Meetup - Packet Capture Games

Have you ever wanted to know what your ISP can see about you? What about your
employer? Or someone else on your hotel wifi with you?

Come learn about network packets and play some **Wireshark** packet capture games

https://dc435.org/blog/2016-12-17-making-sense-of-the-scaas-new-flavor-wheel/

Thursday, Feb 7th 7pm
BTech West Campus - 1410 North 1000 West




# Topics:

* Introduce Assignment 3: Dynamic Blog in Django With The ORM
* What is "database schema"?
* Activity: Design your blog app's schema
* Django Database Integration




----------------------------------------------------------------------------
# Introduce Assignment 3: Dynamic Blog in Django With The ORM

[Assn3](https://usu.instructure.com/courses/529849/modules/items/3366237)


## What is the problem that you have been asked to solve?

(mudcard)



----------------------------------------------------------------------------
# What is "database schema"?

Remember that time I made a visitor counter using a global variable?  There had
to be a better way than that, right?  There is.

Databases.


Consider an object-oriented application that stores blog posts and comments on
those posts.

* What objects will your system include?
* How will the objects relate to one another?
* What pieces of data will you store in each object?




#### Schema: The organization of data as a blueprint of how the database is constructed 


When desiging a database we want to follow organizational principles that are
similar to the way we organize object-oriented code.


#### Classes : Object :: Table : Rows

Code is organized into classes with data members
Databases are organized into tables with columns

The concepts of classes and tables are virtually the same, so much so that
nearly every programming interface to a database represents database tables as
classes, and rows of data in a database as instances (a.k.a. objects).


----------------------------------------------------------------------------
# Activity: Design your blog app's schema

(mudcard)

1. List the items of data that your blog app will need to capture.
2. Organize them into categories (OOP classes)
3. Draw the relationships between the classes

You should keep this diagram if it is helpful to you.

For participation purposes, put your name and/or A number on a sticky note.



----------------------------------------------------------------------------
# Django Database Integration


The piece of software which is a bridge between a database system and an
object-oriented programming language is called an Object Relational Mapper.

## Object Relational Mapper (ORM)

https://en.wikipedia.org/wiki/Object-relational_mapping


Learning how to interact with the database through Django is learning to use its ORM.

An ORM gives us many benefits:

* I don't have to learn/write/read SQL
* I'm isolated from the various platform-specific SQL idiosyncrasies
* I get to treat my DB as if it's a collection of objects - within an OOP
  language, this is snazzy


## Models in Django

At this point I shall encourage you to go through the Django tutorial from the
beginning, not just reading, but actually building the Polls app described
therein.  It won't take you very long to build it, and will introduce you to
many useful concepts and techniques that I will expect you to understand.

https://docs.djangoproject.com/en/2.1/intro/tutorial02/

Additionally, the MDN has a similar Django tutorial for building a slightly
more sophisticated app that you may find to be helpful to follow:

https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django


A model in Django is a class which inherits from models.Model.  Its data
members correspond to the columns contained within.



## Data types in our database

A database naturally wants to impose certain datatypes on each of its columns;
if a column is declared to hold character data it is an error to introduce
textual data.  This will feel familiar if you're coming from C++ or Java,
however databases tend to take it even further, specifying that character data
(strings) be no longer than some maximum length.

Naturally, this poses a difficulty for a dynamically-typed language such as
Python which does not ordinarily impose this restriction on its variables.

We can work around this limitation by creating values that are a special kind
of object created by the Django folks.  When we try to assign a value into one
of these special Django fields (using something like a setter function), code
is run which ensures that the incoming value matches the expected type.

Django ships with dozens of built-in field types; here is the complete list:

https://docs.djangoproject.com/en/2.1/ref/models/fields/#model-field-types
