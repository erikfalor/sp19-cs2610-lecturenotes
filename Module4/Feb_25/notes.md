# CS2610 - Mon Feb 25 - Module 4

# Announcements

## FSLC - Ricing your Linux Desktop

Wednesday 2/27
7pm - ESLC room 053






# Topics:

* BSidesSLC debrief
* What do I need to do on the next assignment?
* JavaScript and the DOM


----------------------------------------------------------------------------
# BSidesSLC debrief

Takeaways from the [BSidesSLC 2019 - Tech Panel](https://www.youtube.com/watch?v=UyplZKZoDms)

* Be curious and get out of your comfort zone.

* Kids coming out of school are already 5 years out-of-date.  Academia cannot
  keep up with the latest trends in a rapidly-moving industry.

* Students must "learn how to learn", and have the drive to not sleep until
  they have solved the problem at hand.

* University courses must start out of the gate with security.

* To the extent that you can, learn under people who are professionally doing
  what you want to do.

* School is not enough.  Get hooked into the community and make connections
  with professions.  Engage in competitions and conferences.

* Don't hold yourself back because of your lack of experience.  The people
  sitting on this panel were just like you a few years ago.  Folks in the
  industry can work with a lack of knowledge.  What they don't abide is a lack
  of curiosity and drive.


### *Q:* What advice do you have for people coming out of Academia?

*A:* What are you doing outside of school?  What are your passion projects?  What
are you blogging about and posting to your Bitbucket/Github repo?  Show
potential employers/partners how you choose to spend your personal time.


### *Q:* What are the knowledge gaps in people coming out of Academia?

*A:* New grads are very current on techniques from 5 years ago.
Don't push security off to the end of the semester, lead with security.
Learn about SCADA, learn about Internet of Things (IoT)



[BSidesSLC 2019 Conference Channel](https://www.youtube.com/channel/UCuJ0qrx-oNq2hxrUX5IYd9A)


## OpenWest: the next big conference

* [OpenWest - April 10-12 - Sandy, UT](https://openwest.org/)
* [Call for papers extended](https://cfp.openwest.org/)

* [Intercollegiate Capture the Flag](https://ictf.openwest.org/)

Utah OpenSource holds monthly cyber-sparring events with participants from
other schools in the state.  These are held on the last Saturday of the month.

Let's put together a team and jump in!  Contact me for more information.




--------------------------------------------------------------------------------
# What do I need to do on the next assignment?

Awesome stuff!

https://usu.instructure.com/courses/529849/assignments/2581303

Get the [starter code](https://bitbucket.org/erikfalor/cs2610-falor-erik-assn4/)


<Demo ../Assn/4/>



--------------------------------------------------------------------------------
# JavaScript and the DOM

#### Document Object Model (DOM): A browser API which connects a web page's content (HTML) to runnable code

[JavaScript-generated Content Demo](content/index.html)

Q: Where did all of the words come from?
A: From JavaScript code contained in content.js


Q: How might we improve the organization of this code?
A: Put the repetitive bits into a function called `addDiv()`


* https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model
* https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction
* [JavaScript and the Document Object Model](../Readings-and-resources.md#markdown-header-javascript-and-the-document-object-model)
