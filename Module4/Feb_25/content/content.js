// Set the document's title
document.title = "Client-side dynamic pages with JavaScript"


// The addDiv() function creates a div of the specified color ('red', 'yellow',
// 'blue', 'green', or 'black'), and fills it with paragraphs containing text
// passed in as optional string parameters.
var addDiv = function(color) {
    var div = document.createElement('div');
    div.setAttribute('class', `${color} stuff-box shadowed`);
    document.body.appendChild(div);
    
    // Skip over the formal param 'color' by starting my loop at 1
    for (var i = 1; i < arguments.length; i++) {
        var p = document.createElement('p');
        p.textContent = arguments[i];
        div.appendChild(p);
    }
}


// Add content to the red div
addDiv('red',
 "First, take a look at this page's HTML document",
 "(It's the file called index.html)",
 "Where are all of these words comming from?");


// Add content to the yellow div
addDiv('yellow',
    "Then take a look at the stylesheet for this page",
    "(It is a file called 'style.css')",
    "Did you find what you are looking for?");


// Add content to the black div
addDiv('black',
    "You'll never take me alive, copper!");


// Exercise: Do the same for the blue and green divs

// Add content to the blue div
p0 = document.createElement('p');
p0.textContent = "Then have a look at that JavaScript file mentioned at the end";
p1 = document.createElement('p');
p1.textContent = "(This file is called 'content.js')";
var blue = document.createElement('div');
blue.setAttribute('class', 'blue stuff-box shadowed');
blue.appendChild(p0);
blue.appendChild(p1);

// Add content to the green div
p0 = document.createElement('p');
p0.textContent = "Does that finally solve the mystery?";
var green = document.createElement('div');
green.setAttribute('class', 'green stuff-box shadowed');
green.appendChild(p0);


var divs = [blue, green];

for (var d of divs) {
    console.log(d); // basic print debugging
    document.body.appendChild(d);
}


// remove the black div
document.querySelector('.black').remove()
