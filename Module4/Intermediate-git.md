# Table of Contents
* Intermediate git

----------------------------------------------------------------------------
# Intermediate git

Here are some new git commands that will give you more detailed information
about the state and structure of your repository.

### `git help glossary`

* Display a manual page defining bits of git jargon


### Working tree

* The files tracked by the git repository which are presently checked out,
  along with any new or changed files.
* New commits are formed with the `git add` command by computing the
  differences in the contents of the working tree with the most recently
  recorded commit.  This new commit is then permanently recorded by running the
  `git commit` command.


### `git diff`

* Display the difference between your source code files (the working tree) and
  the contents of git's last commit.  This shows you what will be recorded with
  `git add` followed by `git commit`

**Important**: press `q` to quit the diff viewer.



### `git config --global log.decorate true`

* Display extra branch/commit name information in the output of `git log`



### `git log`

* Display the commit history from the current commit back to the genesis of the
  repository.
* Also: `git log --stat` - displays a brief summary of files and their changes
* Also: `git log --patch` - displays the diff for that patch
* Also: `git log --stat --patch` - show all of it at once

**Important**: press `q` to quit the log.



### SHA-1 Object Name

* This *true name* of a commit is a cryptographic checksum of the author's name
  and email address, timestamp, commit message, as well as the checksum of the
  contents of the commit.  This cryptographically-strong digital fingerprint of
  the commit prevents tampering with the history of the repository.
* A commit's SHA-1 object name also incorporates the SHA-1 object name of its
  parent commits, which, in turn, depend upon their parents' checksums, etc.
  Thus, git repositories were blockchains before blockchains were cool.
* This name takes the form of a 40 character-long string of hexadecimal digits
  (0 through 9, A through F), e.g. `f7e8295498512363f5cd0b12459e548ca80f329f`  

You can always refer to commit with this universally unique identifier.
Because sequences of 40 arbitrary characters are hard for humans to remember,
git gives us a few shortcuts, explained below.  Any commit may have multiple
names at once, but it always has at least one SHA-1 object name.

### `HEAD`

* This is always the name of the commit you are on RIGHT NOW.  Analogous to the
  notion of *current working directory* to the command shell.


## Git commands which operate on commit objects

You may use any of the following names in the commands described below when
that command needs you to refer to an *OBJECT*.  Understand that in the
foregoing discussion `OBJECT` refers *only* to git commits.  A file or
directory is *never* an `OBJECT`.

1.  An SHA-1 object name, which may be abbreviated to the first 7 or 8
    characters
2.  A relative reference such as `HEAD`
3.  A tag name
4.  A branch name such as `master`



### `git log OBJECT`

* This form of the `git log` command displays the commit history beginning from
  the commit denoted by OBJECT back to the genesis of the repository.
* Any commits which were added *after* `OBJECT` are not listed in this output.


### `git tag TAGNAME`

* Names the current (HEAD) commit `TAGNAME`.
* The tag sticks to this commit and does not follow the branch along as you add
  commits.


### `git tag TAGNAME OBJECT`

* Grant the name `TAGNAME` to the object referred to by `OBJECT`.
* `OBJECT` can be any object anywhere in your repository.
* A tag is a more human-friendly name for a commit.
* A tag always refers to the same commit, even after new commits are added to
  the repository.
* A tag exists only in your local repository until you push it.


### `git tag -d TAGNAME`

* Remove a tag from a commit.
* This command does not modify or delete the commit itself.



### `git push REMOTE TAGNAME`

* Send the name of a tag along with the commit to which it points to REMOTE
  repository.
* Tags are not pushed unless you explicitly instruct git to push them.  If you
  forget to explicitly push your `Assn2` tag, for instance, we won't know which
  commit to grade.  


### `git push --delete REMOTE TAGNAME`

* Remove a tag from REMOTE repository.  Use this if you tagged the wrong
  commit.


### `git diff OBJECT`
* List the differences between `OBJECT` and `HEAD`.
* In other words, what changes would I need to make to the code as checked-in
  at `OBJECT` to make it become the same as `HEAD`?


### `git checkout OBJECT`
* Make the working tree become identical to the state captured by `OBJECT`.
  Where you are is now `HEAD`.
* This is how you travel back in time with git
* You can return to your previous location with `git checkout -`
* Return to your latest commit on the master branch with `git checkout master`

