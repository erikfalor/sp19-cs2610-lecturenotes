// This is an icky global variable, just begging for trouble
var shutUps = 0;

var shutUpWesley = function () {
    console.log("Sir, I know this may finish me as an acting ensign, but...");
    shutUps++;

    // risky use of a global variable is risky
    console.log("SHUT UP, WESLEY" + "!".repeat(shutUps));
};

// what if I set shutUps to a negative number?


// Solution: IIFE
// https://en.wikipedia.org/wiki/Immediately-invoked_function_expression
(function () {
    var shutUps = 0;

    shutUpWesley = function () {
        console.log("Sir, I know this may finish me as an acting ensign, but...");
        shutUps++;
        console.log("SHUT UP, WESLEY" + "!".repeat(shutUps));
    };
})();


