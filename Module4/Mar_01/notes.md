# CS2610 - Fri Mar 01 - Module 4

# Announcements

## Attention: seniors who are graduating in May 2019

All graduating students are required to complete a short online survey and an
in-person interview with Dan Watson.  Contact Cora Price for help accessing the
survey, which is the first step.


## Next FSLC meeting

### Scheme: the reason your programming language has any redeeming qualities at all

> A language that doesn't affect the way you think about programming,
> is not worth knowing.
>
> -- Alan Perlis

Take a step towards enlightenment by exploring Scheme, a practical and simple
dialect of LISP.  This talk will demystify those concepts functional
programming novices find the most confusing, giving you a whole new way of
thinking about programming.

March 6th, 7pm
ESLC 053






# Topics:
* JavaScript Functions
* Objects in JavaScript
* WAT



--------------------------------------------------------------------------------
# JavaScript Functions

* [Module 4 R&R: Functions](../Readings-and-resources.md#markdown-header-functions)
* [MDN: Functions in JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions)


--------------------------------------------------------------------------------
# Objects in JavaScript

* [Module 4 R&R: Objects in JavaScript](../Readings-and-resources.md#markdown-header-objects-in-javascript)
* [MDN: Introducing JavaScript Objects](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects)



--------------------------------------------------------------------------------
# WAT (5 min)

https://www.destroyallsoftware.com/talks/wat

Try these out in your own browser, and read the explanation behind them:
    http://2ality.com/2012/01/object-plus-object.html

JavaScript is... an *interesting* language.

Solve [common problems ](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Howto) in your JavaScript code

