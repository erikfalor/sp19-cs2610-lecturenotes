// Set the document's title
document.title = "Add and remove divs dynamically"


// Function to add a new div from the '+ div' button
// See https://www.youtube.com/watch?v=EaRrmOtPYTM
var plusDiv = function(button) {

    // find the <form> element
    var form = document.querySelector('form');

    // Create a new <div> in the document, set its color to the selection
    var newDiv = document.createElement('div');
    newDiv.setAttribute('class', 'stuff-box shadowed');
    var words = document.createElement('p');
    newDiv.appendChild(words);

    var color = document.querySelector("#new-div-color").value;
    newDiv.setAttribute('style', 'background-color: ' + color);

    newDiv.setAttribute('onclick', 'this.remove()');

    let newWords = document.querySelector('#new-div-text').value;
    if (newWords !== "")
        words.textContent = newWords;
    else
        words.textContent = "This is a brand-new <div> with default text!";

    document.body.appendChild(newDiv);
}

