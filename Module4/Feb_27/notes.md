# CS2610 - Wed Feb 27 - Module 4

# Announcements

## FSLC "Ricing" Night

Wednesday Night 7pm room 053 in ESLC is our ricing night!

You heard me right, *Ricing*.  I do not mean throwing small grains at wedding.
Or adding rice to all your meals.  I am referring to customizing your machine
to look the way you want it to so you gain street cred.  Lab cred?  You know
what I mean.


## HackUSU March Madness Month Long Mini-Hackathon

Come Create a Project, Eat Food, and Win Prizes!

*Too busy or started too late?*  Create something simple and show up to the
judging for a chance to win an award or raffle. Education will be documented in
judging so do not be afraid if you think you don’t enough, you just might WIN!

| Important Date    | Activity
|-------------------|--------------------
| Monday March 4th  | Opening, team-forming (optional), project working, food
| Monday March 18th | Project working, food
| Monday March 25th | Project working, food
| Monday April 1st  | Judging, awards, food




## DC435 Capture The Flag

Thursday, March 7th @ 7pm

Bridgerland Main Campus - 1301 North 600 West - Room 840

Make sure you bring a laptop with RDP (Remote Desktop Protocol) and/or SSH
clients (e.g. PuTTY) so you can play!

You'll be logging into a Kali Linux box from which to do your hacking.



# topics:
* Correctly submitting your work
* Intermediate git
* JavaScript Events



----------------------------------------------------------------------------
# Correctly submitting your work

Part of participating in this class is following the guidelines for assignment
submissions.  You are allegedly adults now, so you must take up the
responsibility for this.  Moreover, there are just too many of you for me and
the graders to monitor and hand-hold.

From our perspective, a private repository without *read* access looks the same
as no submission at all: no code.  We can't but treat it as though it is not
submitted.

[How a private repo you are not invited to appears](https://bitbucket.org/fadein/top-secret/src/master/)


Clicking those "Add" buttons may seem like a trivial thing for me to penalize
your grade so harshly, but it really makes all the difference.

[Submitting work on Bitbucket](../../Class_Rules.md#markdown-header-your-bitbucket-account)



----------------------------------------------------------------------------
# Intermediate git

[Intermediate git](../Intermediate-git.md)


----------------------------------------------------------------------------
# JavaScript Events

[JavaScript Events](../Readings-and-resources.md#markdown-header-events-and-event-driven-programming)

See demo code in the directory called [dyndivs](dyndivs/)


## Key takeaways:

* Buttons within a `<form>` element default to behaving as a **Submit** button.
  When such buttons are clicked the page will reload.  To cause a button to run
  a JavaScript expression without reloading the page, give the `type` attribute
  the value of `button`:  `<button type="button" onclick="doSomething() />`

* Any DOM element may have an `onclick` attribute; this is not just restricted
  to buttons and other `<input>` elements.  The value of which the `onclick`
  attribute interpreted to be a JavaScript expression.

* Within the JavaScript expression in an `onclick` attribute, the keyword
  `this` refers to the DOM element the `onclick` attribute belongs to.

* The value of an `<input>` element can be retrieved in JavaScript through the
  `.value` property.

* Use `document.querySelector()` to locate `<input>` elements in the DOM.  If
  there is a label associated with them, they will already have an ID attribute.

* There are many events besides mouse clicks that DOM elements may respond to [JavaScript Events](../Readings-and-resources.md#markdown-header-lists-of-events-your-dom-elements-may-respond-to)
