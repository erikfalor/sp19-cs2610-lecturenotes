# 4: Readings and Resources

## Table of Contents

* Using the browser's developer tools
* Useful JavaScript resources
* Arithmetic and math in JavaScript
* Control structures
* Loops
* Functions
* Objects in JavaScript
* Events and Event-Driven Programming
* JavaScript and the Document Object Model
* Miscellaneous Protips



--------------------------------------------------------------------------------
## Using the browser's developer tools

-   [MDN Web Console (Firefox-specific)](https://developer.mozilla.org/en-US/docs/Tools/Web_Console)
-   [MDN Browser Console (Firefox-specific)](https://developer.mozilla.org/en-US/docs/Tools/Browser_Console)
-   [Using the Console (Google Chrome-specific)](https://developers.google.com/web/tools/chrome-devtools/console/)



--------------------------------------------------------------------------------
## Useful JavaScript resources

-   [MDN JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
-   [Document Object Model API](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model)
-   [MDN Solve Common JavaScript Problems](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Howto)



--------------------------------------------------------------------------------
## Arithmetic and math in JavaScript

[MDN Arithmetic Operators](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#Arithmetic_operators)

JavaScript shares its arithmetic operators with the mainstream ALGOL-derived languages you are likely familiar with (C, C++, Java, etc.)

-   Addition `+`
-   Subtraction `-`
-   Multiplication `*`
-   Division `/`
-   Remainder `%`
-   Increment `++`
-   Decrement `--`
-   Exponentiation `**`

JavaScript [represents numbers](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Numbers_and_dates#Numbers) as 64-bit double-precision floating-point values. There is no dedicated integer type in JavaScript. However, there are [bitwise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#Bitwise_operators) operators which convert a 64-bit double into a 32-bit signed integer internally for the computation, but the result is converted back into a 64-bit double. The TL;DR is that you shouldn't try to implement cryptography in this language.

The built-in [Math](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Numbers_and_dates#Math_object) object provides advanced mathematical functions such as .abs(), .sqrt(), .random(), the trigonometric functions, and more.

### Data type conversion

Because JavaScript is a dynamically-typed language, programmers don't declare that a variable will contain only one type of data. It is also possible to combine different types of values using the above operators, often with unexpected effects. There are [rules governing the results](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_Types#Data_type_conversion) of various combinations of types. Misunderstanding these rules is a common source of confusion for new JavaScript programmers. Don't forget that you are never very far away from a high-quality JavaScript REPL. When in doubt, try it out!


--------------------------------------------------------------------------------
## Control structures

if/else blocks in JavaScript have the same familiar look as C/C++/Java

[MDN: Control Flow and Error Handling](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling)


### Variables and Scope

[MDN: Variable Declarations](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_Types#Declarations)

_**JS Protip**_ with regards to variables declared with 'var', { } isn't a scope delimiter in JavaScript

{ } is a scope delimiter in the other languages which JavaScript is trying to look
like. This may confuse you if you have used those other languages (you have).
Said another way, curly braces _don't_ by themselves introduce a new scope as
they do in C/C++/Java.  Historically, JavaScript only had 2 layers of scope:

-   Global Scope - the default
-   Function Scope - only exists within a function body

This is an example of what I mean:

    var count = 1;
    if (count === 3) {
        console.log("count was three");
        var a = count;
    } else if (count === 4){
        var b = count;
        console.log("count was four");
    } else {
        var c = count;
        console.log("count was something else: " + c);
    }

This example just put an identifier in the global scope. After evaluating this
code, which of a, b, or c will have been defined as a global variable?

In 2015 the 'let' keyword was added to JavaScript to enable 'block statement'
scope which will behave more as you expect:

    var count = 1;
    if (count === 3) {
        console.log("count was three");
        let a = count;
    } else if (count === 4){
        let b = count;
        console.log("count was four");
    } else {
        let c = count;
        console.log("count was something else: " + c);
    }


Now none of a, b, or c will be available out in the global scope. The
difference is `let`.

Because `{ }` isn't a scope delimiter in JavaScript as it is in the other
languages which JavaScript is trying to look like, you must remember to use
`let` instead of 'var' to create a variable which will behave as you'd expect
with regards to scope and curly-braces


_**JS Protip**_ There is an important thing called 'the global object'

A lot of people mention it, but none of them will tell you what it is or where
to find it.

Let me save you 20 mintues of Googling. You're using a browser, so it's
`window`.  If you were using Node.JS, the global object is an object named
`global`.



--------------------------------------------------------------------------------
## Loops

### Ordinary `for` loops

Familiar while and do/while loops exist in JavaScript.

Familiar 3-part `for` loops from C/C++/Java work similarly in JavaScript (apart
from that pesky scope thing we just discovered):

    for (let i = 0; i < 5; i++){
        console.log("i is " + i);
    }



### ["for x in y" loop](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration#for...in_statement)

This is a handy loop for dealing with array indices and objects:

    var person = { fname:"Paul", lname:"Ken", age:18, pet:"doggy", 'favorite food!!!:P lol']: 'pizza'};

    for (var x in person){
        console.log(person[x]);
    }

In an array, the value `x` will be the _index_ into the array, and _NOT_ the
value.  Try this code in your console to see whether it does what you expect it
to. Is there a way to write this loop such that the _values_ of the array
literal are displayed in the console?

    for (let x in [10, 11, 12, 13, 14, 15]) {
        console.log("x is ", + x);
    }

[Arrays](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Indexed_collections#Array_object)
are basically "glorified" objects in JavaScript.



### ["for x of y" loop](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration#for...of_statement)

It may more natural to iterate over each element of an array without being
concerned with its index within the collection.  For these situations use the
`for ... of` loop:

    for (let x of [10, 11, 12, 13, 14, 15]) {
        console.log("x is ", + x);
    }



--------------------------------------------------------------------------------
## Functions

[MDN: Functions in JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions)

One of JavaScript's most distinct features is that functions are first-class values. When people speak of something being a "first-class" part of a language, they refer to the fact that such values may be stored in variables, passed to and returned from functions. In fact, functions in JavaScripts are actually Objects, complete with their own properties.

There are many ways to declare functions in JavaScript. I'll focus on the most simple and flexible way, but you are welcome do use the other syntaxes as you wish.

```
var f = function(arg) {
    // code goes here
    return 1337 + arg;
};

> f()
NaN

> f(0)
1337

> f(1337)
2674

> f(1, 2, 3)
1338

> f("hello, world!")
"1337hello, world!"
```

As you can see, functions in JavaScript aren't picky regarding the types or number of parameters given.

The function is defined by the use of the function keyword, which associates the given parameter list with the block of code following it. The function itself is then assigned to the variable f. If I supply a parenthesized parameter list after the name of the function, that _invokes_ the function with that list of parameters.

I say that this method is the most flexible as it enables me to associate a function with a name by assigning it to a variable immediately. In contexts where I pass one function into another function (as a parameter), I can do so by using the variable containing the function. Or, I can directly pass the function without first assigning it a name. When used in this way we say that the function is "anonymous". This is a very common and popular pattern in JavaScript, so spend some time with it as it will come up often.

```
var add = function(a, b) {
    return a + b;
}

var sub = function(a, b) {
    return a - b;
}

var doMath = function(f, a, b) {
    return f(a, b);
}

> doMath(add, 2, 3)
5

> doMath(sub, 2, 3)
-1

doMath(function(a, b) { return a ** b; }, 2, 3)
8
```

### Scope of variables within functions

As explained above, pre-2015 the JavaScript language had only two scopes for
variables: *global* and *function*.  Back then developers still needed
variables that were safely constrained within scoped sandboxes, and had to
resort to clever tricks to do so.  

Today, developers may declare a variable with the `let` keyword instead of
`var` to take advantage of nested scopes, however, you must be prepared to
encounter the common JavaScript idiom used pre-2015.

This trick is known as a **lexical closure**, which is another another idea
borrowed from functional programming languages such as Scheme.

Here is an example where a lexical closure is used to implement the behavior of
a **static** variable, another feature which the JavaScript language lacks
explicit support for, but arguably doesn't need to support because of closures.

A variable that is out-of-scope at the REPL can still be accessed by a function
which was defined within that scope.  Because functions are first-class values,
we can create a function within another function and assign it to a global
variable.  It has access to variables defined in the same function scope that
it was created in, and yet be called by the name of the global variable it is
assigned to.


First, the counter-example that proves the need for this convoluted-sounding
concept.  The behavior of the function `shutUpWesley()` is dependent upon the
value of a global variable named `shutUps`.


```
// This is an icky global variable, just begging for trouble
var shutUps = 0;

var shutUpWesley = function () {
    console.log("Sir, I know this may finish me as an acting ensign, but...");
    shutUps++;

    // risky use of a global variable is risky
    console.log("SHUT UP, WESLEY" + "!".repeat(shutUps));
};
```

We all agree that globals are bad, mmm'kay.  What if some other function
changes `shutUps` to an unexpected value?  What if `shutUps` is set to a
negative number?

To protect `shutUps` from external meddling, we can hide it within a function's
scope.  In this formulation, only `shutUpWesley()` can modify this important
value:

```

(function () {
    var shutUps = 0;

    shutUpWesley = function () {
        console.log("Sir, I know this may finish me as an acting ensign, but...");
        shutUps++;
        console.log("SHUT UP, WESLEY" + "!".repeat(shutUps));
    };
})();

```

This idiom is known as [Immediately invoked function expression] (https://en.wikipedia.org/wiki/Immediately-invoked_function_expression).

There are two functions going on here:

* An anonymous function on the outside, which we immediately invoke
* An inner function defined and assigned to the global variable `shutUpWesley`

I create an outer function with no name and immediately call it.  After it is
finished I have no need to call it again, so it needs no name.  Its only
purpose is to lend its private *function scope* to my variable `shutUps`.


--------------------------------------------------------------------------------
## Objects in JavaScript

[MDN: Introducing JavaScript Objects](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects)

An "Object" in JavaScript resembles Dictionaries from Python, both as regards
its syntax and semantics:

    var myObject = {
        uno: 1,
        dos: ['two', 'deux'],
        tres: { another_object: true },
        cuatro: function() { console.log("This is a function stored as a property within an object"); },
        'the number five': function() { console.log("Well, this is awkward"); },
    };

The "key" of the Python dictionary is called a "property" in JavaScript. Unlike in Python, the property does not need to be surrounded with quote marks. Like a Python Dictionary, property-value pairs are joined with a colon ':', and pairs are delimited with commas.

### Accessing properties of an object

There are two ways to refer to an object's property by name.

#### Object-Oriented syntax
```
> myObject.uno
1

> myObject.cuatro()
This is a function stored as a property within an object

> myObject."the number five"()
Uncaught SyntaxError: Unexpected string
```

Most programmers prefer this style because it looks familiar and is easier to type. It doesn't work when the name of property contains characters which are _not_ valid in an identifier such as whitespace, +, -, \*, /, etc.



#### Dictionary syntax

```
> myObject['uno']
1

> myObject['cuatro']()
This is a function stored as a property within an object

> myObject['the number five']()
Well, this is awkward
```
This way may be less easy to type, but doesn't have the limitations of the OO syntax.



#### Object.keys( ... ).length

How many things are in my object? Your object won't have a .length property (unless you put one in yourself). The way to programmaticaly determine this is to use the Object.keys() method to create a list of the property names of your object, then use that lists' .length property:
```
obj = { 'a' : 1, 'a' : 1, 'a' : 1, 'a' : 1, 'a' : 1, 'a' : 1, 'a' : 1 }
Object.keys(obj).length


obj = { 'a' : 1, 'b' : 1, 'c' : 1, 'd' : 1, 'e' : 1, 'f' : 1, 'g' : 1 }
Object.keys(obj).length
```



--------------------------------------------------------------------------------
## Events and Event-Driven Programming

[MDN Events Reference](https://developer.mozilla.org/en-US/docs/Web/Events)

Elements in the DOM may respond to some sort of interaction by calling a JavaScript function. This function is known as a "callback"

### Callback Functions

A function defined to be called automatically by the runtime environment (read: browser) at some future point.

For instance, moving the slider in our homework assignment should cause a change to occur on the webpage. We don't have control over _when_ this change will occur, so we can't write imperative, sequential code that will cause this change to happen at the right time since we don't know when that right time will be until the user moves the slider. Instead, the browser calls the function for us when the time is right.

This all means that we never write a line of code which calls our _callback_ function. Since we don't write a function call, we don't get to pass any parameters to our callback function. Since we aren't passing parameters to this function, does it not get any parameters at all?

It turns out that the browser does pass one argument to the callback, an object which conforms to the '[Event](https://developer.mozilla.org/en-US/docs/Web/API/Event)' interface. This means that our function must be written to accept a single argument, and that this argument will be contain all relevant information about the user's interaction as collected by the browser.

### Configuring callback functions in Javascript

These examples are fairly straightforward:

-   Write a callback function which accepts an Event object as a parameter
-   Assign the callback function to the appropriate attribute of a DOM node

[MDN Example: click event](https://developer.mozilla.org/en-US/docs/Web/Events/click#Example)

[MDN Example: change event](https://developer.mozilla.org/en-US/docs/Web/Events/change#Examples_for_user_input)

### Configuring callback functions in HTML

HTML elements can take an attribute corresponding to the type of event you want to give behavior to. The name of the attribute is the name of the event from the preceding lists prefixed with "on" (`onclick`, `onchange`, `onhover`, `oninput`, etc.). To connect a callback function to an element simply write a snippet of JavaScript code between the quote marks.

**HOWEVER**, now _we_ are writing the code which calls the function, and thus, we can pass something besides an event object.

Within this little snippet of JavaScript code embedded within the value of the HTML attribute we can use the identifier `this` to refer to the DOM element which is experiencing the event. If you want to do what the browser would do in this situation, you can use the identifier `event`.

```
<script type="text/javascript">
    function divClick(domElement) {
        alert("The <" + domElement.localName + "> says it's Hammer Time!");
    }
</script>

<div id="clicky" onclick="divClick(this)">
    <h2 onclick="divClick(this)">U can't touch this!</h2>
    <ul onclick="divClick(this)">
        <li onclick="divClick(this)">My, my, my, my</li>
        <li onclick="divClick(this)">Music hits me so hard</li>
        <li onclick="divClick(this)">(That it) makes me say "Oh, my Lord:</li>
        <li onclick="divClick(this)">Thank you for blessing me</li>
        <li onclick="divClick(this)">With a mind to rhyme and two hyped feet".</li>
        <li onclick="divClick(this)">It feels good when you know you're down,</li>
        <li onclick="divClick(this)">A superdope homeboy from the Oaktown.</li>
        <li onclick="divClick(this)">And I'm known as such.</li>
        <li onclick="divClick(this)">And this is a beat, uh, you can't touch.</li>
        <li onclick="divClick(this)">I told you, homeboy: Can't touch this.</li>
        <li onclick="divClick(this)">Yeah, that's how we livin', and ya know: Can't touch this.</li>
        <li onclick="divClick(this)">Look in my eyes, man: Can't touch this.</li>
        <li onclick="divClick(this)">Yo! Let me bust the funky lyrics.</li>
    </ul>
</div>
```


### Lists of events your DOM elements may respond to

Some DOM elements are designed for user interaction and are natural launching points for callback functions. Such elements include <input>, <button>, and <textarea>.

-   [MDN Button Input](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/button)
-   [MDN Checkbox Input](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/checkbox)
-   [MDN Color Input](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/color)
-   [MDN Number Input](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/number)
-   [MDN Range Input](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/range)
-   [MDN TextArea](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/textarea)

However, _any_ visible DOM element can be made to respond to user interaction.  For example, you may create your own custom buttons from <div> elements.

There are also _many_ other events besides those for user interaction. The sheer number of these can be overwhelming. For our purposes, in the following resource you will want to scroll down on the left sidebar and expand the node labeled "HTML DOM Events" to find the list of events we will be using for the next few assignments.

[MDN Events](https://developer.mozilla.org/en-US/docs/Web/Events)


--------------------------------------------------------------------------------
## JavaScript and the Document Object Model

The global `document` object in JavaScript is your gateway to the DOM.

The `document` object corresponds to the <html> node of your HTML document. Through it you can access every node in the <body>, set the document's title, access the stylesheets, etc.

### Operating on individual nodes within a document

These functions all return a reference to an element in the DOM. Below the name `DOMelement` refers to any HTML element which has been stored into a variable:

-   `document.querySelector()`
-   `DOMelement.querySelector()`
-   `document.querySelectorAll()`
-   `DOMelement.querySelectorAll()`
-   `document.createElement()`
-   `DOMelement.createElement()`

In this way you may put a new HTML tag (hereafter called a 'node') into a JavaScript variable and operate on it.

While you create a new node with the `.createElement()` method, it will not appear on the webpage until it is attached to the HTML tree. One way to put a node into the tree is to add it as a child node to an already extant node:

`node1.appendChild(node2)` - makes the node referred to by `node2` become a child of `node1`

We can do this interactively with the developer tools of your browser.

Nodes have a property called `.parentElement` which refers to their parent. You can find an ancestor by following a node's `.parentElement` up the tree as far as desired.

### Removing nodes from the DOM tree

Once you have a reference to a node you may also remove it from the DOM tree by calling its `.remove()` method:

> var die = document.querySelector('#userNameInput')
> die.remove()

Alternatively, you may remove a node by using its parent's `.removeChild()` method, passing in a reference to the node to be deleted:

> // a long-winded way to do the same thing as above:
> var die = document.querySelector('#passwordInput')
> die.parentElement.removeChild(die)


### How do I copy a Node?

Let's try to copy a child node from one colored div into another, so that there are *two* identical pieces of text in our webpage.

Keep in mind that the methods explained above return a *reference* to a node.

This means that if we call `DOMelement.appendChild()` on the result of one of the above calls, that node is _moved_ within the DOM structure, and not copied. If we want to create a new node with the same contents we must use `DOMelement.cloneNode(true | false)` to obtain a reference to a _copy_ of the node.

Pass a [truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) value to cause `.cloneNode()` to make a _deep copy_ of the node, meaning that all of its children are also cloned, recursively.




### How do I change a node's attributes?

Use a getter/setter interface on a node object to set and retrieve attributes:

-   `DOMelement.setAttribute()`
-   `DOMelement.getAttribute()`

To set an attribute with `.setAttribute()`, pass two strings: the name of the attribute and its value. The value of the 2nd string is exactly what you'd put between the quote marks in an HTML document.

`.getAttribute()` takes one argument, the name of the attribute you wish to retrieve.


--------------------------------------------------------------------------------
## Automatic Semicolon Insertion

Like Java, C++ and the other languages bearing syntactic similarity to
JavaScript, semicolons are required at the end of each statement.

Unlike those other languages, the JavaScript compiler/interpreter doesn't
require _you_ to write them.  Instead it will _guess_ where it thinks they
ought to go and _silently_ insert them for you.

To maintain your sanity (and because you're not a lazy slob) you should just
put `;` at the end of each statement even though the language doesn't make you.
You'll also encounter situations in JavaScript where you **must** put a `;` at
the end of an expression even though it really doesn't look like it is needed.
Leaving it off will result in frustrating, hard-to-track-down bugs that will
stump even experienced JavaScript programmers.


#### SILLY DEMO: see if you can spot the missing semi-colon
```
var shutUpWesley = function () {
    console.log("Sir, I know this may finish me as an acting ensign, but...")
}

(function() {
    shutUpWesley()
})()
```

_**JS Protip**_ Use `typeof` to determine the type of a value

`typeof` is not exactly a function, so you don't need to use () with it:

```
typeof true
typeof 1
typeof 1.0
typeof "1.0"
typeof { 'a': 1 }
typeof NaN
```
--------------------------------------------------------------------------------
## Miscellaneous Protips

_**JS Protip**_ Using the HTML `script` element properly

You can use the script tag to embed JavaScript source code directly into an HTML document.
```
<script type="text/javascript">
    // this is JavaScript source code
    window.globals = "are bad";
    var another_global = 1 + "1";
</script>
```
You may also use the script element's src="" attribute to link to an external JavaScript file:
```
<script src="fib.js"></script>
```
Note that when you use the `script` tag to link to an external script, you
still need to add the closing tag.


_**JS Protip**_ The `type=""` attribute of `script` is optional in HTML5.
Its default value is "text/javascript".


_**JS Protip**_ use JavaScript's `alert()` function to pop up a window
containing a message.  Developers once had to (ab)use this faculty to display
debug messages because there wasn't a console or a log file to send them to.
Because this is super annoying, your browser may stop showing messages if it
thinks you are spamming the user.



_**JS Protip**_ Because JavaScript didn't start out with a debugging console it
didn't need a `print()` function. Nowadays browsers have a console, but
JavaScript still has no `print()` function. Instead, use `console.log()` to
print messages to the console.


