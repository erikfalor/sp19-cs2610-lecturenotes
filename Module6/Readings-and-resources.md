# 6: Readings and Resources

## Table of Contents

* Vue.js - The Progressive JavaScript Framework
* Vue directives for use in HTML
* Key properties in the Vue configuration object


--------------------------------------------------------------------------------
## Vue.js - The Progressive JavaScript Framework

Vue.js is an open source client-side web application framework.  It is smaller and leaner than larger frameworks such as React and Angular.

* [Vue.js Guide](https://vuejs.org/v2/guide/) - contains a very good six-minute tutorial video

* [Vue.js API documentation](https://vuejs.org/v2/api/)

* [Vue.js App Lifecycle Diagram](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)

* [Vue.js Event handling](https://vuejs.org/v2/guide/events.html) - How to use `on` events with inline JavaScript and Vue methods

* [Vue.js 'on' event handler shorthand](https://vuejs.org/v2/guide/syntax.html#v-on-Shorthand) - `v-on:click` === `@click`


--------------------------------------------------------------------------------
## Vue directives for use in HTML

* [Vue.js Guide: Directives](https://vuejs.org/v2/guide/syntax.html#Directives)
* [Vue.js API: Directives](https://vuejs.org/v2/api/#Directives)


### The Vue App

Vue connects a JavaScript object to a segment of the DOM.  Together, this is
called the Vue app.  The developer identifies a DOM element to be the "root" of
the Vue app by giving it an `id` attribute, which is passed to the `Vue()`
constructor function through its configuration argument's `el` property.  The
`el` property's value is a CSS selector string.

    <div id="my-app">
        <h2>This is my Vue App</h2>
    </div>

    <script>
        my app = new Vue({
            el: '#my-app',
        });
    </script>


### Templates

Like Django's templates, dynamic content can be filled-in with code by writing
JavaScript expressions between the delimiters `{{` and `}}`.

The data that is used for substitution within the delimiters comes from
properties of the Vue object connected with your app.


### Binding attributes

Vue can not only supply dynamic content through templates within elements, but it can
also dynamically create and populate attributes on HTML elements.  This is done
not with templates but with the `v-bind` directive.

The syntax is `v-bind:ATTRIBUTE="expression"` where `ATTRIBUTE` is any HTML
attribute of your choosing, and `expression` is a JavaScript expression.

If, for some reason, you wanted to use your API key as the `id` of a `div`, you
could write something like this (be aware that for brevity's sake this is an
incomplete example):

    <div v-bind:id="apikey">
        <p>Bet you can't guess my API key!</p>
    </div>

    <script>
        my app = new Vue({
            data: {
                apikey: 'c3VwZXIgc2VjcmV0Cg=='
            },
        });
    </script>


The `div` would be rendered as

    <div id="c3VwZXIgc2VjcmV0Cg==">
        <p>Bet you can't guess my API key!</p>
    </div>


Special faculties are available for binding the `class` and `style` attributes:

* [Vue.js Guide: Class and Style Bindings](https://vuejs.org/v2/guide/class-and-style.html)

For instance, consider this (again, incomplete) example:

    <div v-bind:class="classes">
        <p>A terribly classy div</p>
    </div>

    <script>
        my app = new Vue({
            data: {
                classes: ['col-sm', 'text-capitalize', 'font-weight-bold'],
            },
        });
    </script>


After the Vue app is initialized, the list of class names stored in the `data`
property is expanded into a space-separated list.  The end result is that the
`div` is rendered as:

    <div class="col-sm text-capitalize font-weight-bold">
        <p>A terribly classy div</p>
    </div>


### Conditional Rendering

* [Vue.js Guide: Conditional Rendering](https://vuejs.org/v2/guide/conditional.html)

These directives take as their value a JavaScript expression.  The truthiness
of that expression controls whether the element containing the directive is
rendered.

These directives *must* be used together, and in this order.  It is an error to
use `v-else` where it does not immediately follow `v-if` or `v-else-if`.

1. `v-if`
2. `v-else-if`
3. `v-else`



### List Rendering

* [Vue.js Guide: List rendering](https://vuejs.org/v2/guide/list.html)

The `v-for` directive repeats a segment of HTML based upon the contents of a
collection.  The value to `v-for` is a JavaScript expression in the form of
`Identifier in Collection`, where `Collection` is the name of a JavaScript
Array in scope to the Vue app.  `Identifier` is a variable name which becomes
bound to each element of `Collection` in turn, for use in template expansion
within the repeated element.



### Computed Properties

* [Vue Guide: Computed Properties](https://vuejs.org/v2/guide/computed.html#Computed-Properties)

In-template expressions are very convenient, but they are meant for simple
operations.  Putting too much logic in your templates can make them bloated and
hard to maintain.

Create computed properties by adding properties to a sub-object called `computed`.

* [Computed Properties Basic Example](https://vuejs.org/v2/guide/computed.html#Basic-Example)

	var vm1 = new Vue({
	  el: '#example',
	  data: {
		message: 'Hello'
	  },
	  computed: {
		// a computed getter
		reversedMessage: function () {
		  // `this` points to the vm instance
		  return this.message.split('').reverse().join('')
		}
	  }
	})




* [Computed getters and setters](https://vuejs.org/v2/guide/computed.html#Computed-Setter)

You may also create getters and setters for properties like so:

	var vm2 = new Vue({
		// ...
		data: {
            firstName: '',
            lastName: '',
		},
		computed: {
		  fullName: {
			// getter
			get: function () {
			  return this.firstName + ' ' + this.lastName
			},
			// setter
			set: function (newValue) {
			  var names = newValue.split(' ')
			  this.firstName = names[0]
			  this.lastName = names[names.length - 1]
			}
		  }
		}
	});

* [Vue API: Computed Properties](https://vuejs.org/v2/api/#computed)
