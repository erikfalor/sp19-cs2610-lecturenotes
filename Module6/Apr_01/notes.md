# CS2610 - Mon Apr 01 - Module 5

# Announcements

## IDEA Surveys - Rare Extra Credit Opportunity

The regular three week long window for IDEA Student Rating of Instruction (SRI)
opens next week.  You should have already received a personalized email with a
link to complete your survey.

Your IDEA feedback is very important to me, and each semester I take many
useful suggestions and incorporate them into my future courses.  Much of what I
do I owe to your suggestions.  The more input I get from you the better I am
able to improve as an instructor.  My goal is to reach 80% participation.

To that end I am offering 25 points of sweet, sweet *extra credit* for your
response.  Your responses remain anonymous, and I will not even see them until
after finals week.  The extra credit is automatically applied to your grade by
Canvas within 48 hours of your taking the anonymous survey.



## SDL/USU Technical Lecture 
Please join us for the next SDL/USU Technical Lecture Series at 4:30 pm on
Tuesday, April 2 in ENGR 201. SDL Mechanical Designer Greg Hopkins will cover
design principles of mechanical components used in space.

Free pizza after the lecture. More information about this exciting event is
available on [our website](https://engineering.usu.edu/events/sdl-usu-lecture-series).
See you there!




## DC435 - NMAP 101 by @Santiago

One of the easiest ways an attacker can discover vulnerabilities on your
network is by doing what is called a port scan. Come learn how to do a port
scan and/or a little more.

Thursday, April 4th @ 7pm
Bridgerland Main Campus - 1301 North 600 West - Room 840



## Class Cancelled next Friday, Apr 12 for OpenWest conference



# Topics:
* The Vue.js Front-end Framework
* How to "install" Vue into your webpage


--------------------------------------------------------------------------------
# [The Vue.js Front-end Framework](../Readings-and-resources.md)

> Vue (pronounced /vjuː/, like view) is a progressive framework for building
> user interfaces.  Unlike other monolithic frameworks, Vue is designed from
> the ground up to be incrementally adoptable. The core library is focused on
> the view layer only, and is easy to pick up and integrate with other
> libraries or existing projects.  On the other hand, Vue is also perfectly
> capable of powering sophisticated Single-Page Applications when used in
> combination with modern tooling and supporting libraries.


#### Front-end Framework

Pertaining to the web client


#### Progressive Framework

Use as much or as little as you need right now; you can go deeper later on as needed.


#### Monolithic Framework

Use the entire thing or don't use it at all.  Does not play well with others. 


Vue is an alternative to bigger frameworks such as React or Angular.  I chose
it for this class because it is simpler to introduce in a short time frame, and
you don't need a broad foundation in many other concepts to be able to use it.


--------------------------------------------------------------------------------
# How to "install" Vue into your webpage

## Link to the latest version of Vue.js

Use this URL in a `script` element: https://cdn.jsdelivr.net/npm/vue


## Link to a specific version of Vue.js

Include the version number of Vue, like this: https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js




--------------------------------------------------------------------------------
# Vue Templates

* [Declarative Rendering](https://vuejs.org/v2/guide/#Declarative-Rendering)

* [Template Syntax](https://vuejs.org/v2/guide/syntax.html)

Like Django, you can use the tokens `{{` `}}` to create template-like constructs which are filled in by the Vue app.



--------------------------------------------------------------------------------
# Choosing what gets rendered in a template

* [Conditional Rendering](https://vuejs.org/v2/guide/conditional.html)
