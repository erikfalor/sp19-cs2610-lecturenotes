import time

from django.shortcuts import render
from django.http import JsonResponse
from random import uniform

from . import rand

# Simulate API calls that can take
SLEEPYTIME = 4


def index(request):
    return render(request, 'charactersheet/characterSheet.html')


def gender(request):
    time.sleep(uniform(0, SLEEPYTIME))
    return JsonResponse(
            {
                "gender": rand.gender(),
            })


def name(request):
    time.sleep(uniform(0, SLEEPYTIME))
    gender = request.GET['gender']
    return JsonResponse(
            {
                "first": rand.firstName(gender),
                "last": rand.lastName(),
                "gender": gender,
            })


def stats(request):
    time.sleep(uniform(0, SLEEPYTIME))
    return JsonResponse(
            {
                "str": rand.stat(),
                "dex": rand.stat(),
                "chr": rand.stat(),
                "int": rand.stat(),
                "wis": rand.stat(),
                "con": rand.stat(),
            })


def attribs(request):
    time.sleep(uniform(0, SLEEPYTIME))
    gender = request.GET['gender']
    return JsonResponse(
            {
                "attrs": [
                    ["race", rand.race()],
                    ["age", rand.age()],
                    ["height", rand.height()],
                    ["hair", rand.hair(gender)],
                    ["eyes", rand.eyes()],
                ]
            })
