// Vue.js guide
// https://vuejs.org/v2/guide/

// Vue.js API documentation
// https://vuejs.org/v2/api/


// The Vue.js application configuration object
var app = new Vue({

    // Override Vue.js's delimiters so they don't clash with Django's.  I must
    // do this because I'm serving my HTML through Django's template mechanism,
    // and I don't want it to try to fill-in template variables meant for
    // Vue.js
    delimiters: [ '[%', '%]' ],

    // Identify the DOM element this instance of Vue will be in charge of
    el: '#character-sheet',

    // This object holds hard-coded data.
    // This data may be changed by the app at a later time.
    data: {
        character: false,
        attribs: false,
        stats: false,
    },

    // This object holds functions which can be used in templates like values,
    // but whose values automatically change when other values in the system
    // change.
    //
    // You could put this logic into Vue's templates; unlike Django's templates
    // you can execute arbitrary JavaScript in a Vue template.  However, this
    // is considered poor design because you are mixing logic with
    // presentation.  Strive to make your Vue templates simple and declarative.
    //
    // It is best to put complicated logic into a computed property and use
    // them in a declarative fashion.
    computed: {
        // this must be shortcut syntactic sugar for declaring a named function
        hitpoints () {
            return this.stats.con * this.stats.str;
        },
        magicpoints () {
            return this.stats.wis * this.stats.int;
        },

        // the old-fashioned way
        cleverpoints: function () {
            return this.stats.dex * this.stats.chr;
        },

        characterGender () {
            if (this.character.gender == "M") {
                return "Male";
            }
            else if (this.character.gender == "F") {
                return "Female";
            }
            else {
                return "Undisclosed";
            }
        }
    },

    // This isn't a real hook.
    // I just put the original broken code here so you can compare it to the
    // correct, working code in the `created` hook below.
    theOrignalBrokenCode () {
        // These four fetch calls all fire off at the same time, but resolve in
        // a random order.
        //
        // The call to the 'gender' API *ought* to happen first, as the `name`
        // and `attribs` both depend upon knowing the gender of the new character.
        fetch(`${document.location.href}gender`)  // this function returns a Promise
            .then(response => response.json())    // this function returns a Promise
            .then(json => {                       // this function doesn't return anything
                console.log("This is the GENDER API callback");
                console.log(json);
                this.character = json; });

        fetch(`${document.location.href}name?gender=${this.character.gender}`)
            .then(response => response.json()) // The return value of this call to .this() is a Promise
            .then(json => { // at this line of code `json` is now a JavaScript object
                console.log("This is the NAME API callback");
                console.log(json);
                // At this point in the program, `this` refers to the object
                // created up in the 'data' property
                let tempGender = this.character.gender;
                this.character = json;
                this.character.gender = tempGender;
            });


        fetch(`${document.location.href}attribs?gender=${this.character.gender}`)
            .then(response => response.json() ) // The return value of this call to .this() is a Promise
            .then(json => { // at this line of code `json` is now a JavaScript object
                console.log("this is the ATTRIB API callback");
                console.log(json);
                this.attribs = json;
            });

        fetch(`${document.location.href}stats`)
            .then(response => response.json() ) // The return value of this call to .this() is a Promise
            .then(json => { // at this line of code `json` is now a JavaScript object
                console.log("This is the STATS API callback");
                console.log(json);
                this.stats = json;
            });
    },

    // `created` is a life-cycle hook.  Life-cycle hooks, if defined, are
    // called by Vue.js at various points in time.  All lifecycle hooks are
    // called with their `this` context pointing to the Vue instance invoking
    // it.
    //
    // https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram
    //
    // If defined, Vue.js calls the created hook when this Vue.js app is created.
    created () {
        // These two fetch calls all fire off at the same time, but resolve in
        // a random order.
        fetch(`${document.location.href}gender`)  // this function returns a Promise
            .then(response => response.json())    // this function returns a Promise
            .then(json => {                       // NOW this function returns a Promise, too
                console.log("This is the GENDER API callback");
                console.log(json);
                this.character = json;

                // fetch() returns a Promise, a fact I can take advantage of to
                // ensure that the call to the `name` API happens only after
                // this character's gender is chosen.
                return fetch(`${document.location.href}name?gender=${this.character.gender}`) })
            .then(response => response.json()) // The return value of this call to .this() is a Promise
            .then(json => { // at this line of code `json` is now a JavaScript object
                console.log("This is the NAME API callback");
                console.log(json);
                // At this point in the program, `this` refers to the object
                // created up in the 'data' property
                let tempGender = this.character.gender;
                this.character = json;
                this.character.gender = tempGender;
                return fetch(`${document.location.href}attribs?gender=${this.character.gender}`) })
            .then(response => response.json() ) // The return value of this call to .this() is a Promise
            .then(json => { // at this line of code `json` is now a JavaScript object
                console.log("this is the ATTRIB API callback");
                console.log(json);
                this.attribs = json;
            });

        // The `stats` API doesn't depend upon the character's gender, and so
        // may still happen on its own.
        fetch(`${document.location.href}stats`)
            .then(response => response.json() ) // The return value of this call to .this() is a Promise
            .then(json => { // at this line of code `json` is now a JavaScript object
                console.log("This is the STATS API callback");
                console.log(json);
                this.stats = json;
            });
    },
});
