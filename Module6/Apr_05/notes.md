# CS2610 - Fri Apr 05 - Module 6


# Topics:

* Your Recipe for Assn 6
* Computed Properties
* Chaining Promises


----------------------------------------------------------------------------
# Your Recipe for Assn 6

1. Get your own API keys from both services
2. Test a URL in browser to fetch your IP addr's own geolocation data from ipstack
3. Write JavaScript to perform this fetch
4. Test a URL in browser to look up your 5-day/3-hour weather forecast based
   upon the latitude & longitude found in the previous step
5. Write JavaScript to perform this fetch
6. Tie this all together around a simple webpage which displays these results
7. Vue-ify it


## Vue crash course video

It is well worth your time to watch and follow along with this video.  You may
find it on the Vue.js guide:

* [Vue Guide](https://vuejs.org/v2/guide/)





----------------------------------------------------------------------------
# Computed Properties

While you can put rather complex JavaScript expressions within Vue's template
brackets `{{`, `}}`, it is not considered good practice to do so.

Computed Properties offer a better place to put your complex (and reusable) code.

../Readings-and-resources.md#markdown-header-computed-properties

See the [code example](todo.html)



----------------------------------------------------------------------------
# Chaining Promises

Return a call to `fetch()` from within a `.then()` continuation to chain
dependent requests linearly instead of nesting/indenting deeper.


See the example in the directory charactersheet/.  Instructions for installing
this Django app into your own Django project are found in the
[README file](charactersheet/README).
