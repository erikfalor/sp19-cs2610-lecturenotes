# CS2610 - Wed Apr 03 - Module 6

# Announcements

## FSLC
Wednesday, April 3rd
7pm - ESLC Room 053

Building robots isn't easy, but thanks to open source software like ROS (Robot
Operating System), it's a bit less hard than it could be.  Come learn how FOSS +
Linux can help you build your first robot.



## DC435 - NMAP 101 by @Santiago
Thursday, April 4th @ 7pm
Bridgerland Main Campus - 1301 North 600 West - Room 840

One of the easiest ways an attacker can discover vulnerabilities on your
network is by doing what is called a port scan. Come learn how to do a port
scan and/or a little more.



## Class Cancelled next Friday, Apr 12 for OpenWest conference



# Topics
* Special Guest Andrew Jouffray: Alexa Skills, APIs and JSON 
* Interactivity and Vue
* Lifecycle hooks: Arrange to fetch data from an API at the right time



----------------------------------------------------------------------------
# Special Guest Andrew Jouffray: Alexa Skills, APIs and JSON 

Andrew has been working on a project to connect an Echo Dot, Amazon's Alexa
service, and the Crestron classroom automation into a unified voice-controlled
system.

All of it is controlled through APIs which pass JSON data back and forth; the
same JSON that you're becoming conversant with.

See attached files

* [AlexaSkills/IntentRequest.json](AlexaSkills/IntentRequest.json)
* [AlexaSkills/LaunchRequest.json]()



----------------------------------------------------------------------------
# Interactivity and Vue

Today we made my TODO list more interactive.  I encourage you to play with the
code in the [attached file](todo.html) as you learn how to use Vue.


As with vanilla HTML, you may write in-line JavaScript code within an onclick
attribute.  To make this code Vue-aware, prepend `v-on:` to the name of the
event you wish to listen for.

As a shorthand, you may write `v-on:click` as `@click`

* [@click shorthand syntax](https://vuejs.org/v2/guide/syntax.html#v-on-Shorthand)



## Where may I put executable code within my Vue object?

Define functions within the `methods` property of the Vue configuration object.


* [Vue API guide: Methods](https://vuejs.org/v2/api/#methods)
* [Listening to click events](https://vuejs.org/v2/guide/events.html#Methods-in-Inline-Handlers)


## Associating a DOM element with an element of a JavaScript Array

I used a special form of Vue's `v-for` loop to get access to the *index* of
each item in my `todos` array, along with the item itself.  Today's demo code
demonstrates my using the array index as part of the content of the list
(template) as well as inserting the data into each `li` element by way of a
custom HTML attribute which is prefixed with `data-`.

* [Using data attributes](https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes)

I used Vue's `v-bind` directive to create a new attribute at runtime, and
populate it with computed data.

See this module's [Readings and Resources](../Readings-and-resources.md) for
guidance about using `v-bind` in your project.


----------------------------------------------------------------------------
# Lifecycle hooks: Arrange to fetch data from an API at the right time

In Assignment 5 you automatically fetched data from Quandl's API as your app
was loading.  Vue gives you control over when certain things happen in your app
with respect to its initialization process.

* [Lifecycle Diagram](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)

You can arrange for the IP address geolocation API call and the call to
OpenWeatherMap's API to occur as your Vue application is being created by the
browser by putting this code into a property of the Vue configuration object
called `created()`.

    new Vue({
        created () {
            ...
        },
    });
