﻿# Class Rules _(how to succeed in my classroom)_


## Table of Contents

* Your Bitbucket account
* Assignment Submissions
* Examinations
* Behavior and Professional Conduct
* Canvas Announcements
* Be The Designated Questioner (5% of your final grade)
* In-Class Learning Activities and Online Discussions (5% of your final grade)
* Asking For Help



--------------------------------------------------------------------------------
## Your Bitbucket account

Unless otherwise stated all assignments will be submitted to
[Bitbucket](https://bitbucket.org) in the form of *private* git repositories.
Use your .edu email address when you sign up to Bitbucket to get an educational
account which has the benefit of unlimited private repositories.

You can upgrade an ordinary Bitbucket account into an educational plan by
simply changing your primary email address to one in the .edu domain.

[What are the guidelines for academic licenses?](https://confluence.atlassian.com/bitbucket/what-are-the-guidelines-for-academic-licenses-296094528.html)


### Provide access to your private repositories

The instructor and TAs must be given _read_ access to your private repository
so that it may be graded.  If you do not give us _read_ access, it is the same
as not submitting anything at all.  Inaccessible repositories are treated as
missing submissions and get 0% credit.


### Do not push your work to public repositories

To prevent academic dishonesty, submissions made to public repositories are
unacceptable and will receive zero points.  It would please me for you to use
your course work as part of your online portfolio, but I must ask that you wait
until after the semester has concluded to publicize your work.



--------------------------------------------------------------------------------
## Assignment Submissions

*We follow an aggressive schedule of an assignment every other week*

* It is *your* responsibility to submit work before the due date.
* Some assignments build upon previous submissions.  It is on you to keep up.
* It is your job to convince the grader your submission is complete and
  correct.


### What To Submit

*Submit to Canvas the URL to your private git repository*

*   Unless otherwise specified, submit to Canvas a URL to a private git
    repository hosted on your Bitbucket account.
*   Repositories on other git services (i.e. GitHub, GitLab) *are not* accepted.
*   The instructor and TAs must be given _read_ access to your private
    repository, otherwise your submission receives zero points.
*   Submissions made to public repositories are unacceptable and will receive
    zero points.

To prevent an accidental late submission I recommend that you submit your git
URL as soon as you can access the assignment on Canvas.


### How To Submit (penalty of 20% of a submission's grade)

The name of your assignment's repository must follow this pattern:

`Course-LastName-FirstName-Assn#`

Examples:

-   `cs2610-smith-jane-assn1`
-   `cs2610-jones-bill-assn5`

*Failure to follow this format will result in a 20% penalty*


#### Your submission _MUST_ contain:

1.  A directory called `src/` that contains your source code files and
    subdirectories needed for their organization.

#### Your submission _MAY_ contain:

1.  A directory called `data/` if extra data files are used by your program.
2.  Any other subdirectories to further organize your work as you see fit.
3.  A text file called `README.md` in the top-level directory.  This file
    contains special instructions or explanations for the grader.  Here you
    will cite any external sources used in the creation of your submission
    (e.g. Stack Overflow, Wikipedia, etc.).  You may omit this file if you did
    not use any external sources or do not have any special instructions for
    the grader.

Example submission structure:

```
    cs2610-jamieson-phil-assn4
    |-- README.md
    `-- src
        |-- Assn2
        |   |-- __init__.py
        |   |-- settings.py
        |   `-- urls.py
        |-- blog
        |   |-- __init__.py
        |   |-- admin.py
        |   |-- apps.py
        |   |-- models.py
        |   |-- static
        |   |   `-- blog
        |   |       |-- images
        |   |       |   |-- Beach.jpg
        |   |       |   `-- blonde_1.jpg
        |   |       `-- style
        |   |           |-- jquery.min.js
        |   |           `-- style.css
        |   |-- templates
        |   |   `-- blog
        |   |       |-- about.html
        |   |       |-- index.html
        |   |       `-- techTips.html
        |   |-- tests.py
        |   |-- urls.py
        |   `-- views.py
        `-- manage.py
```

If I provide starter code for an assignment I will very likely give you a
README.md file with instructions or a TODO list.  This file is not read-only to
you; you should modify its contents as you see fit.

Example README.md:
```
In crawl.py, the function parseURI() is borrowed and adapted from a recursive
function I found on Stack Overflow: https://stackoverflow.com/a/2520868

The grader will need to be sure to supply a fully qualified URL on the
command-line when they run my program.

My program hangs when it encounters URLs from cdn.cmp.omniupdate.com, so I went
ahead and added them to a blacklist on line 35 of crawl.py to speed things up
for the grader.
```

*Failure to follow this format will result in a 20% penalty*


#### Your submission _MUST NOT_ contain:

1.  Directories containing pre-compiled files or other generated files created
    by your IDE or build tools (e.g. `venv`, `*.pyc`, etc.).  An exception to
    this is the `.idea/` subdirectory created by the PyCharm IDE.
2.  Zip files, backup files, screenshots or other artifacts of your
	experimentations

Use a `.gitignore` file to prevent such files and directories from being
included in your repository.  Note that this `.gitignore` will also prevent
PyCharm's `.idea` directory from being included in your repository.

    venv
    .DS_Store
    .vscode
    .idea
    db.sqlite3
    __pycache__
    *.pyc
    *.zip

*Failure to follow this format will result in a 20% penalty*


### Verifying Submission

*You are graded on what you submit, not on what you meant to submit*

#### Reference Implementation

The graders will use the Anaconda distribution of Python 3.7 as the reference
implementation for grading assignments.  It is up to you to either

* Install this version of Anaconda Python on your computer

or

* Find a computer with Anaconda Python

The Engineering Computer Lab has Anaconda Python.


#### How To Verify Your Submission

It is *solely* your responsibility to ensure that your submission is complete
and correct.  Do these things *every time* you complete an assignment to
prevent 95% of possible submission problems:

1.	Clone your repository from Bitbucket into a *fresh* location and verify
    that the contents are complete and correct. 
2.	Run your program from top to bottom to catch any errors or warnings.
3.  Follow any special instructions from your `README.md` to ensure that they
    are complete and correct. 
4.  Run any tests attached to your code in one last comprehensive check.


#### Errors and warnings

* Code which does not run due to errors may, at most, receive 50% credit
* Warnings are penalized at 5% per warning


### When to Submit (penalty of up to 100% of a submission's grade)

*No assignment will be accepted that is more than 2 days late*

All assignments are due by 11:59 PM of the due date.  Assignments must be
submitted both to Canvas _**and**_ pushed to your repository on Bitbucket
before the due date arrives to be considered on-time.  Submitting late to
either one makes the entire assignment late.

*It is up to you to keep to the schedule*


-   **Best:**
    
    Submit your work before the day it is due.
    
-   **Down to the wire submission:**
    
    At 11:30 PM, submit whatever you have, even if it is incomplete... submit it!
    
    Work on it some more, then at 11:50 PM submit it again.
    
| Lateness  |            Penalty           |
|-----------|------------------------------|
|< 24 hours | 25% of total points possible |
|< 48 hours | 50% of total points possible |
|>= 48 hours| 100% of total points possible|


*   No assignment is accepted after 48 hours.
    **_Do not email me, do not ask, it is not accepted._**

*   The TAs and instructor are not available for last-minute questions and
    assistance.  In other words, don't expect a timely reply to an email sent
    after 10 PM in the evening or on weekends.

*   If you are planning on being out of town when an assignment is due, for any
    reason, be sure to get your homework assignment done before you leave.

Exceptions to the above can only be made in the event of a  _serious,
unexpected_ emergency.  Inform the instructor as soon as possible to work out
an arrangement.


#### Grading Gift

*Each student is allowed one late submission (up to 48 hours) without penalty*

*   The assignment must have been submitted within the acceptable late time
    frame *which is 48 hours in this class*
*   The gift is automatically used on the first assignment you submit late; you
    do not have to ask for it
*   The gift is *only* for late submissions.  You cannot ask to use it when
    your code is incomplete.
*   _The **Grading Gift** may not be used on the final assignment_

Do not use this early in the semester just because you feel a little lazy and
want to turn one in a little late without penalty.  Later in the semester you
may have a legitimate problem arise and you will have wasted this opportunity
to recover from an honest mistake without consequence.

If you "accidentally" make a late submission, the grading gift is nevertheless
consumed.  Any mistakes in later submissions that you might have corrected
within the 48 hour grace period will result in a penalty appropriate to the
error.



--------------------------------------------------------------------------------
## Examinations

*   There will be three exams this semester; two midterm exams and a final exam
*   The examinations are cumulative
*   There will be an exam every five weeks
*   Examinations will be taken in the testing center
*   The exams are already registered with the testing center and are available
    for you to schedule

On the Monday of the week of an exam the lecture will be a review.  The midterm
exams may be taken any time between Tuesday and Thursday of that week.  On the
Friday of that week we will discuss the results of the exam.  After this
discussion takes place on that Friday it is too late to make up an exam as the
answers will be common knowledge.

You should schedule your time slot for each exam as soon as possible, and add a
reminder to your calendar so that you don't forget to take it.  Once the exam
review occurs on the Friday following the exam it cannot be re-taken.


### Preparing for an examination

I provide two excellent ways for you to excel at my exams:

* Lecture notes git repository on Bitbucket
* Mastery Quizzes

If you are keeping up with assignments and attending lectures you will find my
exams to be very straightforward.  I draw exam questions from the lecture notes
I post to Bitbucket.  I will also use questions with varying degrees of
variation from the mastery quizzes posted in each module.



--------------------------------------------------------------------------------
## Behavior and Professional Conduct

*   All participants in this course are expected to conduct themselves in a
    manner conducive to the educational goals of this institution.
*   I will remove disruptive influences from my classroom.
*   Assignments must be your own original creation.  You may work in a group,
    but you cannot submit the same work as anyone else.  It is not
    out-of-bounds to seek advice and inspiration from online resources such as
    Stack Overflow and Wikipedia; that's how a lot of real*world coding
    happens!  When you borrow ideas from online sources, you must cite them in
    a `README.md` file in your repository.
*   Exams are to be completed without the aid of notes or web resources, unless
    the test contains a statement to the contrary.
*   Electronic devices may be used in class so long as I do not deem their
    presence to be disruptive.  Do not bother to come to class if you are just
    going to watch movies or play games.



--------------------------------------------------------------------------------
## Canvas Announcements

I reserve the right to amend the syllabus or these rules through in-class and
online announcements.  Whenever there is a conflict between this syllabus and
an announcement on Canvas, the announcement will take precedence.



--------------------------------------------------------------------------------
## Be The Designated Questioner (5% of your final grade)

*Every student in my class gets their own day to ask a question*

An important part of your training as a software engineer is to develop a habit
of critical thinking.  The best engineers and programmers I worked with have
one thing in common: they were driven to understand *why*.  In each lecture I
will call upon students to be the "designated questioners" (DQ) for that day.
When you are called or volunteer it is your responsibility to ask a *good*
question during that lecture.

*This is worth 50 points*


### Bad Questions

Some questions are better at eliciting a good, thought-provoking discussion
than others.  These examples are not suitable for a DQ's question:

1.  _"When is the exam?"_

2.  _"Will this be on the test?"_

3.  _"How many points will I lose if I turn this in a day late??"_

4.  _"What day did we talk about DOM events?"_


These questions are insufficient because

* They don't get at the *why* of any topic
* They will not spark a thoughtful discussion
* Their answers are readily found on Canvas, in the syllabus or in the lecture
  notes on Bitbucket


### Good Questions

In order to get better answers (and DQ points), do the following:

1.  Restate what you _do_ understand.

    _"As I see it, the DOM is a tree-like data structure similar to the file
    system on my computer. Is that correct?"_

2.  Explain what you _don't_ know.  Putting your misunderstandings into words
    is an effective way to organize your thoughts and to identify gaps in your
    knowledge.
    
    _"I don't get what you mean by 'dispatching control to the view function'.
    Could you please explain this in a different way?"_

3.  Ask a counterfactual question.  When you are called upon to be the DQ but
    don't feel that you need clarification, ask a question that attacks the
    underlying assumptions behind the subject at hand.
    
    _"What would be the difference if we did that computation on the server
    instead of in the browser?"_

    _"Why did you do it that way?  What would happen if you did it the other way?"_


*NOTE* If you have a legitimate reason to _not_ be a DQ, you may discreetly
contact me to explain your situation so that we can work out an alternative
arrangement.



--------------------------------------------------------------------------------
## In-Class Learning Activities and Online Discussions (5% of your final grade)

*Regular lecture attendance is correlated with high academic performance* 

Frequently throughout the semester you will participate in in-class learning
activities such as mud-cards, group discussions and assignment retrospectives
to

* Check your understanding
* Reinforce the material by taking a different approach
* Gather feedback on the effectiveness of my instruction

These events are graded for participation rather than performance.  Unless
otherwise stated these activities are worth 5 points each.  You may earn *up to
a maximum of 50 points* over the course of the semester.

I understand that many of you skip class in service of other worthy demands.
This policy does not demand that you attend 100% of my lectures; that is a
standard to which I do not expect to hold myself.  You may miss a few lectures
and still receive full points in this category.  Frequent absentees should not
expect excellent results.

This policy extends to online discussions hosted on Canvas.  Asking good
questions and giving good answers in a Canvas discussion thread will be
rewarded.  Students participating in online discussions must obey the following
guidelines:

-   The greatest benefit from assignments comes when you work out your own
    solution.  Do not post full or partial solutions.
-   If you have a complex programming question, design and post a simplified
    version of your problem.
-   Discussion forums are not to be used for gossip, inappropriate or hurtful
    messages.  Don't write anything you will regret later.
-   Don't expect instant responses on the forums.  We're not on the same
    schedule as you.



--------------------------------------------------------------------------------
## Asking For Help

*Help yourself before seeking help from others*


### Study Buddies

Identify and connect with a small circle of _study buddies_ among your
classmates who can be relied upon to

* Catch you up when lectures are missed
* Help study for examinations
* Work together on assignments and talk through technical problems
* Review each other's code prior to submission


### Escalation Hierarchy

It is natural for you to reach out to me for help when you encounter
difficulty.  While I am always happy to help you, the reality is that I am vastly outnumbered
by students.  To keep things orderly and efficient:

0.  Begin by carefully reviewing resources provided in class (lecture notes, example code, etc.)
1.  Ask your Study Buddies for help
2.  Approach the Teaching Assistant with your question
3.  Seek help from the CS tutor labs
4.  Finally, reach out to the instructor  

I reserve Sundays for family.  Do not expect a response before Monday.


### Use Git Instead Of A Screenshot

*Push broken code to Bitbucket instead of sending a picture of your error*

It has been said that a picture is worth a thousand words.  Whoever said that
never had to debug a program from a screenshot.   Push your code to Bitbucket,
grant the TAs and the instructor access.  Write an email containing your
repository's URL along with a description of the problem and steps you've
already taken.  You can put more detailed instructions into your repository's
`README.md` file.  This lets us see your problem immediately and avoids a long
back-and-forth process.
