# CS2610 - Wed Jan 30 - Module 2

# Announcements


## Free Software and Linux Club - Bash Course Crash Course

Learn how to use the Command Shell

We will be discussing navigation of your shell this week.  We will not try to
overwhelm anyone so it's going to be the basics and will provide everyone with
tools and resources to pursue Shell mastery on their own!

We hope to see everyone there!

Wednesday, January 30th, at 7:00pm in ESLC 053



## BSidesSLC Student Scholarship Program
https://www.bsidesslc.org/scholarship

*Interested students must submit a proposal by Friday, February 1st, 2018*



## Exam 0 is next week

The exam is available at the testing center from Tues Feb 5 - Thurs Feb 7.

The exam will cover all material from the beginning of the semester through
this Friday.

We will have an exam review on Monday to prepare.  The Mastery Quizzes for
Modules 0, 1 and 2 are your best way to review; I'll be updating Module 2's
Mastery Quiz with material from today and Friday's lectures before the exam.




# Topics:

*   Assignment 2: Static(?) Blog in Django
*   Python Intro Course on Canvas
*   The Official Django Tutorial
*   What does Django actually do?
*   Writing a view in Django




--------------------------------------------------------------------------------
# Assignment 2: Static(?) Blog in Django

https://usu.instructure.com/courses/529849/assignments/2581301


--------------------------------------------------------------------------------
# Python Intro Course on Canvas

We'll be doing a fair bit of work in the Python language now that we're using
the Django framework.

If you are new to the Python language or are a bit rusty, you've been invited
to a [Python Intro](https://usu.instructure.com/courses/474722) course on
Canvas.  This course will quickly bring you up to speed with what you need to
know to work in Django.


## TL;DR The most important things to understand about Python right now:

* [Python Numbers](https://usu.instructure.com/courses/474722/pages/numbers)
* [Python Strings](https://usu.instructure.com/courses/474722/pages/strings)
* [Python Lists](https://usu.instructure.com/courses/474722/pages/lists)
* [Python Control Flow](https://usu.instructure.com/courses/474722/pages/control-flow)
* [Writing Python Functions](../Readings-and-resources.md#markdown-header-writing-functions-in-python)



--------------------------------------------------------------------------------
# The Official Django Tutorial

Consider the Django Tutorial to be required reading.  Consider the polls app
that it describes to be a homework assignment.  I will expect you to be
familiar with this tutorial as it answers your most common questions.  When you
ask me or the TA a question, don't be surprised when the answer is "check the
tutorial".

We won't discuss the entirety of the tutorial in class.  I've listed which
sections of the tutorial I expect you to follow on your own.

[Tutorial sections to follow](../Readings-and-resources.md#markdown-header-the-official-django-tutorial)

Tutorial part 1 covers what we did in class on Monday with setting up a Django
project and app, and covers what we will discuss today.



--------------------------------------------------------------------------------
# What does Django do?

While developing your application Django provides you with a web server.  Web
servers communicate with web clients (user agents) using HTTP as the language.

## Static servers

Some web servers (such as Bitbucket.io) respond to HTTP requests by sending
back a file attached to HTTP headers.  Each identical request results in an
identical response.  Static web servers can be thought of as a mapping from
URLs to local files.

    $ nc unnovative.net 80
    GET /level1.html HTTP/1.0

    $ nc google.com 80
    GET /teapot HTTP/1.1


## Dynamic servers

Some web servers (such as Django) respond to HTTP requests by calling on a
function to create a custom response.

    $ nc checkip.dyndns.com 80
    GET / HTTP/1.1


This function's input is the entirety of the HTTP request, including the method
of the request (GET, PUT, POST, DELETE), the URL requested, all of the HTTP
headers as well as any data payload sent by the user agent.

This response can be *anything* that a function can compute from these inputs.
The function could return a hard-coded string, or it might read and return the
contents of a file.  Or, the function could combine the input data in a complex
computation to produce a unique response.



--------------------------------------------------------------------------------
# Writing our first Django views

Let's start small and build a few simple views.

https://docs.djangoproject.com/en/2.1/intro/tutorial01/#write-your-first-view


Clone my code and follow along with what I do in class

    git clone https://bitbucket.org/erikfalor/sp19-cs2610-djangoproj
