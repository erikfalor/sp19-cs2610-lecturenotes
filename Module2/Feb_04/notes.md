# CS2610 - Mon Feb 04 - Module 2


# Announcements


## SDL/USU Technical Lecture - Engineering: Finding the Great Parts of the Grind
Tom Russell, EE - SDL

Please join us for the next SDL/USU Technical Lecture Series at 4:30 pm on
Tuesday, February. 5 in ENGR 201. Free pizza after lecture. 



## AIS - Real World Cyber Security

Tuesday Feb 5 6pm - 7pm
Huntsman Hall 322

Jon Parker, IT Director  of Enterprise Info Management, Melaleuca
Josh Rolfe, IT Senior Manager of Sec Ops, Melaleuca



## FSLC Meeting

The Vim vs. Emacs Showdown!!!
Wednesday Feb 6
7pm - ESLC 053



## DC435 Meetup - Packet Capture Games

Have you ever wanted to know what your ISP can see about you? What about your
employer? Or someone else on your hotel wifi with you?

Come learn about network packets and play some **Wireshark** packet capture games

https://dc435.org/blog/2016-12-17-making-sense-of-the-scaas-new-flavor-wheel/

Thursday, Feb 7th 7pm
B Tech West Campus - 1410 North 1000 West




## Django version 2.2 is coming out soon

As you're following the tutorial, be sure that you're reading the documentation
for Django version 2.1.  The tutorial for version 2.2 is now available.

According to their release schedule, the next version of Django will be coming
out late in this semester.

https://www.djangoproject.com/weblog/2019/jan/17/django-22-alpha-1/

My plan is to stick with version 2.1 throughout the entire semester.


You can check which version of Django you have installed with this command:

    python -m django --version



--------------------------------------------------------------------------------
# Exam 0 Review


We'll review by doing Exam Review Speed Dating

Face the row opposite you.  Students on the row facing the screen will be the
"novices" who have lots of questions.  Students facing away from the screen are
the "experts" who have all of the answers.

Take a few minutes to ask and answer the listed questions.  Then we'll switch
sides and do it all again.

Ready?





## Git

* What is a git repository?
    It's a folder that contains the "magical" .git folder

* What is the fundamental unit of history in the git log?
    commit

* Who created The Git Version Control System?
    Linus Torvalds (the dude who made Linux)

* How do I
    * list the commit history of a repo?
        git log

    * send commits from your local repository to a remote repository?     
        git push

    * select the files that should be included in a new commit?     
        git add

    * permanently records the staged changes into the repository?     
        git commit  / git commit -m ""

    * create a brand-new repository from scratch?
        git init
        
    * see which files have been changed in the working tree since the last commit?
        git status

    * set up a new remote repository
        git remote

    * set configuration options (for instance, your name and email address)?
        git config



## HTML - Hyper Text Markup Language

* Explain the purpose and proper use of HTML elements
    * head   - for the brower's use, contains metadata
    * body   - contains the page's content
    * title
    * p      - block-level, free-flowing text (the spaces are squashed)
    * pre    - block-level, formatted text (the spaces are preserved)
    * a      - hyperlink 
    * img    -
    * div   - block-level
    * span  - inline
    * ol, ul, li
    * table, tr, td

* Inline vs. block-level elements
    Block-level takes up the width of its container
    Inline doesn't take up more width than it needs

* Identify a correct HTML5 DOCTYPE declaration
    <!DOCTYPE html>

* Recognize the structure of a correct HTML5 document

* What data structure does an HTML document describe?
    A tree


## Cascading Style Sheets - CSS

* What problem does CSS aim to solve?
    modularity - 1 style sheet for all of my pages
    reuse code across pages, across websites
    Don't need to re-structure the tree to change appearance


* Why is it called "Cascading" Style Sheets?
    When I have two or more conflicting rules that apply to the same element,
    the "cascade" algorithm decides which rule applies (tie-breaking)
    The most specific rule wins, then ties are broken by "last-seen-wins"


* Basic Selectors - what do they look like, and what do they select?
    <p class="one two" id="Joey"> this is some text </p>
    <p class="one " id="Adam">This paragraph doesn't match .one.two</p>

    * element
        p

    * class
        .two
        .one.two /* logical AND */

    * ID
        #Joey


* Composite Selectors
    * What does a comma mean in a selector?
        .one, .two /* logical OR */

    * Write a selector matching elements participating in a descendant relationship
        div p

    * Write a selector matching chilren participating in a parent -> child relationship
        ol > li



## Django

* What is a Django Project?

* What is a Django App?

* How do I create a new Django project?

* Which Django (manage.py) command do I use to...
    * Create a new Django application?
    * Add a superuser account to my project?
    * Update my database schema? (2 commands)
    * Launch an interactive shell w/ Django system loaded?
    * Run a server in my project?


*   Which Django file is responsible for...
    * The code which runs when a user wants to see a page? (VIEW)
        views.py
    * Mapping a URL to a view function? (CONTROLLER, aka receptionist)
        urls.py
    * Containing configuration information for my project?
        settings.py


*   Django templates
    *   What function do I call to render a template?
        render()
    *   What is a Django context object?
        a Python dictionary
    *   What type of Django template entity is surrounded by double curly braces {{ }}?
        variable
    *   What type of Django template entity is curly braces like this: {% %}?
        template tag
