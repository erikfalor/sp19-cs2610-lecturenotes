# CS2610 - Mon Jan 28 - Module 2

# Announcements

## The USU Security Team is Hiring

They are looking for Security Minded Students
You can apply on [Aggie Handshake](https://app.joinhandshake.com/jobs/2331049)




# Topics:
* HTTP Headers
* Static hosting on Bitbucket
* Getting started with Django
* Python intro
* Writing your first Django view


----------------------------------------------------------------------------
# HTTP Headers

[HTTP Header Reference](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers)

HTTP headers allow the client and the server to pass additional information
with the request or the response.

A request header consists of its case-insensitive name followed by a colon ':',
then by its value (without line breaks).

Leading white space before the value is ignored.


There are two categories of headers:

1.  **Request** headers are sent client -> server
2.  **Response** headers are sent server -> client

General headers may be sent in both directions.

Here are some important headers that let the client and server negotiate the
terms of communication.


Cache-Control
-------------
The `Cache-Control` general header field is used to specify directives for
caching mechanisms in both requests and responses.  `max-age` is expressed
in seconds.

	Cache-Control: no-cache
	Cache-Control: max-age=600


Connection
----------
The `Connection` general header controls whether or not the network connection
stays open after the current transaction finishes.

	Connection: keep-alive
	Connection: close


Accept
------
The `Accept` request HTTP header advertises which content types, expressed as
MIME types, the client is able to understand.  The last example tells the
server "I will accept all types of content".

	Accept: text/html, text/plain, application/octet-stream
	Accept: image/jpeg, image/png
	Accept: image/*
	Accept: */*


Accept-Encoding
---------------
The `Accept-Encoding` request HTTP header advertises which content encoding,
usually a compression algorithm, the client is able to understand.  Multiple
algorithms are specified, in preference order, separated by commas.

    Accept-Encoding: gzip
    Accept-Encoding: deflate
    Accept-Encoding: br
    Accept-Encoding: gzip, compress, deflate, br
    Accept-Encoding: *


Cookie
------
The `Cookie` HTTP request header contains stored HTTP cookies previously sent by
the server with the Set-Cookie header.

All cookies must be sent as one line of text, with no newline characters.  This
header takes a list of name=value pairs. Pairs in the list are separated by a
semi-colon and a space ('; ').

	Cookie: chocolate=chip; yummy=true; 


Set-Cookie
----------
The `Set-Cookie` HTTP response header is used to send cookies from the server to
the user agent.  An expiration date for this cookie may be specified by the server.

	Set-Cookie: Hungry_For_Apples=no
	Set-Cookie: Whats_Up=My_Glip_Glops; Max-Age: 600
	Set-Cookie: South_Park=Already_Did_It; Date: Wed, 02 Oct 2013 17:01:03 GMT;

The `max-Age` directive is expressed in seconds

The `date` directive is a date FROM THE POINT OF VIEW OF YOUR PC!


Content-Length
--------------
This general header indicates the size, in bytes, of the payload sent to the
recipient.  Non header-only responses from the server will use it to tell the
browser how much data to expect.

When used as a request header it is only meaningful when used in conjunction
with requests that send a payload of data.  These requests include POST, PUT,
and PATCH.  GET requests send only headers, hence this header isn't used then.

The content begins on the line immediately following the `\r\n` blank line
separating the headers from the Payload in a POST req  uest.

    Content-Length: 42
    Content-Length: 1337


*Question:* if GET requests don't tell the server how many bytes to expect, how
does the server know when it has received the entire request from the client?



## Experimenting with headers using `nc` (netcat)

* Linux users likely already have netcat installed.  It may be found by the
  name of `nc`, `ncat` or `netcat`.

* Windows users can download a clone of this program from
  https://joncraton.org/blog/46/netcat-for-windows/.  To extract the zipfile
  use the password "nc".

* I'm not sure about Mac.  Maybe it's in your App store?


You may also use a program called `telnet` to do this, too.  For what we're
doing it behaves as a drop-in replacement for `nc`.  Simply replace `nc` with
`telnet` in the command lines below.

* Linux users: Telnet likely already installed.  If not, netcat is better.

* Windows users: Telnet is already kind-of installed for you, but is disabled
  for "security" reasons.  You'l have to go into the Control Panel and manually
  enable or install it as a "Program and Feature".

* Mac: Let me know if Telnet happens to come pre-installed.  As with Linux,
  netcat is superior.



To run these examples leave off the leading whitspace but otherwise type them
EXACTLY as presented below.  Remember that the line starting with a dollar sign
denotes the command that I typed on my computer; don't copy the dollar sign.
When you type `nc` (or `telnet`) the computer will wait for you to finish
entering your complete request.  Signal that your input is finished by typing
`Return` twice.

After the request is displayed you may not be returned to the command prompt.
When this happens press `Ctrl-C` to exit from `nc`.  If you're using Telnet you
must press `Ctrl-]` and type `quit` at the 'telnet>' prompt.



### Using the `Connection` header

Can you spot the difference between these requests?  What does the `Connection` header do?

    $ nc google.com 80
    GET /search?q=cool+stuff HTTP/1.1


    $ nc google.com 80
    GET /search?q=cool+stuff HTTP/1.1
    Connection: close


    $ nc google.com 80
    GET /search?q=cool+stuff HTTP/1.1
    Connection: keep-alive



### Using the `Accept-Encoding` header

Can you spot the difference from one of the requests above?

    $ nc google.com 80
    GET /search?q=cool+stuff HTTP/1.1
    Accept-Encoding: gzip
    Connection: close


### Visit Google's `teapot` service to see the rare 418 status code

    $ nc google.com 80
    GET /teapot HTTP/1.1
    Connection: close



### A website that responds differently based upon the `Host` header

    $ nc www.ask.com 80
    GET /web?q=cool+stuff&o=0&qo=homepageSearchBox HTTP/1.1
    Connection: close


    $ nc www.ask.com 80
    GET /web?q=cool+stuff&o=0&qo=homepageSearchBox HTTP/1.1
    Host: www.ask.com
    Connection: close



--------------------------------------------------------------------------------
# Static Hosting on Bitbucket

https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html

1. Create a new repository named <username>.bitbucket.io
2. Push the index.html and stylesheet to the bitbucket.io repository made in step 1
3. Open the hosted webpage on <username>.bitbucket.io

_Note:_ You will need to manually segregate different webpages into their own separate directories



## Beware of caches!

Bitbucket will cache parts of your statically hosted webpage for performance
reasons.  Other websites employ CDNs to cache parts of websites to improve
delivery speed and to reduce their bandwidth costs


#### Content delivery network (CDN):
A system of distributed servers (network) that deliver pages and other Web
content to a user, based on the geographic locations of the user, the origin of
the webpage and the content delivery server.

https://www.webopedia.com/TERM/C/CDN.html

This will manifest to you when you push an update to Bitbucket but don't see
the changes in your browser when you refresh the page.  In the past it has
taken upwards of half an hour for changes to propagate through the CDN back to
students.




--------------------------------------------------------------------------------
# Getting started with Django

https://bitbucket.org/erikfalor/sp19-cs2610-lecturenotes/src/master/Readings-and-resources.md#markdown-header-creating-your-first-project



