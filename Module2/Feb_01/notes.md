# CS2610 - Fri Feb 01 - Module 2

# Announcements


## Mastery Quizzes have been updated for Modules 1 & 2

This is in preparation for the upcoming Exam #0, next week from Tues Feb 5 -
Thurs Feb 7.

I will add more questions to M2's MQ based upon how far we make it today.


# Topics:

*   How *NOT* to create dynamic content
*   What is a template in Django?
*   The Django template language
*   What are Context Objects in Django?
*   How to include static content in a Django-generated page
*   Removing hardcoded URLs in templates



----------------------------------------------------------------------------
# How *NOT* to create dynamic content

I worked on webapps a number of years ago, back when Web 2.0 was just getting a
start.  Before I show you the **right** way to create a dynamic website, let's
take a look at how we used to do this so you can more proprely appreciate just
how great Django is.

Let's make two webpages:

1. A chiefly static page with a trivial dynamic element (index.html)
2. Another page including a visit counter


## Follow along with the code I'm writing in class

https://bitbucket.org/erikfalor/sp19-cs2610-djangoproj

This is in the "hello" app.

Well, that seemed unnecessarily difficult.  Let us count the ways those pages
sucked:

1.  Lots of repetition
2.  Mixing two languages in one file, and worse, it's a string literal!
3.  Doesn't scale very well




----------------------------------------------------------------------------
# What is a template in Django?

Basically we want to treat our HTML like a Wacky MadLib (TM) and fill in the
blanks.  A central feature of Django is the template system which lets us do
that, and so much more.


#### Template
Partially-formed webpage file with defined style and layout, containing regions
to be filled in programatically

https://docs.djangoproject.com/en/2.1/intro/tutorial03/#write-views-that-actually-do-something


Let's try this example again, but with templates.



## How Django loads templates

https://docs.djangoproject.com/en/2.1/topics/templates/#template-loading


### Install the template files

Create a subdirectory within your app named templates/, which itself contains
*another* subdirectory named for your app. For example, in my hello app I
create:

    mkdir hello/templates
    mkdir hello/templates/hello

Into this directory I place a template file named `index.html`. This convoluted
scheme is to distinguish a template named `index.html` in my hello app from
another template called `index.html` in a different app.



### "Install" your app into the Django project

When a view asks to render a template Django will look for a directory called
`templates` in every *installed* app within the project.  You might think that
running `python manage.py startapp <APPNAME>` would be enough to install the
app, but it isn't.  We'll just have to do this part ourselves.

Django created many files for you When you started your app with the above
command.  One of the files was called `hello/apps.py`, which contains a Python
class that we must tell the Django project about.  This constitutes
"installing" our app.

These instructions are contained within the Django Tutorial, but I'll list them
here for your convenience as you follow along:

1.  Edit the Django project's `settings.py` file.
2.  Look for a variable named `INSTALLED_APPS`.  It is a Python list, where
    each element is a string giving the Pythonic name of classes corresponding
    to Django apps.
3.  Add to the list the Pythonic name of the class contained `hello/apps.py`.
    In my case, this string is `'hello.apps.HelloConfig'`.  Don't forget to add
    a comma after your string, or else you'll create a syntax error!


If you fail to install your app in this way, Django will fail to locate your
template file and you will see an error page bearing the error
`TemplateDoesNotExist`.



### Render the template in your view function

Next, in my view function, instead of returning an HttpResponse() object
constructed with a string containing HTML content, I call the `render()`
function imported from the  `django.shortcuts` module.

`render()` needs two required arguments and can take a third optional argument.
These are:

1.  The HTTP request object which was passed to our view function
2.  A string naming our template file.  This name is a *relative path* which
    identifies the template under the `hello/templates` directory.  We leave
    out the part of the path containing our Django project, as well as the name
    of the `hello/templates` directory itself.
3.  *Optionally* a "context object" into which we put any variables that the
    template may use to fill-in the blanks.  If we don't pass a context object
    our template is effectively a static web page.


Here is a very minimal example:

    from django.shortcuts import render
    from time import strftime

    def index(request):
        context = { 'now' : strftime('%c')}
        return render(request, 'hello/index.html', context)




--------------------------------------------------------------------------------
# The Django template language

https://docs.djangoproject.com/en/2.1/topics/templates/#the-django-template-language

Templates in Django are a mixture of HTML, CSS, etc. with markup that
Django looks for and acts upon. The Django template language has four
constructs:

### `{{ Variables }}`

A variable outputs a value from the context, which is a dict-like object
mapping keys to values.



### `{% Tags %}`

Tags provide arbitrary logic in the rendering process. This can involve
conditionals (if/else blocks), loops {% for something in something %}, etc.

https://docs.djangoproject.com/en/2.1/ref/templates/builtins/#ref-templates-builtins-tags



### `{{ textVariable | Filters }}`

Filters transform the values of variables and tag arguments.

https://docs.djangoproject.com/en/2.1/ref/templates/builtins/#ref-templates-builtins-filters



### `{# Comments #}`

Prevent text and markup from being rendered (e.g. `{# this won't be rendered #}`)

A `{% comment %}` tag provides multi-line comments.

    {% comment %}

        <!-- WIP: I haven't written the URLConf for 'some-url-name' yet,
        so this tag crashes right now  -->
        {% url 'some-url-name' v1 v2 %}

    {% endcomment %}




----------------------------------------------------------------------------
# What are Context Objects in Django?

The data we send to the template renderer is packaged in a Python dictionary.

[Python Dictionaries](https://usu.instructure.com/courses/474722/pages/dictionaries)

A dictionary is a compound datatype which is used like an array or list but
which uses strings in its subscript instead of integers.

* Lists in Python are constructed with [] 
* Dictionaries in Python are constructed with {}
     

Lists are ordered collections of values. You look up items in a list by their
position. You can meaningfully say "item x is before item y in this list".

Dictionaries, by contrast, are unordered collections of items. There is no
concept of "before" or "after" when it comes to dictionaries. Dictionaries are
key-value pairs.

Therefore, you don't store or retrieve data in a dictionary by referring to its
position. Instead, you give each item a name, and refer to it thus.

A dictionary is denoted with curly braces { }. Keys are separated from values
with a colon :

    sculptors={
            "light_fixtures": "Elsner",
            "concentric_arcs": "Ohran",
            "pivotal_concord": "Deming",
            "french_fries": "Kinnebrew",
            "Tools_of_Ag": ["Cummings","DeGraffenried"],
            "Whispers_and_Silence": "Suzuki",
            "PrincePhraApaimanee": "Kampalanont",
            "BlockA": "Be-No Club"
            }   

    print sculptors['Whispers_and_Silence']



--------------------------------------------------------------------------------
# How to include static content in a Django-generated page

https://docs.djangoproject.com/en/2.1/intro/tutorial06/

Static content (CSS, images, JavaScript files) are to be placed in a
subdirectory named static/ under your app's directory. Just like with your
templates, you should create another subdirectory within static/ that matches
the name of your app.

The URL at which Django will make your static files available will not
necessarily match the directory structure of your webapp, so it won't be wise
for you to try to hardcode the URL to these resources into your template.

Rather, you will use the {% static %} tag to instruct the template renderer to
create the correct URL for you. While this does feel extraordinarily
complicated at first, it does give you the flexibility to later on move or
rename your app with a minimum of fuss.

Remember to use the {% load static %} tag the top of your template to enable
the {% static %} tag.


Example:

    {% load static %}
    <html>
        <head>
            <title>How I shall take over the world</title>
            <link rel="stylesheet" href="{% static 'hello/style.css' %}" />




----------------------------------------------------------------------------
# Removing hardcoded URLs in templates

[Removing hardcoded URLs in templates](https://docs.djangoproject.com/en/2.1/intro/tutorial03/#removing-hardcoded-urls-in-templates)

Instead of writing a hardcoded URL which will suffer from the same drawbacks
as a hardcoded absolute URL in a plain HTML file, we can get Django to write
the correct URL for us.  Use the `{% url %}` template tag to map the name of a
view function to a URL the browser can follow.  Then our template will always
work even if we change the mapping of URLs to view functions in the Controller.

