# CS2610 - Fri Jan 25 - Module 2


# Announcements

## FSLC website is online!

https://usufslc.com/


## BSidesSLC 2019 Cyber Security Conference

https://www.bsidesslc.org

Build, Break, Network, Learn, & Give Back!   

Friday, February 22

10333 S Jordan Gateway, South Jordan, Utah, 84095

### BSidesSLC Student Scholarship Program
https://www.bsidesslc.org/scholarship

*Interested students must submit a proposal by Friday, February 1st, 2018*



# Topics
* How does a web server fit into the scheme of things?
* How does the HyperText Transfer Protocol (HTTP) work?
* Getting started with Django



----------------------------------------------------------------------------
## How does a web server fit into the scheme of things?


#### Mud Card Activity: Create a protocol

For the time being I want you to forget everything you may already know about HTTP.

Your team is tasked with devising a language for two computers to in order to
communicate with each other.  One of the computers will play the role of a
`server`, which lives to fulfill the wishes of its client.  The other computer
will be the `client` which will ask questions and make requests of its server.

To keep things simple, we will follow these assumptions:

* Each interaction will begin with the client's request.

* Each interaction will end with the server's response.

* The client never becomes a server, nor will the server ever take on the role
  of a client.

* If a problem arises with the request, the server will respond with an
  indication of an error, and this communication ends. The client may then
  begin again with a new communicaiton.

### Consider the following questions:

+ What sorts of vocabulary will your language need?
    + What are the nouns in your language?
    + What are the verbs in your language?

+ What concepts do you envision to be the most difficult to exchange
  information about?

+ What concepts will your language be best suited to facilitate communication?



----------------------------------------------------------------------------
## Servers and Protocols


#### HTTP: Hyper Text Transfer Protocol
Originally invented by Tim Berners-Lee at CERN in the early 90's to share
information between researchers.

Fits well with HTML, but you can use HTTP to send other kinds of data


#### Protocol:
A system of rules which define how data is exchanged between systems
Also defines the format of the data

What to say, and when you may say it


#### Server:
Exists to provide services to other users/systems
This is the backend

#### Client (a.k.a. User Agent):
Makes requests of servers to provide functionality to their users
This is the frontend





----------------------------------------------------------------------------
## How does the HyperText Transfer Protocol (HTTP) work?

https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview

As far as protocols go, HTTP is very simple.

It isn't comprehensive, and leaves lots of details to the other protocols it
"rides on top of".

It therefore doesn't need to specify much in the way of addressing, hostname
to address resolution, connection negotiation and maintenance, as these
details are handled by lower-level protocols such as ICMP, IP, DNS and TCP.

HTTP consists of human-readable plain text as opposed to being expressed as
binary - a feature it enjoys by being situated at the top of the protocol
hierarchy.

HTTP is extensible through it's acceptance of a wide variety of headers.  The
protocol works when user-agents (browsers, mostly) and servers agree upon what
a particular header means. But when an unrecognized header is presented to a
user-agent, it ignores it instead of responding with an error condition. This
makes it easy to augment the standard by adding new features to the protocol
later on.


                                                            
### HTTP Semantics
A client initiates a communication in HTTP.  It sends a request to a server.
The server responds, concluding the communicaiton.

* HTTP request messages begin with a block of headers in the form of

    METHOD PATH VERSION
    Header0: value0
    Header1: value1
    ...
    <blank line>
    Content data follows...


* HTTP responses look like this:

    VERSION STATUS_CODE STATUS_MESSAGE
    Header0: value0
    Header1: value1
    ...
    <blank line>
    Content data follows...





### HTTP Methods

https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods

These are the verbs in the HTTP language.  These are the actions that HTTP
clients can perform with an HTTP server.  This table is not exhaustive


| Method | Description                                                                                              |
|--------|----------------------------------------------------------------------------------------------------------|
| GET    | Requests a representation of the specified resource. Requests using GET should only retrieve data        |
| POST   | Submit an entity to the specified resource, often causing a change in state or side effects on the server|
| PUT    | Replaces all current representations of the target resource with the request payload                     |
| DELETE | Removes the specified resource                                                                           |
| HEAD   | Asks for a response identical to that of a GET request, but without the response body                    |
| OPTIONS| Asks the server what communication options it supports                                                   |



### HTTP Response Status Codes

The server's response begins with a three-digit number indicating the
disposition of the client's request.  You've undoubtedly encountered a webpage
bearing the message `404 Not Found`.

Responses are grouped into five broad categories whose status codes share the
same first digit.  Responses always bear a human-friendly translation of the
status code.

| Category      |Digit| Examples                                                                      |
|---------------|-----|-------------------------------------------------------------------------------|
| Informational | 1xx | `100 Continue`, `101 Switching Protocol`                                      |
| Success       | 2xx | `200 OK`, `201 Created`, `204 No Content`                                     |
| Redirection   | 3xx | `301 Moved Permanently`, `307 Temporary Redirect`                             |
| Client errors | 4xx | `400 Bad Request`, `401 Unauthorized`, `403 Forbidden`, `418 I'm a teapot`    |
| Server errors | 5xx | `500 Internal Server Error`, `501 Not Implemented`, `503 Service Unavailable` |



----------------------------------------------------------------------------
## Getting started with Django

To take our next step towards making dynamic, database-driven web applications
we will need to use a server.  Furthermore, we will need to somehow provide
programs to the server so that users can interact with it dynamically.

In this course we will be using [Django](Readings-and-resources.md) to fill
this need.
