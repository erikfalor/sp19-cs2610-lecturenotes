# Module 2 Readings and Resources

## Table of Contents

* Django Web Application Framework
* What's the difference between a _project_ and an _app_?
* Installing Django in Anaconda
* Creating your first Django project
* What are all of the files Django makes for me?
* The Official Django Tutorial
* Writing our first Django views
* Writing Functions in Python
* Importing Python Packages
* What is MVC and/or MTV?
* Tagging commits with the `git tag` command


----------
## Django Web Application Framework

[https://www.djangoproject.com/](https://www.djangoproject.com/)

Django is a web application framework written in and using the Python
programming language.



#### Application Framework:
An application framework is software that provides a fundamental structure to
support the development of applications for a specific environment.


A framework:

-   Helps us build webapps quickly
-   Holds the rest of the application together
-   Provides and defines the shape of the application


### Django Pros:

-   Django gives you a quicker route to your working app
-   It guides you as you make your app, choices are already made for you
-   Batteries are included


### Django Cons:

-   You give up a lot of control over how your project is organized
-   If your app isn't in Django's "shape", then Django gets in your way more than it helps
-   Sometimes what you want is a solar panel, not a battery


Official Documentation for our version of Django: [https://docs.djangoproject.com/en/2.1/](https://docs.djangoproject.com/en/2.1/)


## IMPORTANT!
When you're looking up Django documentation, be sure that you're looking at
documentation for the correct version of Django! (It's in the lower-right
corner).


----------
## What's the difference between a _project_ and an _app_?

We will use Django by creating a *project* which contains *apps*.

-   Project
    -   The code Django creates for me, containing my apps; a.k.a. my project directory
    -   Infrastructure: HTTP inputs and outputs, routing requests to different functions, etc.
    -   Configuration: allows my app to run under different Web Servers (i.e. Apache, Nginx)
    -   Development support: includes a simple HTTP server to support quick development

-   Apps
    -   The code I actually want to write
    -   The fun stuff my users will enjoy


----------
## Installing Django in Anaconda

1.  Upgrade Conda to get the latest versions of all software

    `$ conda update -n base conda`

2.  Install Django

    `$ conda install django`



----------
## Checking the version of Django

The version of Django that we'll be using in this class is v2.1.5.  

As you're following the Django tutorial, be sure that you're reading the
documentation for Django version 2.1.  The tutorial for version 2.2 is now
available, and may lead you astray as we stay on version 2.1.  According to
their release schedule, the next version of Django will be coming out late in
this semester.

https://www.djangoproject.com/weblog/2019/jan/17/django-22-alpha-1/

My plan is to stick with version 2.1 throughout the entire semester.

You can check the version of Django that is installed with this command:

    `$ python -m django --version`


If it ever happens that you accidentally upgrade Django to a newer version, you
can downgrade back to our reference version with this command:

    `$ conda install django=2.1.5`


----------
## Creating your first Django project


1.  Create a new Django Project called "cs2610proj"

    `$ django-admin startproject cs2610proj`

2.  Initialize a git repo in your project directory

    `$ cd cs2610proj`
    `$ git init`

3.  Create a `.gitignore` file.  Initialize your .gitignore with the following
    contents:

    `venv`
    `.DS_Store`
    `.vscode`
    `db.sqlite3`
    `__pycache__`
    `*.pyc`
    `*.zip`

4.  Commit your changes to git.

    `$ git add .`
    `$ git status`
    `$ git commit -m "Created my Django project"`

5.  Launch the Django development server so you can visit your project in your browser

    `$ python manage.py runserver`

6.  Visit http://127.0.0.1:8080 in your browser to see your new Django project in action


7.  Create a new Django application using your Django project's manage.py
    program

    `$ python manage.py startapp hello`


--------------------------------------------------------------------------------
## What are all of the files Django makes for me?


#### Project files

The `django-admin startproject cs2610proj` command creates this directory structure:

```
cs2610proj/          # Project directory; its name doesn't really matter
	cs2610proj/      # Project package. Django looks for settings in here
	   __init__.py
	   settings.py   # our project's settings
	   urls.py       # the "receptionist" - tells you where to go to use an app
	   wsgi.py       # Pertains to the WSGI interface to a "real" web server
	manage.py        # Basically the same as django-admin, but is more aware of your project
```

#### App files

When we run `python manage.py startapp hello`, Django creates the skeleton of a
web application for us:

```
cs2610proj/
    manage.py
	hello/
	   __init__.py      # This file makes the 'hello' directory into a Python package
	   admin.py         # Administration interface of your app
	   apps.py
	   migrations/      # Django will make files under this based upon our Model
		   __init__.py
	   models.py        # the 'M' in MVC
	   views.py         # the 'V' in MVC
	   urls.py          # the 'C' in MVC
	   tests.py         # Optional unit tests
```


You will want to commit these files to git as well

```
$ git add hello
$ git commit -m "adding Django-generated 'hello' project"
```


----------
## The Official Django Tutorial

One of Django's strongest advantages is the quality of their documentation.
When you have a question, please carefully read through the tutorial and the
other online resources.  While it's nice to have a teacher or a TA at your beck
and call, you must become self-sufficient sooner or later.  Now is the time for
you to learn how to answer your own questions by reading technical
documentation.

I did not just say *you are not allowed to ask me or the TAs questions about
Django*.  What I said is *when you ask your question, be ready for us to ask
"what did your own reading turn up?"*

The next two in-class assignments will take you through the process of
converting the static "blog" website into a dynamic blog with a functional
comment feature.  The Django tutorial walks you through the creation of a
dynamic polling app.  These apps may not seem to share much in common, the
truth is that nearly everything that you will need to know to complete in your
blog assignments is taught by following this tutoriral and creating the polls
app.

Consider the tutorial to be **required reading** and the polls app to be
**homework**.  I don't directly grade your polls app, but the quality of your
blog app will reflect the amount of effort you put into doing this.

There are seven chapters in the Django tutorial.  I won't follow it in order,
nor will we cover all seven chapters.  Although you are not expected to do so
(read: only these sections listed below will be covered on exams), you are
encouraged to explore the other chapters on your own.



### Tutorial sections we will cover together

#### Assignment 2: Static(?) Blog in Django

-   [Part 1](https://docs.djangoproject.com/en/2.1/intro/tutorial01/) - Creating a project and an app; writing a simple view
-   [Part 3](https://docs.djangoproject.com/en/2.1/intro/tutorial03/) - Using the template system, avoiding hardcoded URLs in templates
-   [Part 6](https://docs.djangoproject.com/en/2.1/intro/tutorial06/) - Static files: applying CSS and images to your webpage


I anticipate that the trickiest part will be skipping from Part 1 to Part 3;
we'll come back to models for the next assignment.


#### Assignment 3: Dynamic Blog in Django With The ORM
-   [Part 2](https://docs.djangoproject.com/en/2.1/intro/tutorial02/) - Models: Setting up the database, creating models, playing with the Django shell, using the Django Admin Site
-   [Part 3](https://docs.djangoproject.com/en/2.1/intro/tutorial03/) - Dynamic views, rendering templates, avoiding hardcoded URLs
-   [Part 4](https://docs.djangoproject.com/en/2.1/intro/tutorial04/) - Interactive views implementing HTML forms, generic views


### Portions that you may safely skip over

-   [Part 4](https://docs.djangoproject.com/en/2.1/intro/tutorial04/) - The last portion "Use generic views: Less code is better"
-   [Part 5 "Introducing automated testing"](https://docs.djangoproject.com/en/2.1/intro/tutorial05/)
-   [Part 7 "Customize the admin form"](https://docs.djangoproject.com/en/2.1/intro/tutorial07/)





--------------------------------------------------------------------------------
## Writing our first Django views

https://docs.djangoproject.com/en/2.1/intro/tutorial01/#write-your-first-view

Django calls the functions which take HTTP requests as input and produce HTTP
responses as outputs *views*.  A Django webapp contains a file called
`views.py` in which these functions are written.

#### View (Django)
A Python function which takes an HTTP Request as input and returns an HTTP Response

A Django webapp may consist of many views, just as a Django project may consist
of many apps.  When Django receives an HTTP request how does it know which view
to call?  A system called the *controller* looks at each incoming request and
decides which view should handle it.



#### Controller (Django)
The part of Django which looks at an HTTP request and decides which view
function to call in response.


We configure the controller with a Python list called `urlpatterns` which
contains the directory for our project (e.g. directory like a phone book).
Django uses this list to decide which view function to call in response to
receiving a request for a URL.

The controller is like a receptionist in an office.  When you want to visit
somebody at an office you ask the receptionist how to find that person.  They
will then direct you where to go, or they might tell you that person is
temporarily unavailable.


### How do I configure the receptionists?

There are two receptionists:

* One on the main floor of the building (project) who directs traffic to the
  different offices in the building (apps)
* One in each office (app) who directs traffic to specific people (views) in
  the office

The controller is made up of files named `urls.py` in the project directory,
and in each app directory.  The `urls.py` file under the project directory is
created by the `django-admin` program, and is already present.  A source of
confusion arises from the fact that when we run `manage.py startapp`, Django
doesn't make this file for us, and we must create it from scratch within each
app we start.


### What code should be in urls.py?

This file must contain a Python list (in Python a list is similar to an array)
called `urlpatterns`.


The urlpatterns list contains `path` objects.  Each of which contains:
* A pattern describing part of the URL typed into the address bar of the client
* A function to call to serve the resource at this URL

https://docs.djangoproject.com/en/2.1/intro/tutorial01/#path-argument-route

Once I set up my receptionists, Django will be able to run my view function
when I visit my app in a browser.




----------
## Writing Functions in Python

[Python Functions](https://usu.instructure.com/courses/474722/pages/python-functions)

Perhaps the most distinctive feature of the Python language is the fact that it
doesn't use curly braces to denote the extent of a block of code.  Instead of
merely furthering readability, in Python indentation is syntactically
significant.  Code blocks are indented; the level of indentation signifies
blocks of related code.

Be careful about the following:

* mixing tabs and spaces => syntax error
* inconsistent indentation within a block - one line that's indented
  differently from the rest of its block, even by a single extra space =>
  syntax error
* the first line of if statements, loops, functions and classes end with a
  colon character ':', and the next level of indentation begins after the
  first line


----------
## Importing Python Packages

> "The best line of code is the one you didn't have to write"
>     -- me


One factor which contributes to Python's considerable popularity is a large
library of pre-written Python code. Django is an example of this.  Before you
sit down to solve a programming problem, you should consider whether there
already exists a package which does what you need.

Packages of code are imported into your program with the 'import' statement.

    import time

Imported functions and variables are prefixed by the name of their package.

    print(time.strftime('%c'))

Prefixing identifiers within packages protects any variables and functions that
you have written from being overridden.  Imagine what might happen if you
imported a large package with lots of variables, so many that you didn't know
everything that was in there.  Would it cause trouble for you if that package
used a common variable name such as 'i'?  Or, consider what might happen if you
imported two packages, each containing a function with the same name.  You'd
only be able to access one of the functions.  Which would it be?


You can import a function directly into the current package, allowing access to
a function or variable without needing to add a prefix to its name.  Do this
when you're sure that it won't cause problems.

    from time import strftime  # import the strftime() function directly
    print(strftime('%c'))




----------
## What is MVC and/or MTV?

When you learn about Django (and other web or GUI frameworks) you'll see the
acronyms MVC and MTV being bandied about. These describe a style means of
organization for an application's code. The idea is to identify the broad types
of things your code will do, and keep code with the same purpose together.

MVC: [Model, View, Controller](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)

-   Model - Data stored in some kind of database (in our case this is a SQLite
    database)
-   View - Presentation for the user (HTML, CSS, Javascript; the things the
    user will see)
-   Controller - Transforms URLs into Views (think of a receptionist in an
    office building who directs you to the office you are seeking)

Django can be thought of as an MVC framework in that your code can be divided
into these categories. However, Django hackers prefer a more specific term
(maybe they want to be special?)

MTV: [Model, Template, View](https://docs.djangoproject.com/en/2.1/faq/general/#faq-mtv)



--------------------------------------------------------------------------------
## Tagging commits with the `git tag` command

You can give a git commit a nickname or a tag with the `git tag` command.
`git tag` takes the tag name as an argument and applies it to your current
commit.

    $ git tag Assn2


Once you have created a tag, it exists only on your computer until you
explicitly push it.  You must go out of your way to tell `git push` to send it
up to Bitbucket, because, by default, `git push` does not send tags.

    $ git push origin Assn2
    Total 0 (delta 0), reused 0 (delta 0)
    To bitbucket.org:erikfalor/cs2610-falor-erik-assn1.git
     * [new tag]         Assn2 -> Assn2

Make sure that you see that **[new tag]** message


You can view the list of existing tags by running `git tag` without any extra
arguments.

    $ git tag
    Assn0
    Assn1
    Assn2


When you attempt to create a tag which already exists, you get this message:

    $ git tag Assn2
    fatal: tag 'Assn2' already exists


If you make an oopsie (we all do), you can delete a tag using `git tag -d TAGNAME`.
Then you may create it again.

    $ git tag -d Assn2
    Deleted tag 'Assn2' (was 014d3d5)

    $ git tag Assn2


When you re-create a tag in your repo that had already been pushed to
Bitbucket, then try to push it again, Bitbucket will respond with an error.
This is to prevent you from accidentally overwriting a tag on Bitbucket's side.

    $ git push origin Assn2
    To bitbucket.org:erikfalor/cs2610-falor-erik-assn1.git
    ! [rejected]        Assn2 -> Assn2 (already exists)
    error: failed to push some refs to 'bitbucket.org:erikfalor/cs2610-falor-erik-assn1.git'
    hint: Updates were rejected because the tag already exists in the remote.


Remember that tags exist only on your computer until you run a command
explicitly sending it to another repository.  Likewise, the fact that you have
deleted and re-created a tag must be explicitly sent to Bitbucket.  The `-d`
argument to `git push` tells the remote repository to delete an object.  Only
after the incorrect tag has been deleted from Bitbucket may you push the
correction.

    $ git push -d origin Assn2
    To bitbucket.org:erikfalor/cs2610-falor-erik-assn1.git
     - [deleted]         Assn2

    $ git push origin Assn2
    Total 0 (delta 0), reused 0 (delta 0)
    To bitbucket.org:erikfalor/cs2610-falor-erik-assn1.git
     * [new tag]         Assn2 -> Assn2
