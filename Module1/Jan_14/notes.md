# CS2610 - Mon Jan 14 - Module 1

# Announcements


## Free Software and Linux Club Spring Opening Social

Come hang out with people who share an interest in Linux

There will be pizza!

Wednesday Jan 16, 7pm @ ESLC 053



# Topics:

*   Introducing Assignment 1
*   Validating HTML on the W3C site
*   HTML Tables
*   Valid HTML5 Document Outline


----------------------------------------------------------------------------
# Introducing Assignment 1

I've been helping many students get their Git repos ready for Assignment 0 (due
on Wednesday).  This is great!

Let's take a look forward at what is required for the following assingment

[Assignment 1](https://usu.instructure.com/courses/529849/assignments/2581299)


----------------------------------------------------------------------------
# Validating HTML on the W3C site

Since the browser is not very helpful when it comes to detecting HTML errors,
we must turn to another tool for this insight.

Look ahead to your next assingment

    https://usu.instructure.com/courses/529849/assignments/2581299

One of the requirements is that your webpage(s) correctly follow the HTML5
standard.  As explained above, your web browser is very forgiving and will not
alert you to your mistakes.

The W3C's Markup Validation Service will help you find trouble spots.
https://validator.w3.org/nu/#file


----------------------------------------------------------------------------
# HTML Tables

Last Friday we created a simple [HTML document](index.html) to illustrate basic
HTML elements.  Let's revisit that document to discover more about the
appearance and layout of HTML elements.


*   [table](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table)
*   [thead](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/thead)
*   [tr](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tr)
*   [td](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td)


----------------------------------------------------------------------------
# Valid HTML5 Document Outline

What form must a complete and correct HTML5 document have?


Compare the contents of the file `index.html` with the document shown in the
Inspector.  The following elements are present in the Inspector despite not
existing within the file:

*   html
*   head
*   title
*   body

While the contents of each webpage may be unique, there is a basic structure
which they must all follow in order to be correct.  

    <!DOCTYPE html>
    <html>
        <head> </head>
        <body> </body>
    </html>


Your browser will silently fix any problems it comes across so that users have
a pleasant experience.  This is frustrating for developers because it means
that many errors are hidden from us by the "helpful" browser.



## Questions to consider
* What sort of content belongs within the `head` element?

* What sort of content belongs within the `body` element?

* How many `html` elements may your document contain?

* Does it matter where the <!DOCTYPE> directive appears?


