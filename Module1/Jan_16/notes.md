# CS2610 - Wed Jan 16 - Module 1

# Announcements

## Free Software and Linux Club Spring Opening Social Tonight

Come hang out with people who share an interest in Linux

There will be pizza!

Tonight, 7pm @ ESLC 053





# Topics:

* Block-level vs. inline elements
* Hyperlinks
* URLs


----------------------------------------------------------------------------
# Block-level vs. inline elements

https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Block_versus_inline_elements


Most HTML elements can be placed into one of two major categories related to
how they share space with their neighbors.


#### Block-level elements
Separated vertically from adjacent elements in the source code.  These take up
the entire width of their container.

Examples of block-level elements:
    p, div, h1, h2



#### Inline elements
Not separated vertically; adjacent inline elements are strung together side-by-side.

Examples of inline elements:
    em, strong, a, img, span


You are able to visualize this by opening the Inspector and hovering the mouse
over elements in the document tree.




## Everything else:
There are a few HTML elements which do not fit into either of these categories.
Some elements are not visually represented in the body of the document; these
include tags that belong in the `head` of the document:

*   `title`
*   `meta`
*   `link`
*   `style`

There are a few non-visible tags that may also go in the body of the document.
The most common that you will encounter is

*   `script`

Of the visible tags, `table` is notable because it is in a class of its own and
is neither inline nor block-level.  It's behavior is most similar to block-level.





----------------------------------------------------------------------------
# Hyperlinks

Q: What does HTML stand for?

A: HyperText Markup Language


Q: What does the H mean?

A: Hyper; as in "hyperconnected".


HTML documents may contain links to other HTML documents.  These links are encoded as <a> elements.



## The [anchor](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a) element

`a` is for "anchor".  The content of the `a` tag is what the user will click
on.  *Where* the hyperlink takes you is encoded in the `href` attribute.

Hyperlinks <a href="https://amazon.com">Spend all of your $$$ here!</a>

For an `a` element to become active, it must have the `href` attribute.


##  The <a> element is what makes HTML "hyper"

Different cultures may disagree over which side of the book is the "front",
but we can all agree that a book has a beginning and an end with one page
following another page.  

HTML allows an arbitrary number of pages to follow from one page.
There may or may not be a "first" page of a website.
Likewise, there may or may not be a "last" page.

If there is such a thing as a "first" page, it would be index.html (or some
variation on that name: index.php, index.aspx, etc.).  This is simply a
convention that web browsers and web servers follow: If you visit a webpage and
don't specify which exact page you want to see, you'll be given a default page
with a name chosen from the list above.



## Questions to consider:
* Can HTML do everything which can be done with an ordinary, bound book?
* Can you replicate the "hyper"-ness of the web in traditional media? 


------------------------------------------------------------
# URLs

#### Uniform Resource Locator: a unique name for an object on a computer network.

## Also related: URI
#### Uniform Resource Identifier: a unique name for an object on a computer network.

https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL


## URL Syntax
https://en.wikipedia.org/wiki/URL#Syntax

    scheme:[//[userinfo@]host[:port]]path[?query][#fragment]


## Absolute vs Relative URLs

#### Absolute URLs

These include the domain name, and only work when that domain name hosts the
specified resource (webpage, image, stylesheet, etc.).

Pros:
* Specifies the main website - 1st point of contact

Cons:
* If the website moves to a new domian, these must be updated


#### Relative URLs

These do *NOT* include the domain name; the browser prefixes these with the current domain name from the address bar.

Pros:
* Shorter = less typing ;)
* Less work when moving website to another location

Cons:
* Cannot be used to refer to an outside website


We can use URLs to join webpages together.  Let's try this now.

index.html <--> aboutMe.html

