# Module 1 Readings and Resources

## Table of Contents

* Mozilla Developer Network
* What is the big problem Cascading Style Sheets aim to solve?
* How may I see what an HTML element looks like?
* Hyper Text Transfer Protocol


--------------------------------------------------------------------------------
## Mozilla Developer Network

The MDN is the most highly-regarded learning resource for web technologies. In
addition to the pages listed below I encourage you to explore and become
familiar with this excellent site. StackOverflow and Wikipedia can be good
supplements to information found on MDN. In the past W3Schools has excelled at
search engine optimization more than anything else, resulting in a lackluster
reputation among professional web developers. However, the quality of their
content has improved recently, and some students prefer their style.

### HyperText Markup Language Guides

-   [HTML Basics](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics)
-   [HTML Guides](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML)
    -   [Metadata in HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/The_head_metadata_in_HTML)
    -   [Debugging HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Debugging_HTML)
    -   [HTML Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)
-   [Exhaustive HTML Element Reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Reference)
-   [http://www.html-5-tutorial.com](http://www.html-5-tutorial.com)

While there is a standard to which HTML documents must adhere, historically,
each browser has held to their own interpretation. Moreover, each browser
vendor would create their own "convenient" HTML features in a bid to win the
allegiance of website authors. This resulted in web development becoming a big,
confusing mess for users and developers alike. The industry leaders have
recognized the error of their past ways and now strive to adhere to one common
standard. However, the code (and tutorials) of the past still affect us to this
day.

### Cascading Style Sheet Guides

-   [CSS Guides](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS)
    -   [How CSS works](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/How_CSS_works)
    -   [CSS syntax](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Syntax)
    -   [Selectors](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Selectors)
        -   [Simple Selectors](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Simple_selectors)
        -   [Combinators and multiple selectors](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Combinators_and_multiple_selectors)
    -   [Values and units](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Values_and_units)
    -   [Cascade and inheritance](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Cascade_and_inheritance)
    -   [The box model](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Box_model)
-   [Exhaustive CSS Property Reference](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)
-   [W3C CSS Validator](https://jigsaw.w3.org/css-validator/#validate_by_upload)


### Miscellaneous Topics

-   [URL](https://en.wikipedia.org/wiki/URL#Syntax)
-   [What is a URL?](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL)
-   [The Document Object Model (DOM)](https://developer.mozilla.org/en-US/docs/Glossary/DOM)

----------

## What is the big problem Cascading Style Sheets aim to solve?

Manually writing the style information in every single HTML element is tedious,
repetitive, and error prone. At best it means that we must repeat ourselves; at
worst it makes it difficult for us to apply a consistent look-and-feel across
all of the pages on our website.

CSS gives us a way to simultaneously:

1.  Apply style information to specific elements
2.  Follow the DRY principle (Don't Repeat Yourself) You write the style info
    once and it is applied everywhere, automatically

CSS is named for "the cascade algorithm" which dictates the order in which
contradicting styles are applied

Basically, the "last one" defined wins - "last one" means last in source
order... unless a more specific selector is used. The last, most specific
selector is the true winner:

    #widget { 
      font-size: bigger; /* This loses */
    }

    /* then later in the file... */

    #widget {  /* ID selector; most specific */
      font-size: biggest; /* this rule is applied */
      color: red;
    }

    .class { /* this is a class selector, and is less specific than an ID selector */
      font-size: 10px;
      color: green;
    }  
    h1 { /* this is an element selector, and is less specific than a class selector */
      font-size: 14px;
      color: orange;
    }

With CSS, you only have to specify the styles you want - the browser will supply defaults for everything else.

--------------------------------------------------------------------------------
## How may I see what an HTML element looks like?

The appearance of an HTML element in a document depends upon the complex
interaction of all CSS rules which are at in play. If your element doesn't take
on its expected appearance, how will you debug the problem?

Fortunately for you, the web browser that you use every day is actually a
capable web development IDE. To activate your browser's hidden powers, try the
following:

-   press Ctrl+Shift+I
-   press Ctrl+Shift+J
-   press Ctrl+Shift+C
-   press F12
-   press Option+Command+I (on a Mac)
-   right-click a point on the document and select "Inspect Element"
-   select the browser's main menu > Tools > Developer Tools

The window that appears contains a lot of confusing tabs; we'll learn many of
them over the course of the semester. For now we want to focus on the Inspector
(Firefox) or Elements (Chrome) tab, which gives us a peek at the HTML structure
of the webpage we're looking at.

The Inspector enables you to modify the contents of the webpage you're viewing.
This is a great way to figure things out (or create convincing faux screencaps
without Photoshop...).

_Note_: what you see in the developer tools' Elements panel is _not_ the same
thing that you see in the "View Source" (Ctrl+U) page!

To the right of the Elements (or Inspector) tab is a panel which shows all of
the CSS rules that apply to the selected element. From here you can discover
which rules are overridden by other rules. You can also disable, change, delete
and add rules to modify the appearance of the document on-the-fly. As with
changes to elements in the DOM tree, these changes are lost upon refresh.

--------------------------------------------------------------------------------
## Hyper Text Transfer Protocol

The **Hypertext Transfer Protocol** (**HTTP**) is an [application
protocol](https://en.wikipedia.org/wiki/Application_protocol "Application
protocol") for distributed, collaborative, and
[hypermedia](https://en.wikipedia.org/wiki/Hypermedia "Hypermedia") information
systems.

HTTP is the foundation of data communication for the [World Wide
Web](https://en.wikipedia.org/wiki/World_Wide_Web "World Wide Web").

https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol

Originally invented by Tim Berners-Lee at CERN in the early 90's to share
information between researchers.

HTTP works well with HTML, but you can use HTTP to send other kinds of data
besides webpages.


### Important definitions
  
_**Protocol**_: A system of rules which define how data is exchanged between systems. A protocol also defines the format of the data.

A protocol defines What to say, and when and how you say it

_**Server**_: Exists to provide services to other users/systems

_**Client (a.k.a. User Agent)**_: Makes requests of servers to provide functionality to their users

Your web browser is an example of a client.
