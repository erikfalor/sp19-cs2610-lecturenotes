# CS2610 - Fri Jan 11 - Module 1


## Intro Video
 
[1993: CNN's first reports on the Web](https://youtu.be/4aIkMwUeL_Q)


# Announcements

Complete the Class Rules & Syllabus quiz by midnight Jan 13 or you will be
dropped from the class.



# Topics:
* What is a markup language?
* Basic HTML5 Elements


----------------------------------------------------------------------------
# What is a markup language?

https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started

A markup language gives a way to describe the appearance of a document only
using plain text.


Historically, markup languages have been used to encode visual information
about a document ina medium which did not support directly storing that
appearance.

More recently, new markup elements have been added to HTML to encode structural
or other meaningful information about a document.  Search for "semantic web" if
you want to learn more about that.



#### Element: Markers which enclose or "wrap" other pieces of plain text

    <p>This is an element, complete with tags which wrap plain text</p>


#### Tag: Text which denotes the type and extent of an element

    <h1>, <p>, <b>, <strong>, <img> are examples of tags


#### Content: Text or more HTML elements contained within another element

`p` elements contain content.  The content is surrounded by an opening `<p>`
tag, and a closing `</p>` tag.  `p` elements may contain other HTML elements
and text.
    
`img` elements do not contain content.  Therefore, there is no `</img>` closing
tag.


#### Attribute: Extra information about an element which is not part of its content

Attributes in HTML look like this:

    href="https://duckduckgo.com"
    style="1px solid black"
    class="document-title"

Attributes belong *inside* of a tag, and give more information about the
meaning and appearance of a tag.

The `img` element is completely configured by its attributes.

For example:

*   The `src` attribute specifies the location of an image
*   The `alt` attribute defines an alternative text description of the image
    that is used by screen readers or in the event that an image cannot be
    downloaded
*   The `title` attribute is where you can specify the text that appears when
    you hover your mouse over the image


Learning to write HTML is largely an exercise in learning what elements exist,
how they are composed, and which attributes affect them.


----------------------------------------------------------------------------
# Basic HTML5 Elements

You must use each of these elements on [Assignment 1: Static Blog on BitBucket
(Web 1.0)](https://usu.instructure.com/courses/529849/assignments/2581299)

Their form and function are documented on the Mozilla Developer Network's [HTML
elements reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Element).
You will be expected to read and understand this documentation as part of this
assignment.

I welcome you to play with [index.html](index.html) on your own to experiment
with using these tags, as well as the browser's developer tools.

*   h1
*   h2
*   p
*   pre
*   code
*   a
*   img
*   ol
*   ul
*   li
