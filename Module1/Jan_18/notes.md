# CS2610 - Fri Jan 18 - Module 1

# Announcements


## Cyber Security Club

The opening meeting will occur next Wednesday, January 23rd @ 6pm - ESLC 053 



## FSLC InstallFest

Do you want to rock Linux on your own computer, but don't know how to start?
Get experienced help installing it!

* Try a new distribution
* Reinstall Linux
* Dual-, Triple-, or Quadruple-boot
* Repair a broken installation

Bring a laptop and a flash drive, or install on one of our machines




# Topics:
*   Cascading Style Sheets
*   Selectors: Finding our way around the DOM
*   Properties & Rules


----------------------------------------------------------------------------
# Cascading Style Sheets


## What's wrong with writing a webpage like it's 1996?

I've just implied that there is something wrong with SpaceJam.com.

*   Is there anything wrong with using HTML tags to style a webpage?
*   If you don't already know the answer to this question, what do you think a
    better approach would be like?
*   If you do know the answer, explain in your own words why it is superior.



[What is CSS trying to solve?](../Readings-and-resources.md#markdown-header-what-is-the-big-problem-cascading-style-sheets-aim-to-solve)

[CSS Syntax](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Syntax)


Instead of crafting a document using special tags you can instead use otherwise
invisible elements such as DIV and SPAN to define the structure of the
document, then define an appearance for those SPANs and DIVs in CSS.

The advantage of the CSS approach is that you can make one change in one
location and effect the entire website.  If you used combinations of visual
tags such as H1, STRONG, EM to decorate your document, you'd need to rewrite
lots of CSS and restructure your document to change how it looks.


----------------------------------------------------------------------------
# Selectors: Finding our way around the DOM

In order for CSS to be useful we need a langnuage which lets us explain to the
browser how to locate our desired elements in the document.


#### Document Object Model: a way to represent and interact with an HTML document

https://developer.mozilla.org/en-US/docs/Glossary/DOM

An HTML document can be thought of as a tree data structure (like a binary or
N-ary tree, or a directed acyclic graph if you've taken CS2410).  The DOM is
the set of functions and data structures which we can use to query and modify
the HTML document like a tree data structure.



## How can I use the structure of the document to my advantage?

CSS uses the notion of *selectors* to let us describe locations in the tree.



### Simple selectors

https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Simple_selectors

There are three simple CSS selectors

*   Element or Type selectors: simply name an HTML element
*   ID selectors: matches a unique element named by the value of the id=""
    attribute; begins with the '#' symbol
*   Class selectors: matches one or more elements which contain a name in the
    class="" attribute; begins with the '.' symbol



### How may I combine selectors?

Stay DRY and Don't repeat yourself - use the same CSS rules for many selectors!

https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Combinators_and_multiple_selectors


These composite selectors enable you to refer to elements by their relationship
to other elements.  For example, you can select P elements that are children of
a DIV, or LI elements with an ordered list but *not* those within an unordered
list, etc.


----------------------------------------------------------------------------
# Properties & Rules


Styling elements may be done with:

#### Tags which give an element an appearance (em, i, b, strong)

*   *pros:* easy and backwards-compatible with the 1990's (this is how
    http://spacejam.com was originally authored)
*   *cons:* you must change the very structure of the document to make it look
    different; you must visit every node in every document which needs to be
    updated


#### The style="" global attribute
*   *pros:* we can use the CSS "property: value" syntax that we'll soon become familiar with
*   *cons:* again, we must visit every node to apply style


#### CSS
*   *pros:* your style information is all in one place; conveniently apply the
    style to large chunks of the document in one fell swoop
*   *cons:* added complexity of a new language



## CSS Ruleset Syntax

https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Syntax



### Properties and values

The "fun" part of learning CSS is learning all of the properties which affect
elements, and learning *how* to give the desired effect.  Each CSS property
takes on a value.  Each property has its own set of allowed values.  Most
browsers will ignore a property which is given an invalid value.



#### Assignment #1 Required CSS properties

These are the CSS properties which *must* be used on Assignment #1.  You are,
of course, welcome to use any other properties you like.

*Note:* this list has been changed slightly to make it easier for you to
complete the assignment.

* [background-color](https://developer.mozilla.org/en-US/docs/Web/CSS/background-color)
* [color](https://developer.mozilla.org/en-US/docs/Web/CSS/color)
* [font-family](https://developer.mozilla.org/en-US/docs/Web/CSS/font-family)
* [font-style](https://developer.mozilla.org/en-US/docs/Web/CSS/font-style)
* [text-align](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align)
* [border](https://developer.mozilla.org/en-US/docs/Web/CSS/border)
* [margin](https://developer.mozilla.org/en-US/docs/Web/CSS/margin)
* [padding](https://developer.mozilla.org/en-US/docs/Web/CSS/padding)



### The box model

[Box Model](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Box_model)

The last three properties in the list above control how elements are arranged
on the screen.  Conceptually, each visual element is contained within a
rectangular box.

The size of the box depends upon the size of its contents, the thickness of the
*padding* between these contents and the thickness of the *border* surrounding
the box.  This border is always present, but may or may not be visible.  There
may be empty space just outside of the borders of boxes called the *margin*.



### What happens when many CSS rules apply to the same element?

https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Cascade_and_inheritance

The cascade algorithm applies rules in order according to this hierarchy:

1.  **Importance**
2.  **Specificity**
3.  **Source order in the CSS file**


Importance is denoted with the `!important` token beside a rule


Specificity refers to how specific the selector is with regard to an element.
From least specific to most specific:

    Element selector < Class selector < ID selector


Source order means that the last rule seen by the browser wins.  When one
element belongs to two classes, the last class in the final CSS file wins.
