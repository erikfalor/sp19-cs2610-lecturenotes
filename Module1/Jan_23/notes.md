# CS2610 - Wed Jan 23 - Module 1

# Announcements

## Cyber Security Club

The opening meeting will occur tonight @ 6pm - ESLC 053



## FSLC InstallFest

Tonight @ 7pm - ESLC 053

Do you want to rock Linux on your own computer, but don't know how to start?
Get experienced help installing it!

Bring a laptop and a flash drive, or install on one of our machines




# Topics:

*   Bootstrap CSS
*   How CSS is applied to the document
*   Validating CSS
*   Misc. Subjects that came up over the course of the lecture



--------------------------------------------------------------------------------
# Misc. Subjects that came up over the course of the lecture

## HTML Entities

In Java and Python you'd write "\n" to represent a newline, or "\t" to
represent a tab character.  These sequences of two characters to represent
another character are called "escape sequences".

Entities are HTML's version of escape sequences.

Some entities which you may have seen before include:

*   &gt;  for greater-than sign >
*   &lt;  for less-than sign <
*   &times; for a multiplication symbol
*   &nbsp; non-breaking space, a space character which a `p` won't eat.


https://developer.mozilla.org/en-US/docs/Glossary/Entity


## Flexbox for layout

I used to include Flexbox in the curriculum, but have dropped it this semester
in favor of including other topics.  Flexbox is a good alternative to using
HTML Tables for layout (honestly, *anything* is better than a Table when it
comes to laying things out - see the source code for https://spacejam.com if
you aren't convinced).

Flexbox is completely optional for this course, but if you want to try it I
recommend this guide:

https://css-tricks.com/snippets/css/a-guide-to-flexbox/




--------------------------------------------------------------------------------
# Bootstrap CSS

Presentation by TA Thomas O'Reilly

Bootstrap is basically a CSS library that you can drop into your project and
use immediately.  For purposes of Assignment 1 you must do *some* CSS manually,
but from here on out you're welcome to incorporate it into any of your
assignments.


Get the files for this presentation from
https://bitbucket.org/thomasoreilly/bootstrap-presentation.git



## Get Bootstrap

https://getbootstrap.com

The easiest thing to do is to include Bootstrap using a `link` tag using a
Content Delivery Network (CDN) URL.  The servers which make up a CDN are close
to you geographically, meaning that your users will download the content
quickly.  Also, because they are distributed around the world, if one server
goes down another is ready to take its place, meaning that your site is more
likely to look good no matter what the internet is doing.




--------------------------------------------------------------------------------
# How CSS is applied to the document

## <style></style> vs. style=""
Style information can be applied to (nearly) any element by way of the style="" attribute.

CSS can be placed into a document's head by writing it in a style element.

This method is known as "internal" CSS



## Supplying an external stylesheet
An external CSS file can also be linkd from within a document's head.

In this case we use a link element with the attributes rel and href:

    <link rel="stylesheet" type="text/css" href="style.css"/>




### Internal CSS vs. External CSS

* External CSS can be used across HTML files to apply the same style everywhere
* It's easier to maintain styles when the HTML code and style information is stored separately
* Move everything with the style element and paste in a new file with .css extension
* Either way you include CSS, it belongs in the head of the document


--------------------------------------------------------------------------------
# Validating CSS on the W3C site

CSS files can be validated with a W3C tool just like HTML files.

https://jigsaw.w3.org/css-validator/#validate_by_upload



