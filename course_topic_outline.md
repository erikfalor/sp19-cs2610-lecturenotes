# CS 2610 Topic Outline

## Using the command-line

*   Using the Git version control system (GitHub and/or BitBucket)


## HTML

*   HTML5 Document Structure
*   Principal HTML5 Elements
    *   HTML syntax: Tags, Attributes
    *   Inline vs. Block elements
*   Validtating an HTML5 document


## CSS

*   The Document Object Model (DOM) tree
*   CSS Syntax: Selectors & Properties
    *   Using Selectors to locate elements in the tree
*   Validtating a CSS stylesheet
*   Bootstrap CSS framework (time permitting)


## Django Web Application Framework

*   Python Basics
*   The Model-View-Controller (MVC) architecture
*   View functions
*   Django's Template System
*   The Object-Relational Mapper (ORM)
    *   Models


## Interacting with the server with HTTP

*   Requests & Responses
    *   Headers (Browser vs. Server)
*   HTML Forms & input elements
    *   Form input validation
    *   Create, Read, Update, Delete (CRUD)


## JavaScript

*   JavaScript Basics
    *   Selecting elements in the DOM tree
    *   JS Functions & interactive HTML elements
    *   JS Gotchas
*   Asynchronous JavaScript
    *   Consuming Web APIs
*   Vue.js: A JavaScript Front-End Framework


## Tie it all together with a Dynamic-Database Driven Web Application

*   Create a Web API in Django
*   Create a dynamic Web App which consumes our Django Web API 


## Learning the Browser's built-in Developer Tools

*   Inspecting and modifying the DOM
*   Using the style editor
*   Dissecting HTTP requests with the Network request tool
*   Exploring and debugging with the JavaScript


##  Web Application Security

*   Injection Attacks
*   Cross-site scripting (XSS)
*   Red team strategies
*   Blue team responses
    

##  Misc. Topics (time permitting)

*   Secure Shell (SSH)
*   File Transfer Protocol (FTP)
*   The Domain Name System (DNS)
    *   Obtaining a domain name
*   Web Hosting
    *   How to get your website hosted
    *   Host a static website on GitHub or BitBucket
