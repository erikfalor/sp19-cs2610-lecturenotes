# CS2610 - Fri Mar 22 - Module 5

# Announcements

## Due dates for remaining assignments changed

Of particular interest to you is the fact that the due date of Assn5 has been
pushed back by one week.  It is now due on April 3rd.

# Topics:
* Exam #1 Recap
* Arrow functions
* Promises and the Fetch API
* Using the JSON object resulting from `fetch()`




----------------------------------------------------------------------------
# Exam #1 Recap


__Question 3__ (68% answered correctly)
* Q:    <p> is an inline element
* A:    False!


__Question 5__ (79% answered correctly)
* Q:    Inline elements, by default, take up the entire width of the page
* A:    False!


__Question 9__ (23% answered correctly)

* Q:    What will the selector `body div p` select?
* A:    only paragraphs within a <div> which is within a <body>

`body div p` is an example of a *composite selector* which will select the
paragraph in this example: 

    <body>
        <main>
            <article>
                <div>
                    <ul>
                        <li>
                            <p>


`body > div > p` is an example of a *composite selector* which will select the
paragraph in this example: 

    <body>
        <div>
            <p>


See the lecture notes from Fri. Jan 18, and the Exam #0 recap from Friday, Feb 8.
This was Question #22 from Exam #0 where 66% of you answered it correctly.


__Question 31__ (53% answered correctly)

* Q:    What is the purpose of the <form> element's `action` attribute?
* A:    Specifies where to send the form-data upon submission




----------------------------------------------------------------------------
# Arrow functions
# Promises and the Fetch API
# Using the JSON object resulting from `fetch()`

See Readings-and-resources.md for Module 5.
