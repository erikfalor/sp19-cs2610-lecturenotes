# CS2610 - Fri Mar 08 - Module 5

# ASCII Art Star Wars

You can watch this on your own computer provided you have either the `telnet`
or `netcat` program installed.  If you are on Linux or Mac these programs are
likely already installed or are readily available.

If you are a Windows user, check the lecture notes from Jan 28 for a section
called "Experimenting with headers using `nc` (netcat)" to learn where to
download netcat.

You may watch the "movie" in telnet with this command:

    telnet towel.blinkenlights.nl


You may use netcat like so:

    nc towel.blinkenlights.nl 23




# Announcements

## Exam #1 - the week after Spring Break

Available in the Testing Center from Tue 3/19 - Thu 3/21

We will hold a review on Monday 3/18



## DC435 Capture The Flag system

Zodiak (Matt Lorimer) of USU IT presented his CTF system last night at the
DC435 meetup.  You're invited to get an account and search for flags in this
excellent simulation.

1. Visit http://bit.ly/dc435 to get onto the DC435 Slack

2. Join the #ctf channel and introduce yourself to @Zodiak to get your own
   account on the CTF system

3. ???

4. Profit!



============================================================================
# Call on 3 designated questioners
============================================================================

# Topics:
* SSH and the Web Developer Roadmap
* Remote shells 1.0: telnet and rsh
* Remote shells++: SSH
* Network connections and port numbers
* SSH tunnels



----------------------------------------------------------------------------
# SSH and the Web Developer Roadmap

[WebDev Roadmap](https://github.com/joshuajosh59/Webdeveloper-roadmap)


## Mud card question:

* Which part of this map (if any) are you most interested in?


So far you've been working with a webserver that runs on your laptop.  However,
a real-life production system won't be running from your personal workstation.
It likely won't even be running on a machine that is within driving distance of
your office.

Controlling a web site means either:

1. Using the control panel webpage provided by a web hosting company. This
   works so long as the web host has provided all of the tools that I want to
   be able to use (not likely, since their target audience are all muggles).

2. Connecting to a command console on the server and directly running commands.
   This puts you in complete control.  And so long as the server has a command
   shell and familiar tools, you won't have to re-learn a new control panel
   interface every time you encounter a new web hosting provider (or when 
   the web host upgrades the control panel at their own whims).

This semester we've been using the command line because many web technologies
were created in this environment.  You are not likely to become a well-rounded
web developer by avoiding contact with the command line.

SSH enables you to securely connect to and administer remote systems from
anywhere in the world, and gives you the most control of any interface
available.  Let's take a look at why security is such an important
consideration.





--------------------------------------------------------------------------------
# Remote shells 1.0: telnet and rsrh

The earliest remote shell programs offered users the convenience of connecting
to remote systems over the internet, albeit without the protection of
encryption.

Wireshark (https://wireshark.org) is a tool that allows me to snoop on network
traffic. I'll use it to illustrate the shortcomings of a non-encrypted remote
shell program:

<Demo: telnet jim@localhost w/ wireshark>

You can see everything the server prints out, including the prompts. You also
can see every keystroke I make, including my username and password. They're
helpfully highlighted by the prompts!


--------------------------------------------------------------------------------
# Remote shells++: SSH

The Secure SHell (SSH) prevents this information leakage by encrypting the
entire conversation using secret keys which aren't exchanged directly over the
wire, so an eavesdropper cannot decrypt our conversation.

The details of how the secret key exchange works is outside of the scope of
our class.  However, I highly encourage you to read up on it over spring break.
It's fascinating stuff!

[Diffie-Hellman key exchange](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange)


SSH is invoked from the command line like this (optional arguments are
surrounded by square brackets):

    ssh [-p port] [-R address] [-L address] [-D port] [user@]hostname


If you leave off the username, SSH substitutes the name under which you are
currently logged in.  If you leave off [-p port], port 22 is used.



<Demo: SSH to jim@localhost w/ wireshark>



--------------------------------------------------------------------------------
# Network connections and port numbers

Communication between two computers across a network requires two pieces of information:

* IP address
* Port number

The IP address identifies a machine on the network.  Domain names (or host
names) are human-friendly synonyms for numeric, machine-friendly IP addresses.
The special IP address `127.0.0.1` is used by a computer to refer to itself.
The hostname `localhost` is the synonym for `127.0.0.1`.

A port number identifies a service on that machine.  A port number is a 16-bit
unsigned integer, covering the range [0..65535].  Some port numbers are
reserved for the use of specific services.

Port | Service
-----|---------------------------------------------
20   | File Transfer Protocol (FTP) data
21   | File Transfer Protocol command
22   | Secure Shell (SSH)
23   | Telnet
25   | Email - Simple Mail Transfer Protocol (SMTP)
43   | WHOIS
53   | Domain Name System (DNS)
80   | Hyper Text Transfer Protocol (HTTP)
443  | HTTP over Secure Sockets Layer (HTTPS)
666  | Doom PvP Deathmatch
3389 | Microsoft Remote Desktop Protocol (RDP)
8000 | Django development server
8080 | HTTP alternative


As you've noticed as you've worked on Django, you can specify a port number
(i.e. 8000) after the hostname in a URL, separating the port number with a
colon ':'.

You might think of port numbers as analogous to channels on a TV.  You tune
into channel 80 to talk to the HTTP server, and use port 443 to talk to the
server securely.  The `nslookup` program talks to a DNS server over port 53,
etc.


[Ports on Wikipedia](https://en.wikipedia.org/wiki/Port_(computer_networking))


--------------------------------------------------------------------------------
# SSH tunnels

Another SSH feature is called "tunnels", which are secure channels for other
data apart from the secure shell to flow.  There are many uses for this - I'll
illustrate just a few for you today.

For purposes of today's lecture I'll use the hostname `viking-dyn` as the
SSH server. In all of my examples you may replace that with the hostname of
*your* SSH server.

When dealing with tunnels, we name the two ends of the tunnel **local** and
**remote** from the point of view of the *SSH client*.  Each end of a tunnel is
defined by its *IP address* and its *port number*.  Network traffic is directed
to a port on one machine, flows through the tunnel, and comes out at the other
side.  The traffic will enter from one IP:port and come out with a new IP:port.

Keep these definitions in mind throughout this discussion:

#### Local: the end of the tunnel at the SSH Client (e.g. your laptop)

#### Remote: the end of the tunnel at the SSH Server



## Local port forwarding

This causes a port on your laptop to be connected through a tunnel to a port on
the machine hosting the SSH server.  Traffic from your laptop appears to come
out at the server.


### Use case: Access systems protected behind a firewall

I have a router in my home network.  It has a webpage from which I administer
it.  To prevent hackers from taking over my home network, this admin webpage is
only available from within my home network.  What if I need to fix something on
my home network for my family while I'm here at school?  Do I need to run home
to fix it?

If only I had a way to "bounce" a connection from my laptop here at work
through some device within my home network into my router...

Now, I do have a Raspberry Pi in my house which has an internet-exposed SSH
server.

I'll connect to my Raspberry Pi (named viking-dyn) and command SSH to tunnel
port 9000 on my laptop through the SSH connection into viking-dyn, connecting
the other end of the tunnel to port 80 on my router:

    $ ssh viking-dyn -L9000:router.asus.com:80

The syntax of the -L argument breaks down like this:

    -L local port ':' remote address ':' remote port

So long as this SSH connection is alive, I can go to http://localhost:9000 on
my own laptop and it will go to my router by way of my Raspberry Pi.

The "remote address" is a hostname which is reachable from the SSH Server.
The address 'router.asus.com' is the hostname of my own router from the
point-of-view of my Raspberry Pi.

The connection formed by this command is made from the *perspective of the
remote machine*.  What computer does the name `localhost` refer to?  It depends
on which computer you're on!  I may connect to a service *on* my Raspberry Pi
itself by using "localhost" as the *remote* address.  Here's a coinflip service
I run from my house:

    $ ssh viking-dyn -L9001:localhost:5050
    $ nc localhost 9001


You may forward multiple connections on one command line by repeating -L:

    $ ssh viking-dyn -L9000:router.asus.com:80 \
        -L8000:printer.falor:80 \ 
        -L9001:localhost:5050 


### Adding new tunnels at runtime (Optional)

I've showed how to set up tunnels using command line arguments to the ssh
program. If you're using the OpenSSH client you can add new tunnels without
closing down your SSH process, using the same syntax as the command line
arguments.

To do this I need to tell OpenSSH to *not* pass the next keystrokes I type
through the connection to the remote system. I do this by using the SSH escape
key. By default this key is the tilde `~`

1. Type the SSH *escape* key (tilde) to put the OpenSSH client into command
   line mode SSH only recognizes the escape key if it immediately follows a
   newline. Just to be safe, it's best to hit Enter before typing the escape
   key.

2. Enter 'C' to put OpenSSH into command line mode

3. Enter a command line argument in the prompt just as you would enter it if
   you were invoking the ssh program on the command line

You can use the escape sequence '~?' to cause OpenSSH to print out a help
screen of the available commands.



### Use case: Covering your tracks

I can make it appear to the internet that I'm accessing certain webpages from
my home network instead of USU campus.  The 'remote address' in the -L argument
can be *any* address reachable from my SSH server (in this case, my Raspberry
Pi).

http://checkip.dyndns.org

    $ ssh viking-dyn -L9002:checkip.dyndns.org:80

Now, if I connect to http://localhost:9002 from my laptop, I reach
checkip.dyndns.org having gone through my Raspberry Pi at home.  So far as
dyndns.org can determine, I'm connecting from my house, not from USU campus!

Theoretically, I could use this to tunnel to any website of my choosing; if I
have enough local ports I can map each one to a separate web site and give them
the impression that I'm connecting from my SSH server.

In practicality, systems such as HTTPS with hostname-based certificates make
this brittle.  Moreover, nearly all webpages include resources from lots of
different addresses, and this only lets me tunnel into a few at a time.





## Dynamic port forwarding (Poor man's VPN)

Cause a port on your laptop to forward requests from a web browser through the
SSH Server.

Suppose I work at an office with a web filter which prevents me from visiting
Stack Overflow or other websites because "muh social media and inappropriate
content".

> True story: 
>
> Experts-exchange.com is a site similar in intent to Stack Overflow.  It was
> once hosted on the domain 'expertsexchange.com'.  Our HR department didn't
> approve of employees visiting that site while on the clock.

Perhaps your IT department trusts a creepy Russian antivirus program which
installs its own certificates so they it decrypt my traffic as it moves
through their router.

Or, suppose that you're at McRestaurant using McFree WiFi.  You may not
trust WiFi networks that you don't pay for.  For one thing, it is provided by a
corporation which doesn't regard its customers well enough to feed them real
food.  For another thing, there is a dude sitting across from you with what
appears to be a WiFi Pineapple sticking out of his laptop.

You can use local port forwards to protect traffic on a host-by-host basis, but
it would require you to manually set up lots of port forwards, and you would
still have to remember which port goes with which website.  Any websites which
use absolute URLs will break (this would be almost all of them).

It would be nice if you could tell your browser to bounce *all* of its requests
through the SSH server running on a trusted system, and do all of the work of
keeping everything straight.

OpenSSH has you covered as it provides an interface known as a SOCKS proxy
which scales my tunnels up to a usable level.  It's a bit like using a VPN
connection on an application-by-application basis.

    $ ssh viking-dyn -D12345

Next, configure the web browser on your laptop to use local port 12345 as a
SOCKS proxy.  Each request will securely go through an encrypted tunnel to my
Raspberry Pi and all of my web traffic will appear to originate from my house.
All of it.


### Use case: Test a webservice from many locations

If I ask DuckDuckGo [what is my ip address?](https://duckduckgo.com/?q=what+is+my+ip+address),
it reports that I'm at USU.  What happens when I ask the same question from
another computer?




## Remote port forwarding

Make a port at the server connect to a port on your laptop

Here's the Django server I've been developing on my laptop:

    http://localhost:8000/hello/highFive

Right now, I need somebody to visit that URL and tell me how nice it looks.


Oh, you can't reach my laptop through the domain name `localhost`? Now try

    http://voyager2.bluezone.usu.edu:8000/hello/highFive

How does that look? Impressive?



### Use case: Let a customer test a web site under development on my laptop

Suppose that I'm a freelance webdev hacker working on my client's cool new web
site on my own laptop.  I don't want to give my client full access to my laptop
because I have other stuff on there that's not for them.

Yet, the client demands to know that I'm making progress.  I haven't yet gotten
the kinks all worked out on the testing server.  The app works *great* on my
laptop, but it isn't exactly impressive (or practical) bring my laptop to their
office to show them how awesome it is (maybe they are on the other side of
the country).

Remember that my Raspberry Pi *is* on the internet.  What if I could ask my
client to connect to my Raspberry Pi, and have that connection come directly
into my own laptop.  Now the client can see what I'm working on right now,
without having to physically look over my shoulder.  My private laptop is only
online so long as I keep that SSH connection open, so I can cut it off at will.

For this to work, I must edit my Raspberry Pi's SSH server configuration file
and enable the `GatewayPorts` setting:

    GatewayPorts yes


From the machine *running* Django I issue the command

    $ ssh viking-dyn -R8000:localhost:8000


Now, will someone please visit http://unnovative.net:8000/hello/highFive?


The syntax of the -R command breaks down like this:

    -R remote port ':' local address ':' local port

The 1st number is the port which you may connect over the internet to my
Raspberry Pi.

The "local address" in the middle is the hostname of the machine hosting the
Django server *from the point of view* of the SSH command.  Because the SSH
command is running on the same machine as Django, the correct hostname is
`localhost` which means "this very same machine".

The 3rd number is the port on my laptop which my Django server is listening to
connections on. 



#### ngrok: remote port forwarding as a service

If you don't have your own Raspberry Pi on the internet, you can use a 3rd
party service such as https://ngrok.com/ to do this.

You install a program on your computer which does the SSH connection for you.
On ngrok's end, they give you a nice, memorable domain name which connects back
through your tunnel onto your laptop.
