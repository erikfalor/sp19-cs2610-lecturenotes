# CS2610 - Wed Mar 27 - Module 5

# Announcements

## FSLC

Cal Coopmans of AggieAir

Wednesday, March 27th
7pm - ESLC Room 053




## Utah OpenSource Intercollegiate Cyber Sparring Event

Practice your CyberSec skills in a friendly environment.

Saturday, Mar 30 12pm through 6pm
ESLC 053

You'll need to install a piece of VPN software called
[ZeroTierOne](https://www.zerotier.com/download.shtml) to connect to the Utah
OpenSource VPN to play.

On ZeroTierOne's homepage you may see an insane command line which installs
their software.  Don't do it.  [Whoever puts instructions like this on their
website needs to be drug out into the street and
shot](https://www.idontplaydarts.com/2016/04/detecting-curl-pipe-bash-server-side/).
Just download the installer for your OS and install it like a normal person.


After ZeroTierOne is installed, launch the zerotier-one service

    $ zerotier-one -d

Next, join a ZeroTier network, and wait for your new IP address.  The
hexadecimal number is the ID of the UTOS cybersecurity sparring network:

    $ zerotier-cli join a0cbf4b62a48c5f9




## Computer Science Department Spring Social and Awards Dinner

April 16, 2019
5 – 7 pm
Haight Alumni Center, USU Campus

* Free Dinner - Taco Bar & Drinks from The Italian Place
* Prizes
* RSVP is required. RSVP to cora.price@usu.edu number of adults & kiddos, & dietary notes



# Topics:
* Write a Django App which provides an API
* REST: Representational state transfer

----------------------------------------------------------------------------
# Write a Django App which provides an API

Let's write an API endpoint in Django which takes a GET request and computes a
Fibonacci number requested in the `n` GET parameter.


## Mud card Activity

Before we begin writing code, let's take a few minutes to design it.
On your mud card, sketch what you think this view function will look like:

- What are your inputs?
- What is your output?
- What error conditions must you be prepared for?
- What helper functions will you need?


Here's the outline I came up with:

* Hook our view up in fib/urls.py
* Get input from request.GET
    - Loop over GET variables and print them out
* Write the fibonacci(n) helper function
* Create a dictionary to send as a response
    - Respond to a valid request with the user's requested data
    - Respond to an invalid request with a helpful error message
* Return a JsonResponse object
    - Convert our dictionary into JSON
    - `from django.http import JsonResponse`


As we write this code, jot any questions you have for me on your mud card.


## The finished product

Find the code in https://bitbucket.org/erikfalor/sp19-cs2610-djangoproj/src/master/



----------------------------------------------------------------------------
# REST: Representational state transfer

[REST on Wikipedia](https://en.wikipedia.org/wiki/Representational_state_transfer)

REST is an architectural style that defines a set of constraints to be used for
creating web services.  Web services confirming to the REST style are said to
be "RESTful".  RESTful web services are characterized by supporting stateless
operations.

The unit conversion API you're writing for Assignment 5 is an example of a
RESTful API.

"State" in this context refers to any information about requests which exists
between requests.  This data may be stored in a database (e.g. account
information), maintained as global variables in the server (ick!), or stored on
the client side (e.g. Cookies).

Being stateless just follows from being delivered over HTTP, which is itself a
stateless protocol.  Stateless means that each request stands alone, and no
information about the client is stored on the server.  This property enables
RESTful services to be spread across many different machines, providing
horizontal scalability.

It's not possible or desirable to always REST to its logical conclusion.  If
Canvas were stateless, you'd have to authenticate every time the browser made a
request.  This means that you'd have to re-log in each time you clicked a link,
and you'd need to give your credentials every time Canvas executed a `fetch()`
on your behalf.
