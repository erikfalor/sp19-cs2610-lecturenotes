# CS2610 - Fri Mar 29 - Module 5

# Topics
* Mud Card Review
* Hosting a website with an external hosting provider
* File Transfer Protocol (FTP)



--------------------------------------------------------------------------------
# Mud Card Review

## JavaScript and the future

> Have we switched from JavaScript completely?

1. In this class?  No, we'll be using JavaScript through the end of the semester.

2. As an industry?  No, we rely on JavaScript more than ever.  Despite the
   popularity of Web APIs which enable us to distribute work across the net,
   the importance of server-side computational resources has been diminished
   since the early days of the web.  Back then 100% of computation took place
   on a server.  Now we are able to do much more on the client-side, though
   servers are still of vital importance.

3. As a language?  Not really, and I don't think that we ever will.  JavaScript
   has reached such a high degree of mind-share that I doubt it'll ever be
   replaced.  Too many people love it as a language.  It is the **C** of the
   new millennium.  While there are "new" client-side languages (e.g.
   TypeScript, CoffeeScript, Elm), these are ultimately transformed into
   JavaScript before the browser can run them.  My prediction is that
   WebAssembly will supplement, but not completely replace JavaScript.


## Making Recursive Fibonacci efficient

> Can you easily store values of Fibonacci to allow the recursion to go even
> further without using massive amounts of memory?

Yes you can.  This technique is called [memoization](https://en.wikipedia.org/wiki/Memoization)

Here is a simple implementation which uses a dictionary to cache values of
previous calls to `fibonacci()`.  Notice how this cache is pre-populated with
the base-case values, simplifying our code at the expense of an icky global
variable:

    fib_memo = { 0: 0, 1: 1 }
    def fibonacci(n):
        if n not in fib_memo:
            fib_memo[n] = fibonacci(n-1) + fibonacci(n-2)
        return fib_memo[n]


There are more sophisticated ways of doing this.  You can read more here:
[Memoization + Python on SO](https://stackoverflow.com/questions/1988804/what-is-memoization-and-how-can-i-use-it-in-python)



--------------------------------------------------------------------------------
# Hosting a website with an external hosting provider

So far in this class we've hosted static websites on Bitbucket and have run a
development web server on our own computers.  Today we'll look at how to set up
a website with a dedicated hosting service.


## 0. Choose a hosting provider

The zeroth thing to do is to pick a company to host your website.  Things to
consider are:

* What technology do they support?
    - If you want to host a static webpage or a webpage written in PHP, you'll
      have plenty of options.
    - If you want a place to host your class projects written in Django, you'll
      need to do some research to find a good home for your code.

* Cost:  How much are you willing to pay to host your site?
    - Is a domain name included in the package?
    - How much storage do you get?
    - How much bandwidth are you allowed to use per month?
    - What happens when you exceed the bandwidth limit?
    - Is there a CPU limit?

* Service:  When something goes wrong, how responsive will they be?
    - Do you get an email account for your site?
    - Are backups included?
    - What if you get hacked?  Do they help you fix that, or are you on your
      own?
    - How long of a contract are you entering into?


Once you choose a hosting company you buy a hosting package with the level of
service you need for this site.

Today I'll show you what this looks like with WestHost.  WestHost is a local
web hosting company who was kind enough to provide a free hosting package to
our class for this demo.  WestHost is a PHP shop, so I'll only be able to show
you static pages today.


Today's website is found at `http://aggies.us`

* [My awesome site at http://aggies.us](http://aggies.us)

Your hosting package with the hosting company will often include a web-based
interface to control your website.  This is how muggles are able do get things
online despite being quite unaware of our wizardry.  WestHost provides a common
control panel called **cPanel**.

* [My WestHost control panel](https://chi.westhost.com)


### Things you are able to do from cPanel

-   Manage files on the server
-   Backup and restore files
-   Manage the databases used by my PHP web apps
-   Analyze website metrics
-   Install common web apps and libraries in Softaculous
-   Create a quick and easy webpage from a template
-   Download access logs


## 1. Get a domain name

Sometimes a domain name is included in the hosting package you purchase.

If you purchase a domain name through your hosting provider, they'll create the
DNS records to point at the web server you're buying from them.  Easy-peasy.
This is how I got a webpage up and running at http://aggies.us.  I spoke with a
nice person at WestHost, told her the name I wanted for my website, and voila.
So easy a muggle can do it.

Alternatively, you can bring your own hostname with you.  Earlier in the
semester we registered a domain name on www.dot.tk.  Let's see if the domain
name **fadein.tk** is available and make it point to my website hosted on
WestHost's servers.


### Register the domain **fadein.tk**

* Login to https://my.freenom.com
* Choose a domain name **fadein.tk**
* Use Freenom's DNS, point the A records to the same IP address as **aggies.us**
* Play the waiting game.  After about 3 minutes fadein.tk should resolve.

    nslookup fadein.tk                 # USU's default DNS
    nslookup fadein.tk 80.80.80.80     # Freenom DNS
    nslookup fadein.tk 208.67.220.220  # OpenDNS


What happens if I try to visit my new website **fadein.tk** in its current state?

I'm sharing this server with other customers. By accessing the real IP address
I haven't given WestHost enough information to know which website to call up.

Do you remember when we talked about HTTP headers?  When I go to a website, one
of the things the browser tells the server is which Hostname I think I'm
talking to with the `Host` header.  This enables one HTTP server to serve many
different websites.  Since I'm not paying for my own personal *dedicated*
server, I'm sharing one machine with one IP address with other cheapskates.

This server does not know who's webpage to serve up when it gets requests with
`Host: fadein.tk`, so I'll need to arrange things such that WestHost recognizes
**fadein.tk** as belonging to my account.


### Connecting fadein.tk to my WestHost service

1. Login to https://chi.westhost.com
2. Open cPanel -> Domains -> Domains
3. Click the button "Create a New Domain"
4. Enter the name **fadein.tk** and click *Submit*

Now when my browser visits `http://fadein.tk` I am greeted with my simple
landing page.  How boring.


## 2. Create some interesting content

We've talked about using SSH for website administration before.  Let's use it
to put custom HTML on this server.

SSH access is not enabled by default by WestHost.  I had to speak to a
representative to enable it.  YMMV.

The files comprising my website are in a directory called `public_html`.
Let's go in there and see what we can do from the command line.



--------------------------------------------------------------------------------
# File Transfer Protocol (FTP)

What if I want to do more than just edit an HTML file from within a terminal?
Suppose I want to put my Fibonacci Tree here - a site in three files.  There's
got to be a better way to move files in bulk, right?

* [FTP on Wikipedia](https://en.wikipedia.org/wiki/File_Transfer_Protocol)

> The File Transfer Protocol (FTP) is a standard network protocol used for the
> transfer of computer files between a client and server on a computer network.
>
> FTP is built on a client-server model architecture using separate control and
> data connections between the client and the server.

FTP is a *very* old protocol.  It has been working over TCP/IP networks since
before I was born, and was invented about 10 years before *that*.

Like HTTP, FTP reports status using three-digit status codes such as the following:

| Code | Human-friendly Message
|------|-----------------------
| 230  | User logged in, proceed. Logged out if appropriate
| 331  | User name okay, need password
| 430  | Invalid username or password
| 501  | Syntax error in parameters or arguments


FTP does have a few odd quirks.

* FTP uses *two* TCP ports per connection.  Port 21 is used to send commands
  and responses back and forth.  Port 20 is used to send file data.  This means
  that while bulk data is being transferred the server and client have a free
  channel of communication.

* FTP distinguishes between *binary* and *ASCII* data.  The distinction is
  important when sending text files between Unix and Windows systems as the
  correct End-Of-Line character is applied on the receiving end (e.g. "\n" in
  Unix text files are converted into "\r\n" for Windows).  Other text data
  conversions may be applied as well, which will corrupt binary data.


While many web browsers can access FTP servers, most of them only support
*downloading* from FTP servers.  We need to *upload* files.  To do this we'll
need an FTP client.


### [Filezilla](https://filezilla-project.org/)

Filezilla is a graphical FTP client.  Once logged in to ftp://aggies.us, it
displays local files on my laptop on the left and remote files on the right.
To transfer files in either direction, I simply navigate the file structure and
drag them to their destination.

Through an FTP client I can do nearly any file-manipulation task that I could
do from a familiar File Explorer, including deleting, renaming, copying and
moving remote files.  Filezilla even lets me edit a remote file using a local
editor.  When I close the file on my computer, Filezilla re-uploads it to the
server!
