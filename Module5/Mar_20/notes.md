# CS2610 - Wed Mar 20 - Module 5

# Announcements

## FSLC - Belated Pi Day celebration

We wanted to share with you some cool projects to do on a Raspberry Pi on Pi
day, but it landed on Spring Break this year.  Better late than never!

Wednesday, March 20th
7pm - ESLC Room 053



# Topics:

* Assignment #5: Worth Your Weight In Gold (Web 2.0)
* Web Application Programming Interfaces (Web APIs)



----------------------------------------------------------------------------
# Assignment #5: Worth Your Weight In Gold (Web 2.0)

https://usu.instructure.com/courses/529849/assignments/2581304

Key points
----------

* Your project will combine Django and JavaScript code.  Your JavaScript code
  may be embedded within your HTML templates in script elements, or may be
  served by Django as static files

* Your project *must* consist of two Django apps: one providing the User
  Interface, and one providing a web API.

* Your webpage needs to fetch data from your web API *and* from a 3rd party
  API.  Don't fetch this data from the Django server-side; the fetch *must*
  happen on the client-side.

* The Django app providing the web API *must* use a Model to store the unit
  conversion values.  It is okay to use a dictionary while you're developing
  it, but the unit conversion data *must* ultimately come from a database.



<Demonstrate the app>



----------------------------------------------------------------------------
# Web Application Programming Interfaces (Web APIs)

## Mud card questions

* What is an Application Programming Interface (API)?

* How does API relate to our role as software creators?

* What are examples of APIs you've used before?


[Module 5 R&R](../Readings-and-resources.md)


### Some APIs to try out

#### Lorem Ipsum generator

https://loripsum.net/

    $ curl https://loripsum.net/api/
    $ curl https://loripsum.net/api/5/long
    $ curl https://loripsum.net/api/5/long/allcaps
    $ curl https://loripsum.net/api/5/long/allcaps/plaintext


#### Dad jokes

    $ curl -H "Accept: application/json" https://icanhazdadjoke.com/
    $ curl -H "Accept: text/plain" https://icanhazdadjoke.com/


#### RoboHash Image Generator

    https://robohash.org/erik-falor.png
    https://robohash.org/erik.falor.png


#### MusicBrainz API

https://musicbrainz.org/doc/Development/XML_Web_Service/Version_2

    $ curl -A "cs2610 (erik.falor@usu.edu)" "https://musicbrainz.org/ws/2/recording?fmt=json&limit=3&query=waking+season" | python -m json.tool
    $ curl -A "cs2610 (erik.falor@usu.edu)" "https://musicbrainz.org/ws/2/artist?fmt=json&limit=3&query=the+appleseed+cast" | python -m json.tool
    $ curl -A "cs2610 (erik.falor@usu.edu)" "https://musicbrainz.org/ws/2/artist?fmt=json&limit=3&query=the+appleseed+cast" | python -m json.tool


#### The Internet Archive Wayback Machine

* [Checking for a snapshot](https://archive.readme.io/docs/website-snapshots)

    $ curl "https://archive.org/wayback/available?url=spacejam.com"


* [Making a snapshot](https://archive.readme.io/docs/creating-a-snapshot)

    $ curl -X POST -H "Content-Type: application/json" -d '{"url": "pokemon.com"}' https://pragma.archivelab.org
