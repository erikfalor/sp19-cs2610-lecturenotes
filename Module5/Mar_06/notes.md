# CS2610 - Wed Mar 06 - Module 5

# Announcements


## FSLC - Scheme: the reason your programming language has any redeeming qualities at all

Bring a laptop with an SSH client (OpenSSH or PuTTY) so you can play along.

Wednesday, March 6th @ 7pm
ESLC 053




## DC435 Capture The Flag

Make sure you bring a laptop with RDP (Remote Desktop Protocol) and/or SSH
clients (e.g. PuTTY) so you can play!

You'll be logging into a Kali Linux box from which to do your hacking.

Thursday, March 7th @ 7pm
Bridgerland Main Campus - 1301 North 600 West - Room 840




## Exam #1 - the week after Spring Break

Available in the Testing Center from Tue 3/19 - Thu 3/21

We will hold a review on Monday 3/18



# Topics:

* The HTTP Hosts header
* How the Domain Name System helps you find things online
* Adding an entry to the DNS



----------------------------------------------------------------------------
# The HTTP Hosts header

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Host

## HTTP Protocol and Headers (review)

A client initiates a communication in HTTP by sending a request to a server. 
An HTTP request begin with a block of headers in the form of

    METHOD PATH VERSION
    Header0: value0
    Header1: value1
    ...
    <blank line>
    Content data follows...



### An example of HTTP headers, as they appear "on the wire":

    POST /unitconv HTTP/1.1
    Host: eriks-cool-django-site.com
    User-Agent: Mozilla-Compatible, but I'm Chrome, and I'm on Linux
    Cookie: chocolateChip=Yummy;
    Content-Length: 1024

    CONTENT GOES HERE, ALL 1024 BYTES OF IT...


All of that information can be used by the server as it decides how to respond
to your request.



## Django echo server demo

What other information about your user agent can the server see?

<Demo: proj/src/headers>




----------------------------------------------------------------------------
# How the Domain Name System helps you find things online

The Domain Name System (DNS) is a hierarchical decentralized naming system for
the internet DNS translates human-friendly hostnames into IP addresses that are
meaningful to routers and other network equipment.

DNS is essentially a database mapping hostnames to IP addresses. To promote
fault-tolerance and also to spread the load across many machines, the database
is split across many servers across the world.
    
In other words, there is no single authoritative DNS server in the world. This
responsibility is shared across many systems across the net.



#### Try not to cringe too hard:

* https://www.verisign.com/en_US/website-presence/online/how-dns-works/index.xhtml

* https://en.wikipedia.org/wiki/Domain_Name_System

## The DNS is Hierarchical

In order to spread the load, a hierarchical, recursive structure exists so that
the many requests from the bottom may be served by a server above it.  If the
server on top doesn't know the IP address corresponding to a particular
hostname, it asks the server above it. This process is repeated (recursion)
until master server at the very top level is consulted.



## The DNS is Decentralized

In the bad old days, each computer kept its own database of host-to-IP
mappings. This meant that whenever the network changed (e.g. new hosts
added, old hosts removed or renamed) each system administrator needed to
manually update his own host database. (This database still exists on Unix
and Windows machines - the /etc/hosts file. If you're up for a challenge,
try finding this file on your Wintendo). This scheme represents the maximum
amount of decentralization possible.

The other end of the spectrum would be having a single, unifying database.
This would cut down on the amount of work spent on maintaining the
database, but that one database would be *busy*. And if something happened
to it, nobody could get anywhere on the net. Also, if the only DNS server
was in the U.S. and you weren't, then you'd enjoy trans-oceanic delays
everytime you visited a webpage, independent of where the webpage itself
was hosted.

The current system represents a good middle ground between these two
extremes.



## Common DNS Record Types

| Abbr. | Name               | Description
|-------|--------------------|-------------------------------------------------------------
| A     | Address            | A 32-bit IPv4 address
| AAAA  | IPv6 Address       | A 128-bit IPv6 address
| CNAME | Canonical Name     | Alias for another hostname
| MX    | Mail Exchange      | Map a domain name to a list of message transfer agents (makes email work)
| NS    | Name Server        | Which Name Server is authoritative for a zone (which NS to use for your domain)
| SOA   | Start Of Authority | Contains the authoritative information for a zone
| TXT   | Text               | Originally for arbitrary human-readable text, but now used by machines



## Interrogating DNS servers

Your computer has programs which act as DNS clients and can communicate to DNS
servers.  These programs send and recieve traffic over TCP and UDP port 53,
which is reserved for DNS.

| Tool         | Description
|--------------|------------------------------------------------------------
| nslookup     | Look up a hostname's DNS information (also available on Windows)
| dig          | Look up a hostname's DNS information
| whois        | Interrogate the WHOIS database for ownership information of a domain
| netstat -Wpt | See which domains/IP addresses are connected to your computer (Linux)




## Choosing your own DNS server

DNS Servers themselves have IP addresses.  Ordinarily, your computer or device
broadcasts a standard query to the router on an reserved IP address to find out
what DNS server to use on the local network.

The "default" DNS server may be owned by your ISP or another unsavory entity
which you may not fully trust with your browsing information.  Or, you wish to
use a custom DNS server who's database purposefully does *not* contain certain
domain names, for malware-prevention or content-filtering purposes.  It may
also be the case that the pervailing DNS server is very slow.

For these and other reasons it is a good idea to learn how to manually
configure DNS settings on your own devices.  This means that you should learn
the IP addresses for a few DNS servers.  Out of all of the IP addresses in the
world, these are good ones to memorize; if your DNS is misconfigured, how else
would you find anything on the net (including a working DNS server)?

### Cloudflare DNS

* 1.1.1.1 
* 1.0.0.1 

### OpenDNS

* 208.67.222.222
* 208.67.220.220

### Google DNS

* 8.8.8.8
* 8.8.4.4

### Verisign DNS

* 64.6.64.6
* 64.6.65.6



----------------------------------------------------------------------------
# Adding an entry to the DNS


* [Free .tk domains](http://www.dot.tk/)


## Now that you have a Domain Name connected to your server...

...how does a browser use that to find the server?


