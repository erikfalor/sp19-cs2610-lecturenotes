# CS2610 - Mon Mar 04 - Module 5

# Announcements


## SDL/USU Technical Lecture - The Integrated Engineer

Preparing Yourself to Successfully Integrate into a Multidisciplinary Team
presented by Ashley Willardson, Software Tester for the Space Dynamics Laboratory

Free pizza after lecture. 

Tuesday, March 4th @ 4:30pm
ENGR 201. 



## FSLC - Scheme: the reason your programming language has any redeeming qualities at all

> A language that doesn't affect the way you think about programming,
> is not worth knowing.
>
> -- Alan Perlis

Take a step towards enlightenment by exploring Scheme, a practical and simple
dialect of LISP.  This talk will demystify those concepts functional
programming novices find the most confusing, giving you a whole new way of
thinking about programming.

Bring a laptop with an SSH client (OpenSSH or PuTTY) so you can play along.

Wednesday, March 6th @ 7pm
ESLC 053




## DC435 Capture The Flag

Make sure you bring a laptop with RDP (Remote Desktop Protocol) and/or SSH
clients (e.g. PuTTY) so you can play!

You'll be logging into a Kali Linux box from which to do your hacking.

Thursday, March 7th @ 7pm
Bridgerland Main Campus - 1301 North 600 West - Room 840



# Topics:
* Another JavaScript tutorial
* Interactive DOM in JavaScript


----------------------------------------------------------------------------
# Another JavaScript tutorial

If you're struggling figuring out JavaScript, here is another tutorial that I
like.  Its only drawback is that it is a bit dated, but the most basic aspects
of the language haven't changed much so it is not a bad place to start.

https://learnxinyminutes.com/docs/javascript/



----------------------------------------------------------------------------
# Interactive DOM in JavaScript

Now that you've had some time to work on
[Assn 4](https://usu.instructure.com/courses/529849/assignments/2581303)
and have run into the challenge that it poses, let's use what we have learned
about JavaScript and its functions to build a dynamic webpage that is
functionally similar to what this assignment calls for.


* [MDN: change event](https://developer.mozilla.org/en-US/docs/Web/Events/change)
* [MDN: input event](https://developer.mozilla.org/en-US/docs/Web/Events/input)

See the [slider](slider/) demo for an example of how to make a nested tree of
divs in JavaScript.  The structure of your code will loosely follow this
example.  

Be sure to spend some time reading the starter code I provided you with for
this assignment so you can see where/when to use each of the CSS classes.

Although it's due right after Spring Break, I recommend that you get this done
ASAP to take advantage of the Tutor Lab, the TA's and my office hours.

Good luck!!!
