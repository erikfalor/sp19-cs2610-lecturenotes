# 5: Readings and Resources

## Table of Contents

* Libraries vs. Frameworks
* Modern web applications leverage APIs in two ways
* How do I learn how to use a web API?
* What tools can I use to access web APIs?
* curl: A universal command line API tool
* JavaScript Object Notation
* XML - eXtensible Markup Language
* A list of free public APIs
* JavaScript and Date arithmetic
* Function Invocation Patterns in JavaScript
* Arrow Functions =>
* Promises & `fetch()`

--------------------------------------------------------------------------------
## Libraries vs. Frameworks

Software developers rely on pre-written collections of code to make their lives
easier.  We can separate these collections into two broad categories: libraries
and frameworks.


#### Library

A collection of classes, functions and variables which don't need to be used in
a particular way.  You have freedom to mix and match code from a library as you
see fit.

A library is a box of generic legos which you can assemble in any way you wish.




#### Framework

A collection of classes, functions and variables which imposes a particular
structure on your project.  A framework not only dictates which
files/classes/methods are to be used, but also where and when.  A framework
will suggest a "shape" for your project.

A framework is a specific Lego(tm) set meant to build a particular toy.

Django is an example of a framework.


### Frameworks use "Inversion of control"

Put another way:

> The key difference between a library and a framework is "Inversion of
> Control".  When you call a method from a library, you are in control.
> But with a framework, the control is inverted: the framework calls you.
>
> -- https://www.programcreek.com/2011/09/what-is-the-difference-between-a-java-library-and-a-framework/


--------------------------------------------------------------------------------
## Modern web applications leverage APIs in two ways

We had been using the term *API* to describe how code we write interfaces with
pre-written systems to access computational resources on our own machines

* Django's ORM is an API which gives us convenient control over a database
  using familiar Object-Oriented syntax and idioms
* The DOM in the browser is an API for manipulating a hierarchical HTML document

Now we shall consider web APIs which enable us to use resources distributed
across the internet and made accessible through HTTP.



### 1. Web APIs provide division of labor

An application's front-end code (HTML, CSS, JavaScript) is written in a
different language from the back-end code (e.g. Python). Data is sent
back-and-forth between both sides of the application using a common language
that both sides will understand (e.g. XML, JSON).

In this situation the form of the API itself is under full control of the
application developers, who use it solely in support of the front-end which is
their "product".


### 2. Web APIs enable access to 3rd-party services

An application may consist of a simple front-end UI which ties together many
different sources of data and services provided by other developers. The API is
not under the control of the application developers; they are customers of the
API just as their clients are customers of the front-end webpage itself.

In this situation there are 3rd party companies who create an API as a
"product". Other devs include these APIs into their own products. This makes
you and I indirect customers of the API developers.



Real-world webapps use both approaches at the same time.  You use APIs several
times per day, whether or not you know it.





--------------------------------------------------------------------------------
## How do I learn how to use a web API?

Using a web API is analogous to calling a function in a programming language.
Learning how to use a library of code comes down to learning three things:

1. What are the names of functions to call?
2. What parameters do the functions accept?
3. What is the output of the functions?


For web APIs, these questions become:

1. What are the URLs I can make requests of?
2. Which HTTP request types do I use, and what parameters may I send?
3. What is the output of the API?


### 1. What are the URLs I can make requests of?

Web APIs are accessed by making HTTP requests.  To use the API you need a way
to make HTTP requests: a web browser, a simple command-line program, or your
favorite programming language will serve.  You will need to learn which URLs
lead to a web application providing a resource you need.



### 2. Which HTTP request types do I use, and what parameters may I send?

The web application providing a resource may respond only to certain HTTP
request types (GET, POST, PUT, DELETE, etc.).  The API resource may require
sending parameters, just as you've done with HTML forms in Django.



### 3. What is the output of the API?

APIs may return data in a number of formats, such as

* JSON
* XML
* Plain-text
* HTML

Additionally, because the API's reply comes in the form of an HTTP response,
you will also receive an HTTP response code (200 OK, 404 Not Found, etc.) as
well as HTTP headers which may bear important information.


### Look for API Documentation/Instructions

APIs intended for public consumption will have a webpage describing this
information so developers can make apps using their carefully crafted API.




--------------------------------------------------------------------------------
## What tools can I use to access web APIs?

Once you have identified an API and located its documentation, you will need to
find a way to send HTTP requests to it.  Here are some programs from which you
can issue requests.

### Your browser

Many web APIs respond to simple GET requests.  To use these services, simply
enter a URL directly into the browser's address bar.  Firefox will pretty-print
JSON responses automatically.  The Network tab in Firefox's developer tools
enables you to edit and resend requests, giving you the ability to perform
other HTTP requests such as POST, PUT and DELETE.


### Browser plugins

Your browser's address bar has a few drawbacks, especially when the request URL
becomes complex.  The RESTED add-on for Firefox provides a convenient GUI to
make and save elaborate requests.

* [RESTED Add-On for Firefox](https://addons.mozilla.org/en-US/firefox/addon/rested/)

* [Postman plugin for Chrome](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop)

Postman is a really popular plugin for Chrome, but plugins on Chrome look to be
going the way of the dinosaur.  It is still offered as a stand-alone app.



--------------------------------------------------------------------------------
## curl: A universal command line API tool

* [curl Home](https://curl.haxx.se/)

While browser plugins are useful for interactive work, they have their
drawbacks:

* Browser plugins are not universal: Firefox plugins don't work on Chrome and
  vice versa.
* Browsers as platform are notoriously unstable; when a plugin's author doesn't
  fastidiously keep up with rapid changes in technology the plugin quickly
  becomes obsolete (viz. the Postman on Chrome situation)
* Browsers support only a limited range of protocols HTTP, HTTPS, and possibly
  FTP.  While the recent trend has been to move everything onto HTTP/HTTPS, the
  internet is much bigger than what you can experience in a browser
* GUI programs are difficult to automate

curl is a simple command line program for accessing online resources.  Think of
it as a browser without a GUI.  Because it is available on all operating
systems, it is used in API documentation as a universal example that will work
on anybody's computer.  curl can make all of the HTTP requests: GET, PUT, POST,
etc., as well as speak other internet protocols.  It's a very nice way to test
out or use an API, especially since you can call it from a script for maximum
automation.

It may already be installed in your Anaconda environment. If not, you can
easily get it on your computer:

    $ conda install curl


### curl command line arguments

    curl [OPTIONS] URL...

To use curl from the Anaconda Prompt, type `curl`, followed by zero or more
options, and one or more URLs.

Be advised that when sending GET requests your command shell might misinterpret
special characters such as `?` and `&`, leading to confusing errors.  Surround
URLs with single or double quotes to ensure that your shell doesn't
misinterpret your command.

curl's command-line syntax is simple, though there are many options.
Here are the most important options for you to know:


#### Specify the type of HTTP Request

Like the web browser, curl sends GET requests by default.  Use the `-X REQTYPE`
option to specify another request type.  Examples:

    curl           URL
    curl -X PUT    URL
    curl -X POST   URL
    curl -X DELETE URL


PUT and POST requests which need to send data that is not part of the URL can
specify that data with the `--data "DATA"` option.

This is a curl command that POSTs JSON data.  Notice that the JSON data is
surrounded by double-quotes to prevent the command shell from misinterpreting
the curly brackets as well as to regard the block of JSON as a single entity:

    $ curl -X POST --data "{'url': 'pokemon.com'}" https://pragma.archivelab.org


#### Specify HTTP Headers

Some APIs use HTTP Headers sent by the user as input.  These headers may be
used for authentication (e.g. an API key or username/password), or to specify
the format of the data the user wants to receive.  HTTP Headers are specified
with the `-H HEADER_STRING` option.

Using "ICanHazDadJoke.com" as an example, an ordinary GET request fetches the website's HTML.

By using the `Accept` header, the user may request a joke in either JSON or plain-text:

    # Default behavior retrieves the website in HTML 
    $ curl https://icanhazdadjoke.com/
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    ...


    # Request a joke in plain text
    $ curl -H "Accept: text/plain" https://icanhazdadjoke.com/
    Did you know you should always take an extra pair of pants golfing? Just in case you get a hole in one.

    # Request a JSON response
    $ curl -H "Accept: application/json" https://icanhazdadjoke.com/
    {"id":"VKe2gNCQnb","joke":"What do you call a group of killer whales playing instruments? An Orca-stra.","status":200}


#### Specify the User-Agent

Recall that the `User-Agent` header is automatically sent by browsers and may
be used by servers to determine compatibility or to decide whether to respond
with the full-sized "Desktop" web page or a smaller "Mobile" version.

Many APIs will refuse to serve requests arriving from unidentified clients.
These APIs don't necessarily expect that your User-Agent string matches that of
a well-known browser, they just expect *something*.

You could use `-H "User-Agent: UA_ID"` to identify your request, but because
this is a very common need, curl provides the shortcut `-A UA_ID`.

`curl -A "My Legitimate User-Agent" URL`


The MusicBrainz API asks that you identify your requests with the `User-Agent`
header:

    $ curl -A "cs2610 (erik.falor@usu.edu)" "https://musicbrainz.org/ws/2/recording?fmt=json&limit=3&query=waking+season" 




----------------------------------------------------------------------------
## JavaScript Object Notation

### Online resources about JSON

* [JSON.org](http://json.org/)
* [MDN - JSON Browser API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON)


### Use curl on the XKCD comic API

The XKCD API is described [here](https://xkcd.com/json.html).  This
documentation explains:

* Which URLs you can use
* What information to put into the HTTP request

Let's try one out:

    $ curl https://xkcd.com/149/info.0.json


What on earth are we looking at?  This data is expressed as JSON - JavaScript Object Notation.



JSON looks like JavaScript, but it is _not_ equal to JavaScript.  Be sure to
read the MDN article above to learn what the differences are.  JSON is a subset
of the JavaScript programming language.  In particular, JSON embodies the
syntax of "objects" in JavaScript, which are equivalent to dictionaries in
Python.

Like Python's dictionaries, the following is true of objects in JavaScript:

* Objects are a mapping of key-value pairs
* Each key must be unique
* Keys may be any string
* The values can be any JavaScript value, including functions and other objects
* Object literals are delimited with curly braces

The keys in JavaScript objects are most often referred to as "properties".
An object's properties can be referred two by two different syntaxes:

* OOP syntax: `console.log( obj.property )`

* Array syntax: `console.log( obj['property'] )`

* Creating a new object: `var o = { 'name': "Park Place", 'price': 350, 'rent': 35 };`



### Using JSON within Python

In Assignment 5 you will create a Django view which responds to GET requests by
returning JSON to the browser.  Because of the close correspondence between
Python dictionaries and JavaScript Objects this is very simple for you.

* [Python json package](https://docs.python.org/3.7/library/json.html) Python's
  `json` package provides a convenient way to convert between Python data
  structures and their JSON equivalents.

* [JsonResponse](https://docs.djangoproject.com/en/2.1/ref/request-response/#jsonresponse-objects)
  Django's `JsonResponse` class is analogous to the `HttpResponse`s that your
  views have been returning all along.  The `JsonResponse` may be used instead
  to conveniently transform a Python dictionary into JSON and send it to the
  browser with extra metadata that helps the browser make better use of it.



### Using curl + Python to pretty-print JSON data:

The JSON we have been getting from servers is a little hard for us humans to
read.

Python's built-in `json` package provides methods for creating/consuming JSON
data. Fortunately, it can conveniently be used from the command line to
pretty-print JSON data such that it becomes easy to read.

Pipe curl's output to `python -m json.tool`, like this:

    $ curl "https://archive.org/wayback/available?url=pokemon.com" | python -m json.tool


Explanation:
------------
The `curl` program makes a GET request of the provided URL and prints the
resulting JSON data. The '|' character on the command-line sends the data to
the Python program instead of to the screen.

The -m flag instructs Python to load the json.tool module. This module reads
whatever data is sent to its standard input, adds whitespace as appropriate,
and sends that to its standard output.

Ordinarily, a program's so-called "standard input" is the keyboard, and
"standard output" is the screen. The pipe character tells the shell to hook the
standard output of the program on the left to the standard input of the program
on the right.

For your convenience, here are some shell programs that cut down on the typing:


### pretty.sh

    #!/bin/sh
    curl "$1" | python -m json.tool


### pretty.bat

    @curl "%1" | python -m json.tool



--------------------------------------------------------------------------------
## XML - eXtensible Markup Language

In the early days of web APIs, response data would have been provided in a
format called XML.  These days most web APIs provide data in the JSON format,
so you should be glad that we're not in the early days anymore!

![The Ascent of Ward](xml_ascent.png)

You'll notice that XML resembles HTML, featuring "tags" formed with angle
brackets, elements and attributes.  Even today, there are still some APIs which
send results only in XML.  Some APIs allow the user to request which format
their application prefers to take its data (Quandl.com, for example, gives you
this choice).

Here is an example of the same API response expressed both in XML and JSON so
you can see for yourself the differences:


### MusicBrainz response in XML

	<?xml version="1.0" encoding="UTF-8"?>
	<metadata xmlns="http://musicbrainz.org/ns/mmd-2.0#">
	  <artist id="5b11f4ce-a62d-471e-81fc-a69a8278c7da" type-id="e431f5f6-b5d2-343d-8b36-72607fffb74b" type="Group">
		<name>Nirvana</name>
		<sort-name>Nirvana</sort-name>
		<disambiguation>90s US grunge band</disambiguation>
		<isni-list>
		  <isni>0000000123486830</isni>
		  <isni>0000000123487390</isni>
		</isni-list>
		<country>US</country>
		<area id="489ce91b-6658-3307-9877-795b68554c98">
		  <name>United States</name>
		  <sort-name>United States</sort-name>
		  <iso-3166-1-code-list>
			<iso-3166-1-code>US</iso-3166-1-code>
		  </iso-3166-1-code-list>
		</area>
		<begin-area id="a640b45c-c173-49b1-8030-973603e895b5">
		  <name>Aberdeen</name>
		  <sort-name>Aberdeen</sort-name>
		</begin-area>
		<life-span>
		  <begin>1988-01</begin>
		  <end>1994-04-05</end>
		  <ended>true</ended>
		</life-span>
		<alias-list count="2">
		  <alias sort-name="Nirvana US">Nirvana US</alias>
		  <alias primary="primary" sort-name="ニルヴァーナ" type="Artist name" type-id="894afba6-2816-3c24-8072-eadb66bd04bc" locale="ja">ニルヴァーナ</alias>
		</alias-list>
	  </artist>
	</metadata>


### MusicBrainz response in JSON

	{
		"id": "5b11f4ce-a62d-471e-81fc-a69a8278c7da",
		"type-id": "e431f5f6-b5d2-343d-8b36-72607fffb74b",
		"type": "Group",
		"name": "Nirvana",
		"sort-name": "Nirvana",
		"disambiguation": "90s US grunge band",
		"isnis": [
			"0000000123486830",
			"0000000123487390"
		],
		"country": "US",
		"area": {
			"sort-name": "United States",
			"disambiguation": "",
			"iso-3166-1-codes": [
				"US"
			],
			"id": "489ce91b-6658-3307-9877-795b68554c98",
			"name": "United States"
		},
		"begin_area": {
			"name": "Aberdeen",
			"sort-name": "Aberdeen",
			"disambiguation": "",
			"id": "a640b45c-c173-49b1-8030-973603e895b5"
		},
		"life-span": {
			"begin": "1988-01",
			"ended": true,
			"end": "1994-04-05"
		},
		"aliases": [
			{
				"type": null,
				"end": null,
				"sort-name": "Nirvana US",
				"begin": null,
				"name": "Nirvana US",
				"locale": null,
				"type-id": null,
				"ended": false,
				"primary": null
			},
			{
				"sort-name": "\u30cb\u30eb\u30f4\u30a1\u30fc\u30ca",
				"end": null,
				"type": "Artist name",
				"primary": true,
				"ended": false,
				"locale": "ja",
				"type-id": "894afba6-2816-3c24-8072-eadb66bd04bc",
				"name": "\u30cb\u30eb\u30f4\u30a1\u30fc\u30ca",
				"begin": null
			}
		]
	}


You've dodged a bullet with JSON, trust me!



--------------------------------------------------------------------------------
## A list of free public APIs

Many APIs require that you make an account before you may access their
resources.  Some APIs charge users based upon how much they access the provided
resources.  You can get started playing with APIs by using some which are freely
available.

[Public APIs by toddmotto](https://github.com/toddmotto/public-apis)



------------------------------------------------------------------------------------------
## JavaScript and Date arithmetic

You can form a string representing the date a few days ago using JavaScript's
[Date()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date) API.



--------------------------------------------------------------------------------
## Function Invocation Patterns in JavaScript

In most programming languages, a function's behavior depends upon how it is
*declared*, and not on how it is invoked.  JavaScript is unlike those other
mainstream programming languages.  A single function in JavaScript can behave
differently depending upon *how* it is invoked.

There are _four_ ways to invoke (call) a function in JavaScript:

1.  The function invocation pattern
2.  The method invocation pattern
3.  The constructor invocation pattern
4.  The apply and call invocation pattern

This means that you can call the _same_ function in four different ways and get
four different results.  Here is a demonstration of what the first three most
common invocation patterns look like ("apply and call" is not as common, so it
is left out):

    // Declaring a function `f` which will print a different message
    // depending upon how it's called
    var f = function() {
        console.log(`This function got ${arguments.length} argument${arguments.length == 1 ? '' : 's'}`);
        if (this === window) {
            console.log(`'this' is the global window object ${this}`);
        }
        else {
            console.log(`'this' is something else ${this}`);
            if (this.hasOwnProperty('name')) {
                console.log(`In fact, 'this' is an object named ${this.name}`);
            }
        }
        return true;
    };

    var r = null;

    // Function invocation
    r = f(1, 2, 3);

    // Method invocation - `f()` is a property of an object, and is invoked
    // like a method.
    var o = { name: 'Steve the object', f: f };
    r = o.f(1, 2, 3)

    // Constructor invocation
    // Notice that while `f()` explicitly returns the boolean value `true`,
    // that is not what is assigned into `r`
    r = new f(1, 2, 3);


### Further reading

This article explains the four patterns in more depth:

* [JS Function Invocation Patterns](http://doctrina.org/Javascript-Function-Invocation-Patterns.html)

MDN articles all about functions in JavaScript

-   [MDN: Declaring functions & function statements](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function)
-   [MDN Guide on functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions)
-   [Another MDN reference about functions? Sure, why not?](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions)



--------------------------------------------------------------------------------
## Arrow Functions =>

In light of the foregoing explanation, it is reassuring to realize that,
although one function may have _four_ separate meanings, that meaning is
decided by how the function is _called_.  Once you learn the patterns you will
know roughly how the function will behave when you write the function call.

Wouldn't it be worse if the meaning of a function could also depend upon _how_
it is declared?  Then, in order to know how a function would behave you'd have
to go search for its definition.  It sure is a good thing that this is not the
case in JavaScript... oh, wait.

In 2015 a new syntax for declaring functions was introduced to JavaScript in a
draft of the language called "ES6".  Here is the same function `f()` from
above, written in the new syntax and named `arrow()`.  Note the presence of the
`=>` arrow operator separating the empty parameter list from the body of the
function in curly brackets:

    var arrow = () => {
        console.log(`This function got ${arguments.length} argument${arguments.length == 1 ? '' : 's'}`);
        if (this === window) {
            console.log(`'this' is the global window object ${this}`);
        }
        else {
            console.log(`'this' is something else ${this}`);
            if (this.hasOwnProperty('name')) {
                console.log(`In fact, 'this' is an object named ${this.name}`);
            }
        }
        return true;
    };

    var r = arrow(1, 2, 3);  // ReferenceError: arguments is not defined

    var o = {'name': 'Kaleb', arrow: arrow};
    r = o.arrow(1, 2, 3);    // ReferenceError: arguments is not defined

    r = new arrow(1, 2, 3);  // TypeError: arrow is not a constructor


The jarring difference between "arrow" and "regular" functions is that arrow
functions are **not** implicitly passed `this` and `arguments` objects.  Thus
they don't work well as methods in an object, nor can they be used as object
constructors.

So, what *are* they used for?  Arrow functions (also known as "Fat Arrow
Functions", but you're not supposed to call them that anymore) improve the
readability of programs using "anonymous functions".  Anonymous functions are
are commonly used in functional programming, a style of programming which
JavaScript supports and that has become increasingly popular in the past
decade.

Ordinary functions are declared with a name, most often by assignment to a
variable upon declaration.  Programs written in the functional style may pass
functions as parameters to other functions:

    // Just an ordinary function
    var boo = function() {
        console.log("Boo!");
    }

    // This code will sneak up behind you and scare you in 3 seconds
    setTimeout(boo, 3000);


A function my be written directly into the parameter list, obviating any need
for a name.  Nameless functions are anonymous.  Since I don't plan on using the
function `boo()` anywhere else, it has No Use For a Name, not unlike that
awesome skate punk band from the 90's (RIP Tony Sly):

    setTimeout(function(){ console.log("Boo!") }, 3000);


This is a very common idiom in modern JavaScript.  As more programmers began
writing code in this style, complaints were raised that the repetitive tokens
`function`, `{` and `}` hindered readability.  The `=>` operator was introduced
to streamline this popular construct.

    // Arrow function accepting 0 args (denoted by an empty parameter list `()`)
    setTimeout(() => console.log("Boo!"), 3000);


### Further reading

-   [MDN: Arrow Functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)



This article nicely explains the differences between arrow and ordinary
functions.  Just don't take the joke about the "goes to" operator seriously.
There is no such thing, and talking about it, even as a joke, is probably bad
for your brain cells.

-   [ES6 In Depth: Arrow functions](https://hacks.mozilla.org/2015/06/es6-in-depth-arrow-functions/)



--------------------------------------------------------------------------------
## Promises & `fetch()`

The ability to make behind-the-scenes HTTP requests to web APIs was added to
the JavaScript language in the early 2000's (by Microsoft, of all outfits).
The original implementation was a JavaScript object called XMLHttpRequest
(XHR).

The XHR API achieved widespread acclaim among web developers and users alike
when it was used by Google's Gmail webmail client in 2005.  Developers now had
a way to create interactive web pages which behaved like desktop applications.
XHR when combined with other technologies is called Asynchronous JavaScript +
XML (AJAX for short).

* [XHR History](https://hackernoon.com/the-xhr-history-lesson-you-never-wanted-2c892678f78d)

You can view the progress of these background requests in the "Network" tab of
the developer tools by clicking a button that's still labeled "XHR".

XHR provides an object-oriented interface to setting up an HTTP request which
can happen in the background.  Instead of causing the execution of the main
JavaScript program to halt, awaiting the completion of an HTTP request which
may be very slow or could fail entirely, an event-driven approach is followed.
User-defined callback functions are added to the XHR object and are
automatically called by the browser upon receipt of the HTTP response.

The unforeseen consequence of this design results in a situation called
[Callback Hell](http://callbackhell.com/).  Callback hell is characterized by
deeply nested control structures and anonymous functions.  This unfortunate
syntactic malady is known as "Hadouken Code"

	fs.readdir(source, function (err, files) {
		if (err) {
			console.log('Error finding files: ' + err)
		}
		else {
			files.forEach(function (filename, fileIndex) {
				console.log(filename)
				gm(source + filename).size(function (err, values) {
					if (err) {
						console.log('Error identifying file size: ' + err)
					}
					else {
						console.log(filename + ' : ' + values)
						aspect = (values.width / values.height)
						widths.forEach(function (width, widthIndex) {
							height = Math.round(width / aspect)
							console.log('resizing ' + filename + 'to ' + height + 'x' + height)
							this.resize(width, height).write(dest + 'w' + width + '_' + filename, function(err) {
								//
								// H A D O U K E N !!!!!!
								//
								if (err) console.log('Error writing file: ' + err)
							})
						}.bind(this))
					}
				})
			})
		}
	})

![Hadouken Code](hadouken.jpg)

In response, a new model of synchronizing a series of asynchronous function
calls has been introduced around the concept of _Promises_.  By leveraging
arrow functions code written using this API simplifies the syntactical
appearance of deeply nested (read: dependent) asynchronous API requests.

### Further reading

* [MDN Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)

* [MDN Using Promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises)

* [MDN Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)

* [MDN Using Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch) - shows by example how to do things with the Fetch API such as POSTing JSON data and uploading files

* [Promises Chaining](https://javascript.info/promise-chaining) - by returning Promises from `.then()`-able callbacks we are able to make sequential calls to `fetch()` which depend upon the result of calls which came before.

* [Why I still use XHR](https://gomakethings.com/why-i-still-use-xhr-instead-of-the-fetch-api/) - an alternate viewpoint on the matter.
