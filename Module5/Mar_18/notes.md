# CS2610 - Mon Mar 18 - Module 5

# Announcements

## Exam #1 in Testing Center this week

The exam is available from Tues 3/19 through Thur 3/21


## Assignment 4 Effort Survey

[Optional Survey about Assn4](https://usu.instructure.com/courses/529849/quizzes/716667/take)

I appreciate any feedback you can offer that helps create make better assignments.



## FSLC - Belated Pi Day celebration

We wanted to share with you some cool projects to do on a Raspberry Pi on Pi
day, but it landed on Spring Break this year.  Better late than never!

Wednesday, March 20th
7pm - ESLC Room 053



----------------------------------------------------------------------------
# Exam 1 Review

* 40 questions in total - 100 points
* 10 review questions from the previous exam
* 30 questions covering material spanning modules 3-5


## Review question topics
* Structure of HTML document
* Inline vs. block elements
* CSS Selectors



## Git

* What does `HEAD` refer to?
    - The currently checked-out commit


* The fundamental unit of history in the git log are ______
    -   commits


* What is the "working tree"?
    - The currently checked-out files/directories that you can see


* Once a tag has been pushed to a remote repo, there is no way to delete it
    - False


* Which git command is used to move between commits?
    - git checkout OBJECT


* Which git command displays the log history beginning at tag `Assn3`?
    - git log Assn3


* Which git command deletes the tag `Assn3` from the remote named "myrepo"?
    - git push myrepo -d Assn3


## CRUD: HTTP + Databases

* What is the Object Relational Mapper (ORM)?
    - A programming API which provides an object-oriented interface to a database.


* List advantages of an ORM over directly accessing a database via SQL.
    - No SQL injections
    - Use familiar OO principles and consistent programming style
    - Avoid subtle differences between database-specific SQL dialects


* When your website is "Statically Hosted", there is no database-driven system
  such as Django providing dynamic content
    - True


* What does CRUD stand for?
    - Create, Read, Update, Delete


* What does it mean for an operation to be safe?
    - It does not modify resources on the server


* Which CRUD operations are safe?
    - Read


* What does it mean for an operation to be idempotent?
    - It does not make a difference to do the operation more than once


* Which of the CRUD operations are idempotent?
    - Read, Update, Delete


* Define a Python function which preforms an idempotent operation
    - def add1(n): return n + 1
    - No matter how many times I call this function, so long as I pass in the
      same value of `n` it continues to give me the same output

For contrast, here is a function which is *not* idempotent:

    n = 0
    def incr():
        global n
        n += 1
        return n

The final result in `n` depends upon how many times I call this function.


* The <form> element may contain only input widgets; other child elements are not allowed
    - False: <form> may contain any elements that a <div> may contain


* What does the *method* attribute on the <form> element mean?
    - Which type of HTTP request the client should use to send data
      (e.g. GET, POST, PUT, etc.)


* What does the *action* attribute on the <form> element mean?
    - Where the data is to be sent


* What is the difference between the "id" and "name" attributes on an <input> element?
    - "id" is used to locate the <input> element in the HTML tree
    - "name" is sent along with the value back to the server


* After successfully handling a POST request, why is it best-practice to redirect the user to another URL?
    - To prevent accidental re-submission


* Where is the data "payload" of a GET request located?
    - In the URL


* Where is the data "payload" of a POST request located?
    - In the body of the request


## JavaScript Objects

* The names of properties in a JavaScript Object may contain special characters
  such as spaces and operators
  - True; in this case they must be quoted and applied using *subscript* syntax.  For example:

    o.['hello'] = 'World';
    o.hello = 'World';
    o.['Is this a valid property? Yes!'] = 1;


* How many properties does the JavaScript object `o` possess?
    - 3: 'Frank' is duplicated here, but exists only once in the object `o`

    var o = {
        'Frank': 0,
        'Colton': 0,
        'Gabriela': 0
        'Frank': 0
    };


* How many properties does the JavaScript object `p` possess?
    - 4; properties contained within sub-objects don't count for `p`

    var p = {
        'Preston': [1, 2, 3, 4],
        'Cayden': { 
            'undeclared': 'hey',
            'sighted': 'hey hey',
            'minority': 'hey hey hey',
        },
        'Peter': 'pan',
        'Delaney': {
            'shade': 1,
            'wonder':2,
            'bathe': 3,
        },
    };



## JavaScript + DOM

* How does one create a DOM node from scratch?
    - document.createElement()


* How does one set an attribute on a DOM node?
    - ele.setAttribute('attr', 'value')


* How does one add a DOM node to the tree?
    - ele.appendChild()


* How does one remove a DOM node from the tree?
    - ele.removeChild()
    - ele.remove()


* How does one evaluate JavaScript expressions in the browser?
    1. Open the Inspector
    2. Go to the `Console` tab
    3. Type away


* How does one print a message to the browser's debug console?
    - console.log()


* How does one pop up a modal message window in the browser?
    - alert()



## DNS + SSH

* Communication between two computers across a network requires which two pieces of information
    - IP address
    - Port number


* What does 'DNS' stand for?
    - Domain Name System


* The Domain Name System (DNS) is essentially
    - A database mapping hostnames to IP addresses


* Explain whether or not this is a true statement: *"If you control the DNS,
  you can control which websites users can visit."*
    - This is true if users don't already know the IP address of a site


* Which task is the Secure Shell (SSH) primarily concerned with?
    - Remote web server administration


* SSH replaced older protocols such as Telnet and rsh because
	- SSH encrypts traffic between the client and the server


* SSH's SOCKS proxy feature is a quick, handy replacement for
	- A Virtual Private Network (VPN)


* You might use SSH's Remote Port Forwarding to do what?
    - Enable a remote user to access a service running on your private laptop
