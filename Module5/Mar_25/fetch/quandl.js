// Erik's API key. Get your own :P
let apiKey = 'api_key=o8A-AZSvTssXC8RkJooE';
let start = "start_date=2018-11-01",
    end   = "end_date=2018-11-07";

// Return a parameter string for a GET request from its arguments
var formParamsString = function() {
    let argsArray = Array.from(arguments);
    let url = argsArray.shift();
    return `${url}?${argsArray.join('&')}`;
}



var naiveStyle = function(url) {
    console.log(`Fetching ${url} the naïve way`);

    var youPromisedMeSomeJson = function(promise) {
        return promise.json();  // this actually returns another Promise
    }

    var youDidntLetMeDown = function(json) {
        theData = json;
        document.querySelector('#quandl').textContent = "Your data is available in the variable 'theData'";
        document.title = theData.dataset.name;
    }

    var promise0 = fetch(url);

    var promise1 = promise0.then(youPromisedMeSomeJson);

    promise1.then(youDidntLetMeDown);
}


var functionalStyle = function(url) {
    console.log(`Fetching ${url} the functional way`);

    fetch(url)
        .then(r => r.json()) // this actually returns another Promise, which
                             // will turn the JSON which the Quandl gave me
                             // into a JavaScript Object at some point in the future

        .then(json => { // by the time this function is called,
                        // my new JavaScript object is all ready to go
            theData = json;
            document.querySelector('#quandl').textContent = "Your data is available in the variable 'theData'";
            document.title = theData.dataset.name;
        });
}


// global variable to contain the result of our asynchronous request
var theData;

var go_fetch = function() {

    let url = '';
    switch ( document.querySelector('#dataset').value ) {
        case "visa":
            url = formParamsString('https://www.quandl.com/api/v3/datasets/EOD/V.json', apiKey, start, end);
            break;

        case "ge":
            url = formParamsString('https://www.quandl.com/api/v3/datasets/EOD/GE.json', apiKey, start, end);
            break;

        case "treasury":
            url = formParamsString('https://www.quandl.com/api/v3/datasets/FED/SVENPY.json', apiKey, start, end);
            break;

        case "poptot":
            url = formParamsString('https://www.quandl.com/api/v3/datasets/UGID/POPTOT_.json', apiKey);
            break;

        case "slcpop":
            url = formParamsString('https://www.quandl.com/api/v3/datasets/CITYPOP/CITY_SALTLAKECITYUTUSA.json', apiKey);
            break;

        case "nycpop":
            url = formParamsString('https://www.quandl.com/api/v3/datasets/CITYPOP/CITY_NEWYORKNYUSA.json', apiKey);
            break;
    }



    if (document.querySelector('#naive').checked) {
        naiveStyle(url);
    }
    else {
        functionalStyle(url);
    }
}
