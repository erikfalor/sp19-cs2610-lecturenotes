# fetch() in two styles

This page uses JavaScript's `fetch()` API in both the functional style that is
commonly found in the wild as well as in what I call the naïve style, where the
functionality is split up across a few "helper" functions.

As I urged you to do in class, please play with this code and see for yourself
how it works.  Feel free to break it, rewrite it, throw it out and start over
from scratch.
