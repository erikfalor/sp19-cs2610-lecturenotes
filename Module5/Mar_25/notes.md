# CS2610 - Mon Mar 25 - Module 5

# Announcements

## FSLC

Cal Coopmans AggieAir

Wednesday March 27
7pm ESLC 053


# `fetch()` and `Promise` syntax revisited

Let's take another look at the code we wrote last Friday to compare the
unfamiliar `fetch()` and `.then()` syntax with the naïve approach a beginner
might take to write it.  I've written that example in two styles, which I
invite you to experiment with:

[A very fetching example](fetch/quandl.js)
