# CS2610 - Mon Apr 08 - Module 7

# Announcements

============================================================================
# Call on 3 designated questioners
============================================================================

# Topics:

* Assignment 7 - Hack This Site!
* Computer Security Threats in $CURRENT_YEAR
* Computer Security Concepts
* Getting Started With Computer Security


----------------------------------------------------------------------------
# Assignment 7 - Hack This Site!


----------------------------------------------------------------------------
# Computer Security Threats in $CURRENT_YEAR

[A slideshow, I'm sorry](CyberSecReport.odp).  It's the only one I'll do.
I promise.



----------------------------------------------------------------------------
# Computer Security Concepts

How do I get started [in cybersecurity?](../Readings-and-resources.md)
