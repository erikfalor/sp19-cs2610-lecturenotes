# CS2610 - Wed Apr 17 - Module 7

# Announcements

## FSLC Closing Social
* Come relax and de-stress before finals week sucks the fun out of life
* Pizza will be provided
* Wednesday, 4/17 7pm @ ESLC 053



## Reminder: Please take the IDEA survey

Big thanks to those who have already left their feedback!



--------------------------------------------------------------------------------
# Guest Lecture by Skyler Cain of Rent Dynamics

See [his slides](APIs For The Real World.pptx) to learn how his company uses
Django to build the APIs that enable their business to handle everything from
communications to propery management.


Skyler also graciously offered to give you a tour of Rent Dynamics to get a
first-hand taste of a Software Development company.  Guys, this is a really
neat offer, and I hope that many of you take him up on it.  Heck, I might even
drop in over this winter break.


Here's how to get in touch with Skyler:

Skyler Cain <skyler@rentdynamics.com>


## How do I get into all of these different frameworks?

Just try them out - spin up a small project in a framework to find out if you
like it.  Just get a little taste of the possibilities.  Try out different
languages the same way, write a small project and see if it feels good.

Take a project that you've already completed in one language/framework, and
translate that.  You'll know what the end result should look like and you won't
feel lost, and it will give you a good perspective on the relative
strengths/weaknesses between them.
