# CS2610 - Wed Apr 10 - Module 7

# Announcements

## US Cyber Challenge Quests open until April 14th

We're in the middle of the 2019 CyberQuests competition and the new season has
been fierce. As of now, the top 5 states of competitors are 

1. Delaware
2. Illinois
3. Hawaii
4. California
5. Virginia

But there is still another week before the competition ends. You can make the
difference. How will you fare in the nation?  This is the link to the
competition for your convenience:

* https://uscc.cyberquests.org/

We keep up-to-date information on our social media so feel free to share our
posts and information relevant to the competition:

* Facebook: https://www.facebook.com/USCyberChallenge/
* Twitter: https://twitter.com/USCybChallenge


## FSLC Meeting Tonight

Tour of Open Source Software Licenses

7pm @ ESLC 053



## Computer Science Department Spring Social and Awards Dinner

Next Tuesday, 4/16 5–7 pm @ West Stadium Center

* Free Dinner - Taco Bar & Drinks from The Italian Place
* Prizes
* The RSVP list is full!  Contact cora.price@usu.edu to get on the wait list.



## FSLC Closing Social

* Come relax and de-stress before finals week sucks the fun out of life
* Pizza will be provided
* Next Wednesday, 4/17 7pm @ ESLC 053


## On-Campus Capture The Flag Event

Thursday, April 18th in HH 220. Please bring your laptop.



## Park City High School "Girls in Tech" Club Hackathon needs mentors

16 mentors are needed on April 27 (the Saturday in the middle of Finals Week)
to mentor a group as they complete a project for the competition. We have tons
of funding and will be giving away prizes to the winning groups at the end of
the event, including the mentor. We are looking for USU computer science
students with experience in App Inventor/Thunkable and/or Android Studio.

The event will be at Park City High School and we are having the mentors stick
with their teams all day and so the commitment would be around 11 hours as our
mentoring begins around 10:00.

[Sign up link](https://forms.gle/BFjx8VnfcqRMUAN78)



# Topics:

* Do try this at home (and only at home)
* Exploitation Step 1: Enumeration with Nmap "The Network Mapper"
* Exploitation Step 2: Launch an automated attack
* Exploitation Step 3: Sneak in through the back door
* Other Brute Force attacks




----------------------------------------------------------------------------
# Do try this at home (and only at home)

What I will show you today could get you into *serious* trouble (the orange
jumpsuit kind) if you do it to a computer system that you aren't permitted to
do these things to.

Don't be scared.  Be smart and use good judgement.

The ../Readings-and-resources.md for this module suggest many helpful things to
read and resources to use to get started learning about security.  Build a
security lab at your home and hack into that.



----------------------------------------------------------------------------
# Exploitation Step 1: Enumeration with Nmap "The Network Mapper"

* [Nmap homepage](https://nmap.org/)

To paraphrase the Nmap documentation:

> Nmap ("Network Mapper") is an open source tool for network exploration and
> security auditing.  Nmap determines what hosts are available on the network,
> what services those hosts are offering, what operating systems they are
> running, etc.  While Nmap is commonly used for security audits, many systems
> and network administrators find it useful for routine tasks such as network
> inventory, managing service upgrade schedules, and monitoring host or service
> uptime.


## Using Nmap from the command line

The syntax of the Nmap tool is simple.

    nmap [Scan Type...] [Options] {target specification}

Remember that when command line syntax is explained, square brackets indicate
optional parameters.  In this case, both `Scan Type` and `Options` are actually
optional.  The trick to Nmap is learning what all of the Scan Types and
Options are.  We'll walk through a few in a moment, but let's first learn what
`target specification` means.


## Identify your target IP address/network

Today we are targeting a virtual machine specially prepared with a few
[insecure web services](http://bit.ly/insecsrvr).  I already know that it's
connected to my computer via the 192.168.56.0/24 network, but even if I didn't,
I could still use Nmap to track it down for me (though this could take a
while).


*Be careful with Nmap!  The USU NOC can tell when people are scanning our network.
If it's you doing the scanning, yes, they know your IP address.*

Let's ask Nmap what systems are live on this network in
CIDR notation:

    nmap 192.168.56.0/24


The first result, 192.168.56.1, is my own laptop, so we'll ignore it.  Besides
my laptop, there are 5 other IP addresses which Nmap identified as hosting
network services.

    1. 192.168.56.16
    2. 192.168.56.17
    3. 192.168.56.18
    4. 192.168.56.19
    5. 192.168.56.20

From Nmap's output we can see the port numbers that were detected.  We
already recognize a few of these ports from our discussion a few weeks ago: SSH
servers, FTP and web servers hosting both HTTP and HTTPS services.  Nmap also
gives us a few hints for some others, such as the MySQL service apparently
running on 192.168.56.18 port 3306.  This is just a guess based upon the fact
that port 3306 is commonly used by MySQL.

Let's focus on these 5 IP addresses and see what more information Nmap can dig
up for us.  We'll refine our `target specification` to `192.168.56.16-20` to
save time.  We will also ask Nmap to perform a deeper scan to discover exactly
what services are running on those ports, and what versions of software are
behind them.

    nmap -sV 192.168.56.16-20

It turns out that the machine `192.168.56.18` actually is running MySQL!  But
Nmap wasn't able to log in to poke around more.  This is a big find because
MySQL isn't the sort of thing that should be exposed to the outside world like
this.  A few of the machines are running web servers.  Let's visit them and see
what their homepages offer.

One of them, `192.168.56.16`, is hosting a WordPress site!  WordPress is a very
common Content Management System built with the PHP language.  It is also
notorious for having all sorts of security flaws due to its under-regulated
plugin system.


## The moral of this story

Protect sensitive resources from being accessible outside of an appropriate
boundary.


----------------------------------------------------------------------------
# Exploitation Step 2: Launch an automated attack

Let's take an even deeper look at the machine running WordPress:

    nmap -A 192.168.56.16

Nmap even grabbed the text within the `title` HTML element of the main page.

It's kinda creepy how much information is so readily available to somebody with
Open Source Software and a few minutes to experiment with it.

Well, if you think that's creepy, you haven't seen anything yet!  Let's ask
Nmap to run a script that will spider its way across the website, looking for
more webpages we might attack.

    nmap -sV --script http-enum 192.168.56.16

Well, that turned up a few interesting tidbits.  Among other things, we found a
couple of login pages.  I wonder if this site admin changed the password after
installing?  Or did the admin pick a weak password?

There's a script for that!  Nmap has a handy option called `--script-help`
which dumps documentation for all installed scripts.  You can filter the list
by giving `--script-help` a pattern to match against.  I can look for all
WordPress related scripts with

    nmap --script-help '*wordpress*'

The script named `http-wordpress-brute` sounds appropriate.

Next, we need a dictionary of passwords for Nmap to try.  Daniel Miessler has
a few [password lists](https://github.com/danielmiessler/SecLists).  But I
think so poorly of this sysadmin that I'm willing to bet that the password is
one of the top 512 passwords revealed when RockYou.com was [hacked in 2009](
https://www.ghacks.net/2010/01/21/rockyou-hacked-some-30-million-passwords-in-the-wild-security/).

I have that dictionary in a file called `rockyou_top512.txt`.  Let's hand it over
to Nmap's WordPress BruteForce login script and see what happens:

    nmap -sV --script=http-wordpress-brute --script-args=passdb=rockyou_top512.txt 192.168.56.16


## The moral of this story

Use care when choosing authentication credentials; choose a password that's
*not* in a well-known cracker dictionary.


----------------------------------------------------------------------------
# Exploitation Step 3: Sneak in through the back door

When we ran the in-depth service scan above, I noticed an **old** version of an
FTP server running on one of the boxes:

    nmap -sV 192.168.56.16-20

Doesn't anybody patch their systems anymore?

On `192.168.56.19` there is a version of ProFTPD which seems familiar to me for
some reason...  Let's see if Nmap knows anything about ProFTPD.

    nmap --script-help '*proftpd*'
    Starting Nmap 7.70 ( https://nmap.org ) at 2019-04-09 23:09 MDT

    ftp-proftpd-backdoor
    Categories: exploit intrusive malware vuln
    https://nmap.org/nsedoc/scripts/ftp-proftpd-backdoor.html
      Tests for the presence of the ProFTPD 1.3.3c backdoor


Oh yes, let's try that!  We'll try the backdoor and attempt to run the `id`
program, which will tell us both whether the backdoor is indeed present, and if
so, it will tell us whether we have administrator access to the system.

    nmap -sV --script ftp-proftpd-backdoor 192.168.56.19

In addition to running an out-of-date and known-broken version of ProFTPD, this
sysadmin is also running it as the root user!  We can do anything we like on
this system.

I wonder what user accounts are active.  Let's check the password and shadow files:

    nmap -sV --script ftp-proftpd-backdoor --script-args="cmd=cat /etc/passwd" 192.168.56.19

    nmap -sV --script ftp-proftpd-backdoor --script-args="cmd=cat /etc/shadow" 192.168.56.19

## The moral of this story

Be aware of what services you are using and stay on top of security alerts.
Update your software so you aren't vulnerable to known attacks that Script
Kiddies can pull off.



----------------------------------------------------------------------------
# Other Brute Force attacks

## Brute Force SSH with Hydra 
According to the file `/etc/shadow` on `192.168.56.19` the account called
`user` has a password.  We could try an offline attack and crack that hash on
our own computer.  But this sysadmin is known to use poor passwords on other
systems; it's not much of a stretch to think this is the case here as well.
Let's try brute-forcing the `user` account via ssh (which Nmap reported is
running on this server) with another automated tool called
[Hydra](https://github.com/vanhauser-thc/thc-hydra):

    hydra -l user -P rockyou_top512.txt 192.168.56.19 ssh

