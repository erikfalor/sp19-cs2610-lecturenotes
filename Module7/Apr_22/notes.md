# CS2610 - Mon Apr 22 - Module 7

# Announcements


## Engineering State Assistants Needed

Engineering State is a fun-filled 4-day summer camp for students entering their
senior year of high school. Participants explore how engineering has changed
our world and learn what earning a degree in engineering is all about. Many of
our graduates have majored in engineering and now have successful, fulfilling
careers.

    https://engineering.usu.edu/events/e-state/


I am running two Challenge Sessions at Engineering State this summer: one is an
intro to Python and programming, and the other is a Virtual Reality experience.
I need two assistants who will be available to be on campus in the daytime
from June 3rd through the 5th (Monday - Wednesday), and who meet these
requirements:

* Know at least a little bit of Python
* Good communicators with upbeat, positive personalities
* Excited to work with teenagers
* Know how to party like a multi-tape Turing Machine!



## Reminder: Please take the IDEA survey

Big thanks to those who have already left their feedback!



# Topics:
* Making a career in cybersecurity
* Website coding projects to work on over the summer
* Final Exam Review


--------------------------------------------------------------------------------
# Making a career in cybersecurity

When speaking to James and John Pope after a guest lecture last semester, it
came up that many organizations are facing a severe shortage of cybersecurity
personnel.

Entry level wages are ~$60k, and this can go up as high as ~$120k with a few
years' experience.  

If you can pass a background test and a drug test, you could get a job with the
FBI today (A person who hires for the federal government told James that he
really likes to hire Utahans because they tend to not have problems passing
these sorts of tests).

Notice that no mention of skills, training, or degrees is mentioned here.
The most important qualification (after passing the tests) is merely having an
*interest* in cybersecurity.

The jobs are out there.  Put on your black hoodie and go get 'em!


--------------------------------------------------------------------------------
# Website coding projects to work on over the summer

Here are some ideas for projects you can work on over the summer to keep your
web-dev skills sharp:

* Set up a Raspberry Pi webserver.
    - Learn how to use real-life production webservers such Apache or Nginx
    - Configure Port Forwarding on your home router to make it visible to the 'net
    - Get a free domain name from https://www.dot.tk and configure your own DNS

* Forum in Django
    - It's a lot like a blog - you can use that code as a starting point

* Django URL shortener
    - There are some good tutorial videos on YouTube about this

* Django Pastebin
    - There are a billion of these on the 'net, so why not make your own?

* Django self-destructing notes service 
    - A cross between a URL shortener and a pastebin
    - Creates a webpage with a messagae which can be visited only once; after
      the first visit, the secret note is deleted
    - The Ur-example is at https://secrets.xmission.com/
    - Bonus points if you can encrypt the message in the browser so that your
      server *never* has access to the actual message!

* Explore other frameworks/languages
    + Try [Flask](http://flask.pocoo.org/), another Python web framework 
        - Translate some of your Django assignments into Flask to learn the
          differences and relative strengths/weaknesses between the two
    + Popular alternatives to Vue.js are Angular and React


Above all, stay curious this summer!



--------------------------------------------------------------------------------
# Final Exam Review

* 50 questions in total - 130 points
* Cumulative across the entire semester
* The exam is available in the Testing Center all week long


# HTML/CSS

* What is the purpose of HTML, and what problem does it solve?

* What is the `form` element for
    - What is the `action` attribute for?
    - What is the `method` attribute for?
    - How are data within the form presented to the server?

* How does the `onclick` HTML attribute work?
    - The programmer specifies JavaScript code as the value of this attribute
    - The keyword `this` refers to the DOM element on which this attribute is applied
    - It may be applied to any visible DOM element to make it respond to clicks


* What is the purpose of CSS, and what problem does it solve?
    - In the absence of a CSS stylesheet, which appears larger, text within `h1` or text within `h4`?
    - In the absence of a CSS stylesheet, what does a `div` look like?

* Simple CSS selectors
    - #ID selectors
    - .class selectors
    - element selectors



# Django

* What is a Django Project?
    - How does one create a new Django project?

* What is a Django App?

*   Which Django file is responsible for...
    - The code which runs when a user wants to see a page (VIEW) = `views.py`
    - Mapping a URL to a view function (CONTROLLER, aka receptionist) = `urls.py`
    - Defining classes which represent database records (MODEL) = `models.py`
    - Contains configuration information for my project = `settings.py`

*   Django templates
    -   Use the `render()` function to render a template
    -   A Django context object is a Python dictionary
    -   Django template variables are surrounded by double curly braces {{ }}
    -   Django template tags are surrounded by curly braces like this: {% %}


* After successfully handling a POST request, best-practices dictate that your
  response to the user should Redirect the user to another URL so that they
  cannot accidentally re-POST



# JavaScript

* What problem was JavaScript created to solve?

* JavaScript is a dynamically typed language, and features automatic type
  conversion

* Be prepared to run some JavaScript code in the Console and explain the
  results (Do this in a separate tab so as to not accidentaly break your Canvas
  test)

* The `fetch()` function returns Promise objects
    - Promises are resolved by calling their `.then()` method.

* The big difference between **arrow** and **regular** functions is that
  **arrow** functions are not passed an implicit `this` or `arguments` object

* APIs primarily intended to be used by other programs

* What sorts of HTTP requests can you use on an API? 

* Why do modern web applications take advantage of APIs?

* What must you know in order to use an API? 



# Domain Name System and Port numbers

* Communication between two computers across a network requires two pieces of
  information:
    - IP address
    - Port number

* A port number identifies a service on a machine

* The Domain Name System (DNS) is essentially a database mapping hostnames to
  IP addresses

* The Secure Shell (SSH) is primarily concerned with the task of remote web
  server administration
    - SSH replaces older protocols such as Telnet and RSH because they are insecure



# Vue.js

* What are the properties of the object passed to the `Vue()` constructor, and what do they do?
    - data 
    - el
    - methods
    - created()

* What do the `v-if` and `v-else` directives do?

* How does the `v-for` directive work?



# Security

* What are Black Hat and White Hat hackers?

* What are Black and White box hacking?

* What are Red and Blue teams?

* What is XSS?
    - Cross-Site Scripting
    - Injecting code directly into another site

* What is Reflected XSS?
    - Inject code into a URL

* Input validation should be handled both on the client side and on the server side
    - Never assume that people will use your application as you intended

* What is your best defense against hackers?
    - It is okay for you to think and dress like a hacker!
