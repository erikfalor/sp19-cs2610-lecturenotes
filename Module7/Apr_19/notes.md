# CS2610 - Fri Apr 19 - Module 7

# Announcements

## Tutor Lab End of Semester Schedule

The tutor lab will close for the semester at the end of the day on Tuesday the
23rd, which is the last day of classes.



# Guest Lecture by James and John Pope of [Pope Tech](https://pope.tech/)

[Slides](https://bit.ly/2IuqxnM)


[70 sites which offer free challenges for hackers to practice their skills](http://www.blackroomsec.com/updated-hacking-challenge-site-links/)
