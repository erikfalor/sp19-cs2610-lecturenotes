# CS2610 - Wed Jan 09 - Module 0

# Announcements


## FSLC Meeting Postponed

The first meeting will be next Wednesday, Jan 16



## Upcoming events

### Silicon Slopes Summit

Jan 31 - Feb 1

A technology and entrepreneurship summit held in Salt Lake City. There are
going to be major speakers, networking opportunities, and a Sundance Movie
Screening. Students can register at https://www.siliconslopessummit.com/ and
use the code SSTS19USUCC at checkout for a free pass. 



### STEM Fair - Career fair for Stem Majors

Wednesday Feb 13 3:00-7:00, TSC Ballroom. 



### Engineering Council

Engineering Council is the student government for the College of Engineering.
We meet every Thursday in room 325 (The room right next to the tutoring center)
at 5 PM. We are especially looking to recruit more CS majors.



### Python Utah North Meetup

Data Pipelines with Airflow - Matthew Housley
Wednesday, January 16, 2019

https://www.google.com/maps/search/?api=1&query=41.687650%2C-111.864990

Details at https://www.meetup.com/Python-Utah-North/events/256899519/


# Topics:

* Installing Anaconda and Git
* What is Git?
* How do I use Git to submit my homework?


----------------------------------------------------------------------------
# Installing Anaconda

See this module's [assigned reading](https://bitbucket.org/erikfalor/sp19-cs2610-lecturenotes/src/master/Readings_and_resources.md)



----------------------------------------------------------------------------
# What is Git?

Git is a version control system (VCS).  Think of it as the ultimate "undo"
button for an entire project.  Presently you would keep All of the files
comprising your project in a directory on your computer.  Git adds a hidden
database into that directory making it a repository.  With that database git is
able to  remember all changes you've made to any files within the repository,
no matter which program you've used to change them.

#### repository = a directory which has been initialized for use with git

#### commit = a record of changes to one or more files

Instead of manually keeping track of lots of backup directories for your code,
git will remember what you were doing at every step of the way, and gives you
the ability to see what your current files were like in the past, conveniently
undo a mistake, coordinate your work with many other programmers, and keep your
project in sync across many computers.


### Is Git the same thing as GitHub?

Git is not made by GitHub; Github just made a hub.

There are several "hubs" Git users may use to share their code online; GitHub
is just the most popular right now.  I'll teach you how to use another Git
repository hosting service called Bitbucket.


----------------------------------------------------------------------------
# How do I use Git to submit my homework?

To complete Assignment #0 you must master the 8 commands listed in the assigned
reading on Bitbucket.  These commands are:

1.  git status
2.  git init
3.  git add
4.  git config
5.  git commit
6.  git remote
7.  git push
8.  git log


On your computer
================

### First, you must create a "repository" somewhere on your computer.

Remeber that a repository (or a repo) is just a directory containing a special
database which is used by Git.

Before you create a repo you should make sure that you don't already have a
repo here.  You definitely *do not* want to create a git repository within a
git repository.  I call this situation *Gitception*, and like the movie
*Inception* it is very confusing to experience.

You can detect the presence of a repo in this folder by running the `git status`
command.  In this case you want to see an error message because the error alerts
us to the *absence* of a repo.


```
$ git status

fatal: not a git repository (or any parent up to mount point /)
Stopping at filesystem boundary (GIT_DISCOVERY_ACROSS_FILESYSTEM not set)
```

Before creating a new repository always run `git status` before running `git
init` to find out if you are already within a git repository.  You want to see
`git status` present an error message in this situation as it will confirm that
you are *not* already inside a git repository.


### Make a new git repository called cs2610-falor-erik-assn0

Be smart and substitute your name for mine.

```
$ git init cs2610-falor-erik-assn0
```


### Move into the repository's directory

Generally speaking, the effects of a command run in the shell is isolated to
the directory the shell is currently open to.  Use the `cd` or *change
directory* command to open a new directory so that commands will operate in the
new location.

```
$ cd cs2610-falor-erik-assn0
```

To conform to the assignment submission guidelines you will create a new
directory called 'src' and put your code there.

```
$ mkdir src
$ cd src
```


### Create a file

Use your text editor to create a new file in this directory.  You might call it
"README.md" and say something nice to your grader (hey, you never know).  I'll
call mine "index.html" and write a webpage that will help us
remember the steps to using git.



### Query git for the status of your little repository

Save index.html and see what git has to say about it's presence in the repository

```
$ git status
```


### Tell git to start keeping track of index.html

```
$ git add index.html
```


Get the status of your repository again; it should tell you that you have
changes ready to be committed

```
$ git status
```


### Tell git who you are

Git wants to know your name and email address so that when you make commits it
can record who was responsible.


```
$ git config --global user.name   "Danny Boy"
$ git config --global user.email  "danny.boy@houseofpain.com"
```


### It is time to go through with it

Tell git to permanently record your work.  In addition to the state of tracked
files within the repository, this permanent record contains the date, name of
the author, and a short message describing the change.

I recommend that you supply a helpful message about what you were doing and why
because that information will be valuable later on.

For beginners it is best to supply a brief message directly on the command line
with the *-m* option and double quote marks, like so:

```
$ git commit -m "This is a thoughtful message about this code commit"
```

If you forget the -m option you may find yourself in an arcane program which is
difficult to leave.  When this happens to you, press <Esc> followed by ZZ (that
is, capital "Z" twice).


Now that you've committed your changes get the status of your repository once
more.

```
$ git status
```


Make a few more changes to this file, saving, adding, and committing each time.
Try hard to give each commit a meaningful message that later on will help you
remember what you were doing at the time.  Now you have created a timeline of
changes to the repository which you can view with the `git log` command.

```
$ git log
```



On Bitbucket
============

Now you're ready to put your code on the web.  Log in to your Bitbucket account
and create an empty repository there.  You will send the code from your
computer to this empty location on Bitbucket.

1. Log into Bitbucket
2. Click on the + button in the left sidebar
3. Click on "Repository"
4. Give your repository a name; cs2610-falor-erik-assn0 seems appropriate here
5. Make sure that you've designated it to be *private*
6. **Do not** include a README file
6. Click "Create Repository"
7. The resulting page will bear the text "Let's put some bits in your bucket"
8. Click the gear icon in the left sidebar to enter the settings for your
   repository.  Click "User and group access".  Enter the Bitbucket usernames
   for the instructor and TAs as found in the Syllabus (erikfalor, manish-usu,
   thomasoreilly).  We only need *read* access to your repository.
9. Click the blue "</>" icon on the left sidebar to return to the  "Let's put
   some bits in your bucket" page.  Scroll down to the section titled "Get your
   local Git repository on Bitbucket".  Copy the commands listed under "Step
   2".  They should look like this, but with your name instead of mine:

```
$ git remote add origin git@bitbucket.org:erikfalor/cs2610-falor-erik-assn0.git
$ git push -u origin master
```


Back on your computer
=====================

Next, use the git program in Anaconda to "push" Assignment 0 to Bitbucket
where the graders can find it.

1. Return to your console

2. Paste the commands Bitbucket provided on the command line.
   You may be prompted for your Bitbucket password.  You will not see anything
   as you type it in; this is to defeat shoulder-surfers from learning your
   credentials.  Enter your password and press enter.  If you spelled your
   password correctly git will display something like the following:

```
Counting objects: 4, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 2.48 KiB | 2.48 MiB/s, done.
Total 4 (delta 0), reused 0 (delta 0)
To https://erikfalor@bitbucket.org/erikfalor/cs2610-falor-erik-assn0.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
```

3. Return to the Bitbucket webpage and refresh it - it will no longer tell
   you how to put more bits in your bucket.

4. Finally, submit the repository's URL to Canvas.

