# Module 0 Readings & Resources

## Table of Contents

* Sign up for a Bitbucket account
* Install Anaconda for Python 3.7
* Upgrading an existing Anaconda installation
* Install Git (optional for Mac and Linux users; recommended for Windows users)
* How to learn Git
* Git Tutorials

--------------------------------------------------------------------------------
## Sign up for a Bitbucket account

[Create a Bitbucket account](https://bitbucket.org/account/signup/)

It is important that you use a .edu email address so that you are not limited
in how many *private* repositories you may create.  Your username doesn't need
to match your real name.

See the class rules on Bitbucket for more details about creating this account.


--------------------------------------------------------------------------------
## Install Anaconda for Python 3.7

The Anaconda Python Distribution is a free, easy-to-install package manager,
environment manager and Python distribution with a collection of 1,400+ open
source packages with free community support.


#### TL;DR: Anaconda is Python + some other tools + a package manager

Think of a *package manager* as an app store.  Anaconda's package manager is a
program called "conda". 

We shall use the Anaconda Python Distribution as the Python 3.7 reference
implementation in this class.  *Reference implementation* means that your
assignment submissions are gradedi against their behavior under this version of
Anaconda Python.  It is your responsibility to ensure that your code performs
well in this environment.


*   [Download Anaconda for your platform](https://www.anaconda.com/download/)
*   [Anaconda Installation Guide](https://docs.anaconda.com/anaconda/install/)



--------------------------------------------------------------------------------
## Upgrading an existing Anaconda installation

If you already have a previous version of Python installed through Anaconda you
may upgrade it by running the following commands:

    $ conda install -c anaconda python=3.7
    $ conda update --all

Verify that the upgrade was successful by asking Python for its version:

    $ python --version
    Python 3.7.1 (default, Dec 14 2018, 19:28:38)


--------------------------------------------------------------------------------
## Install Git (optional for Mac and Linux users; recommended for Windows users)

Anaconda makes it easy to install the Git command-line interface on Windows,
which is a nice bonus for those who don't want to go out of their way to
manually download and install extra software.  If you already have Git
installed on your computer you may rest assured that Anaconda's git program
will not interfere.  I will demonstrate how to use Git via the command-line as
this is the standard, most powerful and most portable way to use git.  I do not
require that you use the command-line to submit assignments, but I do encourage
all Computer Science students to become comfortable with this venerable user
interface.


### Git installation instructions for Windows (or users who don't already have git)

1.  Open the Anaconda Prompt
2.  Run `conda install git` and answer 'yes' when prompted
3.  Run `git` to test that the installation succeeded; if it worked this
    command will display a usage message for git.


### Use an SSH key instead of a password for authentication

Your git private repositories on Bitbucket are password-protected.  Frequently
pushing to and pulling from your private repos requires you to enter your
credentials each time, which quickly becomes tiresome.  Instead, you may
configure Git to use the industry-standard method for avoiding annoying
password prompts: SSH public/private keys.


### _Linux and Mac users_

1.  Log into Bitbucket
2.  Click on your Avatar -> Bitbucket settings -> Security -> SSH keys
3.  Follow the instructions on that page to generate an SSH key on your system
    and add it to your Bitbucket account


### _Extra instructions for students using Anaconda's git client on Windows_

_If you are using Linux or Mac OSX, this section does not apply to you_

_If you are a Windows user, please run through this section before you create
your repository for Assignment #0_


To keep things simple, I have written a Python program which will perform all
of the necessary steps to upgrade your Anaconda command-line environment for
this to work on Windows.  You will need to use the git software to obtain a
copy of my program.  After my program upgrades your Anaconda environment it
will open a notepad window containing your own SSH key.  You may then log into
Bitbucket and submit this SSH key into your Bitbucket settings page (this is a
different page than the per-repository settings page; be sure that you are in
the right place when you set this up!)


1.  Open the Anaconda Command Line
2.  Run `git clone  https://bitbucket.org/erikfalor/conda_fix.git`
3.  Move into the new `conda_fix` directory created by the previous command:  
    `cd conda_fix`
4.  Run the conda_fix.py Python program:  
    `python conda_fix.py`
5.  When Notepad opens up, follow the instructions displayed at the bottom of
    the command window to install your SSH key on Bitbucket.


--------------------------------------------------------------------------------
## How to learn Git

My own experience as well as discussions with industry professionals have
convinced me that being able to competently use a version control system is a
vital skill in today's job market. I am also convinced that your becoming
proficient with a version control system will make your time as a student more
effective and enjoyable. Therefore, mastery of Git is an important goal of this
class.

If you know anything about git, you probably know that git does not have a
reputation as being a user-friendly piece of software. Repetition and continual
exercise is the only way that you will acquire the necessary level of
experience with this tool. I require that you submit all assignments as git
repositories so that you will have ample practice using git.

If you are new to git, you will find a steep learning curve at first. Don't
give up, and don't worry about your mistakes! The power of git is that it
allows you to bravely forge ahead, confident that you can recover from any
mistakes you may make along the way. Git is your safety net.

We'll start off with seven basic commands, and build from there.

|**Command**                                       | **Purpose**
|--------------------------------------------------|------------
|[git status](https://git-scm.com/docs/git-status) | Status of local files (which files are changed, which files haven't been added to the repository, etc.)
|[git init](https://git-scm.com/docs/git-init)     | Create a new repository on your computer
|[git add](https://git-scm.com/docs/git-add)       | Select files for inclusion into the repository
|[git config](https://git-scm.com/docs/git-config) | Set and view git's configuration parameters
|[git commit](https://git-scm.com/docs/git-commit) | Permanently record changes into the repository
|[git remote](https://git-scm.com/docs/git-remote) | Associate a repository on another computer with this local repository
|[git push](https://git-scm.com/docs/git-push)     | Send committed changes to another repository
|[git log](https://git-scm.com/docs/git-log)       | Review the history of commits


As you explore git's capabilities [this reference](https://git-scm.com/docs) will be useful.


--------------------------------------------------------------------------------
## Git Tutorials

There are many great resources for learning git in more depth on the web. This
is a selection that previous students have recommended. Please use these to
deepen your understanding of this important tool.

-   [Git Documentation and Videos](https://git-scm.com/doc)
-   [Getting Git Right](https://www.atlassian.com/git)
-   [Learn Git With Bitbucket Cloud](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)
-   [Git Command Cheatsheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf)
-   [Git Tutorial on Codecademy](https://www.codecademy.com/learn/learn-git)

