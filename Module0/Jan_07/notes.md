# CS2610 - Mon Jan 07 - Module 0


# Announcements

## DC435 Cybersecurity Meetup group

https://dc435.org



## Free Software and Linux Club meetings

This semester the FSLC will be meeting on Wednesdays in ESLC 053 @ 7PM.

Our first meeting is this Wednesday night.  The club is a good place to gain
skills and experiences that you are not likely to find in the classroom.
Together we'll learn and do things of *practical* importance that will make a
big difference to your resume. 


## Cybersecurity club w/Management and Information Systems Dept.

This club will be meeting on Wednesdays in ESLC 053 @ 6PM.


# Topics:

* What can I expect from this class?
* "Get to know you" mud card activity


----------------------------------------------------------------------------
# What can I expect from this class?

The purpose of this course is to prepare you to become a competent
problem-solver who understand and uses the best Software Engineering
techniques.


## Class Rules

To complete your enrollment in this course you will need to read and understand
the Syllabus and Class Rules.  There will be a quiz.  The quiz doesn't count
against your grade and you may take it as many times as you need.  You will be
unable to proceed in the course until you complete the quiz with a 100% score,
and failure to complete the quiz before the due-date will result in your being
dropped from the class.

The Syllabus is found on Canvas, and the class rules are on Bitbucket.

* [Lecture Notes on Bitbucket](https://bitbucket.org/erikfalor/sp19-cs2610-lecturenotes/src/master/)
* [Ten Thousand](https://xkcd.com/1053/)
* [The Three Virtues](http://threevirtues.com/)



### Open-door policy

You are not only welcome to drop in to my office, but you are encouraged to do
so.  My office is on the 4th floor of Old Main, room 421.  My dedicated office
hours are posted in the Syllabus, but you are welcome to drop by anytime I'm
in.


### Teaching Assistants

* Manish Meshram
* Thomas O'Reilly

You will find their contact information and office hours in the Syllabus


### Tutor Room Hours

The tutor room is right across from my office.  You are able to take your
questions there and work on your programs.

Old Main 4th floor, room 419

Mon - Fri: 10AM to 9PM
Sat: 11AM to 9PM



## [Course Topic Outline](../course_topic_outline.md)

Unlike other web-development courses offered at USU, this course is not focused
on making pretty webpages.  Instead, you will learn both sides of the process
to create dynamic, database-driven, web applications using the most basic,
fundamental technologies wherever possible.


----------------------------------------------------------------------------
# "Get to know you" mud card activity


I'll often ask you to turn in a "mud card" at the end of class.  The name comes
from "muddiest point".  You will reflect upon what was discussed in a lecture
and write a brief note explaining what you did not understand or a new question
raised by what you learned.

This serves the following purposes:

1. Provides a quick head-count of who attendend today
2. Putting into writing what you learned in a day increases recall
3. Lets me know which topics were well recieved, and which need another approach
4. Gives another opportunity for you to ask a question in a non-intimidating way



Please write the following information on a piece of paper and return it to the
instructor or a TA:

1. Your name and A#
2. What is your class and major?
3. Why are you enrolled in this course?
4. What is your favorite game?
5. What type of music do you like the most?
6. What do you like to do for fun?
7. Names of at least 3 of your neighbors and something unique or interesting about them
8. Who are your study buddies in this class?
